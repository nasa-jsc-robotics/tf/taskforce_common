from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['taskforce_common',
              'taskforce_common.connection',
              'taskforce_common.logcfg',
              'taskforce_common.task',
              'taskforce_common.utils'],
    package_dir={'': 'src'},
    package_data={'taskforce_common.logcfg': ['python_logging.conf'],
                  'taskforce_common.task': ['resources/json/schemas/taskforce_schema.json']},
    scripts=['scripts/taskforce_convert_blockfile']
)

setup(**d)
