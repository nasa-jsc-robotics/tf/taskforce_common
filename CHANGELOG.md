Change Log
==========

2.1.3
----

* Added ability for Groups to have parameters
* Added check so Tasks that have only been deployed do not get stopped
* Fixed onParameterChange to only happen when task is deployed
* Added the breakpoint parameter to Subscription objects and allow the EventHandler to wait for additional flag before running the relevant thread when encountering these objects.

2.1.2
-----

* Removed unused schema files
* Minor edits to some comments
* Added LICENSE.md

2.1.1
-----

* Fixed bug preventing the onParameterChange hook from executing
* Changed getParameter to return a copy instead of a reference

2.1.0
-----

* Added a new "failed" state with an "onFail" hook.  This makes it very important for Tasks to return True in the
  onInit, onStartup, and onExecute methods.
* Added more information to the default Task template.

2.0.0
-----

* Added the concept of a "Group" to replace what used to be called a Block in TaskForce 1.0
* Groups can have parameters that are mapped to other Blocks (Tasks / sub-groups)
* Groups can propagate arbitrary events using the EventRelay
* Changed the definition of a "Block".  A "Block" is now a base class for a "Task" or a "Group"
* Modified "Tasks" such that they no longer have their own EventHandler or CommandHandler.  These will be centralized in
    the Engine
* The event loop in the EventHandler has been made much more efficient by using a blocking I/O model
* Tasks are no longer a sub-class of multiprocessing.Process.
* Events trigger all events to trigger in their own thread.
* Fixed bug in Event "ne"
* More thorough code comments / documentation
* Added a converter tool to convert .block file to .group files
* Changed the schema from .block to .group files.  Moved subscriptions out of the Tasks and made them as a group level
    set of objects.
* More unittests added
* Changed loggers to use the "Path" as the logger category
* EventHandler throws when subscribing to non-existing destinations or callbacks
* Unified the API between a Task and a Group
* Broke up 'status' and 'info' for Tasks/Groups
* Added a "fileName" option to the status so that it can be used by the Live Viewer
* The root group module can now return itself with a `getBlocks` method call is requesting the root group (i.e: if there 
    is a reference to the root group, the name passed in the `getBlocks` method will be the `<group>` name). This is 
    useful and necessary when dealing with subscriptions/event relays for nested groups
* Added default Event Relays to the `Group.py` module. This has legacy values to facilitate backwards compatibility for
    the previous engine.
* Updated the `taskforce_convert_blockfile` script. The script can now be given a parent folder, and it will search
    recursively all subdirectories for `.block` and `.block.scene` files and convert them to `.group` and `.group.scene`
    files. 
* The script has been updated to reflect any data structural changes.
* Added `VERSION` variable so the GUI will reference the current release version in the window title

1.0.2
-----

* Added xenial CI
