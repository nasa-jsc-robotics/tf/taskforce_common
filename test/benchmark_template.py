#!/usr/bin/env python
import argparse
import sys
from taskforce_common import logcfg
from taskforce_common import ConnectionTestFixture

######################################
# User imports

# from <MY PACKAGE> import <MY PUBLISHER>, <MY SUBSCRIBER>
# OTHER IMPORTS

#
#######################################

class DataClass(object):
	def __init__(self):
		self.a = 1
		self.b = 2
		self.s = 'this is a string'

	def __str__(self):
		return 'a={},b={},s={}'.format(self.a,self.b,self.s)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Benchmark pipe connections')
	parser.add_argument('-o', '--output', type=str, default=None, help='output log filename')
	parser.add_argument('-i', '--iterations', type=int, default=1000000, help='Number of iterations')
	parser.add_argument('-r', '--runs', type=int, default=10, help='number of runs')
	parser.add_argument('-s', '--sleep_time', type=float, default=0.2, help='sleep between publisher init and start publishing')

	args, unknown_args = parser.parse_known_args(sys.argv[1:])
	iterations = args.iterations
	runs       = args.runs
	output     = args.output

	###################################################
	# User data

	# replace with publisher and subscriber
	subscriber  = MY SUBSCRIBER
	publisher   = MY PUBLISHER

	# myInitObj = Obj()
	
	# pubInitArgs = {'myinit_arg':myInitObj}
	# subInitArgs = {'subscriber_arg':1, 'subscriber_arg':2}

	#
	####################################################

	fixture = ConnectionTestFixture(output)
	fixture.setupPublisher(publisher, pubInitArgs)
	fixture.setupSubscriber(subscriber, subInitArgs)
	
	fixture.runTestSuite(output, data, iterations, runs)
