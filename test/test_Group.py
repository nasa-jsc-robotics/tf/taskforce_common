import unittest

from taskforce_common import Task, EventHandler, TaskState
from taskforce_common import QueuePubSub
from taskforce_common import Event
from taskforce_common import taskforce_schema

from pprint import pprint
from taskforce_common import Group, EventRelay, GroupFactory, GroupException

import jsonschema
import time


class ExampleTask(Task):

    parameters = {'param1': True, 'param2': 'blue', 'param3': 42}
    events = ['event1', 'event_with_data']
    commands = ['command1', 'command_with_data']

    def __init__(self, name, eventPorts):
        super(ExampleTask, self).__init__(name, eventPorts)
        self.args = None
        self.kwargs = None
        self.command1Count = 0
        self.onExecuteCount = 0

    def onExecute(self):
        self.onExecuteCount += 1
        self.emit('event_with_data', 1, 2, 3, kwarg1=True, kwarg2='blue')
        return True

    def command1(self):
        self.command1Count += 1
        self.logger.info('[{}] command1'.format(self.name))

    def command_with_data(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.logger.info('[{}] args:{}, kwargs:{}'.format(self.name, *args, **kwargs))


class TestGroupTask(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_properties(self):
        childGroup = Group('childGroup', 'fileName')
        parentGroup = Group('parentGroup', 'fileName')

        childGroup.parent = parentGroup
        self.assertEqual(parentGroup, childGroup.parent)
        self.assertEqual('/parentGroup/childGroup', childGroup.path)

    def test_addEventPort(self):
        group = Group('group', 'fileName')
        group.addEventRelay('event')
        self.assertTrue('event' in group.getCommandNames())

        pubSubConn = QueuePubSub()
        port = pubSubConn.getPublisher()
        group.addEventPort(port)
        self.assertEqual(port, list(group.eventRelays['event'].eventPorts)[0])

    def test_addEventPort_recursive(self):
        group = Group('group', 'fileName1')
        group.addEventRelay('event')

        subGroup = Group('subGroup', 'fileName2')
        task = Task('task')

        subGroup.addTask(task)
        group.addSubGroup(subGroup)
        pubSubConn = QueuePubSub()
        port = pubSubConn.getPublisher()
        group.addEventPort(port, recursive=False)
        self.assertEqual(0, len(subGroup.eventPorts))
        self.assertEqual(0, len(task.eventPorts))

        group.addEventPort(port, recursive=True)
        self.assertEqual(1, len(subGroup.eventPorts))
        self.assertEqual(1, len(group.eventPorts))
        self.assertEqual(1, len(task.eventPorts))

    def test_addEventRelay(self):
        """ Test passing an event through a relay """
        connection = QueuePubSub()
        eventHandler = EventHandler('handler')
        eventHandler.addPort(connection.getSubscriber())

        aaa = ExampleTask('aaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])
        ccc = ExampleTask('ccc', eventPorts=[connection.getPublisher()])
        ddd = ExampleTask('ddd', eventPorts=[connection.getPublisher()])

        # create a group
        group1 = Group('group1', 'fileName1', [connection.getPublisher()])
        group1.addTask(aaa)
        group1.addTask(bbb)

        # add some event relays
        group1.addEventRelay('startup')
        group1.addEventRelay('complete')
        group1.subscribeEvent('startup', '<group>', 'aaa', 'startup', False)
        group1.subscribeEvent('complete', 'aaa', 'bbb', 'startup', False)
        group1.subscribeEvent('complete', 'bbb', '<group>', 'complete', False)

        self.assertTrue('complete' in group1.getCommandNames())
        self.assertTrue('startup' in group1.getCommandNames())
        self.assertTrue('complete' in group1.getEventNames())
        self.assertTrue('startup' in group1.getEventNames())

        # group2
        # create a group
        group2 = Group('group2', 'fileName1', [connection.getPublisher()])
        group2.addTask(ccc)
        group2.addTask(ddd)

        # add some event relays
        group2.addEventRelay('startup')
        group2.addEventRelay('complete')
        group2.subscribeEvent('startup', '<group>', 'ccc', 'startup', False)
        group2.subscribeEvent('complete', 'ccc', 'ddd', 'startup', False)
        group2.subscribeEvent('complete', 'ddd', '<group>', 'complete', False)

        eventHandler.registerDestination(group1.path, group1)
        eventHandler.registerDestination(group2.path, group2)

        for name, child in group1.getChildren().items():
            eventHandler.registerDestination(name, child)

        for name, child in group2.getChildren().items():
            eventHandler.registerDestination(name, child)

        for subscription in group1.getSubscriptions():
            eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination, subscription.callback, False)

        for subscription in group2.getSubscriptions():
            eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination, subscription.callback, False)

        eventHandler.subscribeEvent('complete', '/group1', '/group2', 'startup', False)
        eventHandler.startEventLoop()

        aaa.deploy()
        bbb.deploy()
        ccc.deploy()
        ddd.deploy()

        group1.startup()
        time.sleep(1)
        eventHandler.stopEventLoop()

        self.assertEqual(1, aaa.onExecuteCount)
        self.assertEqual(1, bbb.onExecuteCount)
        self.assertEqual(1, ccc.onExecuteCount)
        self.assertEqual(1, ddd.onExecuteCount)

    def test_addSubGroup_Exception(self):
        group = Group('group', 'group.group')
        self.assertRaises(GroupException, group.addSubGroup, 5)

    def test_eventRelay(self):
        group1 = Group('group1', 'group1.group')
        group2 = Group('group2', 'group2.group')
        relay = EventRelay('event', group1)

        self.assertEqual(group1, relay.parent)

        relay.parent = group2
        self.assertEqual(group2, relay.parent)

    def test_changeName(self):
        group = Group('group', 'fileName')
        subGroup = Group('subGroup', 'fileName')

        group.addSubGroup(subGroup)
        group.changeName('subGroup', 'childGroup')
        self.assertTrue('childGroup', subGroup.name)
        self.assertEqual(subGroup, group.getChild('childGroup'))

    def test_changeName_events(self):
        """ Test changing the name of different elements"""

        connection = QueuePubSub()
        eventHandler = EventHandler('handler')
        eventHandler.addPort(connection.getSubscriber())

        # create some tasks
        aaa = ExampleTask('aaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])
        ccc = ExampleTask('ccc', eventPorts=[connection.getPublisher()])
        ddd = ExampleTask('ddd', eventPorts=[connection.getPublisher()])

        # create a group, but tasks into the group
        group1 = Group('group1', 'fileName1', [connection.getPublisher()])
        group1.addTask(aaa)
        group1.addTask(bbb)

        # add some event relays
        group1.addEventRelay('startup')
        group1.addEventRelay('complete')
        group1.subscribeEvent('startup', '<group>', 'aaa', 'startup', False)
        group1.subscribeEvent('complete', 'aaa', 'bbb', 'startup', False)
        group1.subscribeEvent('complete', 'bbb', '<group>', 'complete', False)

        # group2
        # create a group
        group2 = Group('group2', 'fileName1', [connection.getPublisher()])
        group2.addTask(ccc)
        group2.addTask(ddd)

        # add some event relays
        group2.addEventRelay('startup')
        group2.addEventRelay('complete')
        group2.subscribeEvent('startup', '<group>', 'ccc', 'startup', False)
        group2.subscribeEvent('complete', 'ccc', 'ddd', 'startup', False)
        group2.subscribeEvent('complete', 'ddd', '<group>', 'complete', False)

        group1.changeName('aaa', 'aaa_aaa')
        group1.changeName('bbb', 'bbb_bbb')
        group2.changeName('ccc', 'ccc_ccc')
        group2.changeName('ddd', 'ddd_ddd')

        eventHandler.registerDestination(group1.path, group1)
        eventHandler.registerDestination(group2.path, group2)

        for path, child in group1.getChildren().items():
            eventHandler.registerDestination(path, child)

        for path, child in group2.getChildren().items():
            eventHandler.registerDestination(path, child)

        for subscription in group1.getSubscriptions():
            eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination, subscription.callback, False)

        for subscription in group2.getSubscriptions():
            eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination, subscription.callback, False)

        for name, child in group1.getChildren().items():
            eventHandler.registerDestination(name, child)

        for name, child in group2.getChildren().items():
            eventHandler.registerDestination(name, child)

        eventHandler.subscribeEvent('complete', group1.path, group2.path, 'startup', False)

        eventHandler.startEventLoop()

        aaa.deploy()
        bbb.deploy()
        ccc.deploy()
        ddd.deploy()

        group1.startup()
        time.sleep(1.0)
        eventHandler.stopEventLoop()

        self.assertEqual(1, aaa.onExecuteCount)
        self.assertEqual(1, bbb.onExecuteCount)
        self.assertEqual(1, ccc.onExecuteCount)
        self.assertEqual(1, ddd.onExecuteCount)

    def test_changeName_parameterBindings(self):
        connection = QueuePubSub()

        aaa = Task('aaa', eventPorts=[connection.getPublisher()])
        bbb = Task('bbb', eventPorts=[connection.getPublisher()])
        ccc = Task('ccc', eventPorts=[connection.getPublisher()])
        ddd = Task('ddd', eventPorts=[connection.getPublisher()])

        aaa.parameters = {'param1': None}
        bbb.parameters = {'param2': None}

        # create a group
        group1 = Group('group1', 'fileName1', [connection.getPublisher()])
        group1.addTask(aaa)
        group1.addTask(bbb)
        group1.addTask(ccc)
        group1.addTask(ddd)

        group1.bindParameter('groupKey', 'param1', 'aaa')
        group1.bindParameter('groupKey', 'param2', 'bbb')

        group1.changeName("aaa","aaa01")

        group1.setParameter({'groupKey': 'value1'})

        self.assertEqual('value1', aaa.getParameter()['param1'])
        self.assertEqual('value1', bbb.getParameter()['param2'])

    def test_existing_parameterBindings(self):
        connection = QueuePubSub()

        aaa = Task('aaa', eventPorts=[connection.getPublisher()])
        bbb = Task('bbb', eventPorts=[connection.getPublisher()])
        ccc = Task('ccc', eventPorts=[connection.getPublisher()])
        ddd = Task('ddd', eventPorts=[connection.getPublisher()])

        aaa.parameters = {'param1': None}
        bbb.parameters = {'param2': None}

        # create a group
        group1 = Group('group1', 'fileName1', [connection.getPublisher()])
        group1.addTask(aaa)
        group1.addTask(bbb)
        group1.addTask(ccc)
        group1.addTask(ddd)

        group1.setParameter({'groupKey': 'value1'})

        group1.bindParameter('groupKey', 'param1', 'aaa')
        group1.bindParameter('groupKey', 'param2', 'bbb')

        group1.changeName("aaa", "aaa01")

        self.assertEqual('value1', aaa.getParameter()['param1'])
        self.assertEqual('value1', bbb.getParameter()['param2'])

    def test_changeName_unsubscribe(self):
        # create some tasks
        aaa = Task('aaa')
        bbb = Task('bbb')

        # Create a group with the two tasks
        test_group = Group("test_group", "file_name")
        test_group.addTask(aaa)
        test_group.addTask(bbb)

        # Subscribe one task to another
        test_group.subscribeEvent('complete', 'aaa', 'bbb', 'startup', False)

        # Change the name of one of the Tasks
        test_group.changeName('bbb', 'ccc')

        test_group.unsubscribeEvent('complete', 'aaa', 'ccc', 'startup')

    def test_removeChild(self):
        group = Group('TestGroup', 'TestGroup.group')

        taskA = Task('taskA')
        taskB = Task('taskB')

        group.addTask(taskA)
        group.addTask(taskB)

        group.subscribeEvent('complete', 'taskA', 'taskB', 'startup', False)

        subscriptions = group.getSubscriptions()
        found = False
        for subscription in subscriptions:
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
        self.assertTrue(found)

        group.removeChild('taskA')

        # test that when the child is removed it also removes subscriptions
        found = False
        for subscription in group.getSubscriptions():
            if subscription.eventName == 'complete' and subscription.source == taskA.path:
                found = True
        self.assertFalse(found)

    def test_unbindParameters(self):
        group1 = Group('group1', 'fileName1')

        aaa = Task('aaa')
        bbb = Task('bbb')

        group1.addTask(aaa)
        group1.addTask(bbb)

        aaa.parameters = {'param1': None}
        bbb.parameters = {'param2': None}

        group1.bindParameter('groupKey', 'param1', 'aaa')
        group1.bindParameter('groupKey', 'param2', 'bbb')

        group1.setParameter({'groupKey': 'value1'})

        self.assertEqual('value1', aaa.getParameter()['param1'])
        self.assertEqual('value1', bbb.getParameter()['param2'])

        group1.unBindParameter('groupKey', 'param1', 'aaa')

        group1.setParameter({'groupKey': 'value2'})

        self.assertEqual('value1', aaa.getParameter()['param1'])
        self.assertEqual('value2', bbb.getParameter()['param2'])

    def test_unsubscribeEvent(self):
        aaa = Task('aaa')
        bbb = Task('bbb')

        group = Group('group', 'fileName')
        group.addTask(aaa)
        group.addTask(bbb)

        group.subscribeEvent('event_with_data', 'aaa', 'bbb', 'command_with_data', False)
        group.subscribeEvent('complete', 'aaa', 'bbb', 'startup', False)

        self.assertEqual(2, len(group.getSubscriptions()))

        group.unsubscribeEvent('complete', 'aaa', 'bbb', 'startup')

        self.assertEqual(1, len(group.getSubscriptions()))

    def test_unsubscribeEvent_exception(self):
        aaa = Task('aaa')
        bbb = Task('bbb')

        group = Group('group', 'fileName')
        group.addTask(aaa)
        group.addTask(bbb)

        group.subscribeEvent('event_with_data', 'aaa', 'bbb', 'command_with_data', False)
        group.subscribeEvent('complete', 'aaa', 'bbb', 'startup', False)

        self.assertEqual(2, len(group.getSubscriptions()))
        self.assertRaises(GroupException, group.unsubscribeEvent, 'complete', 'bbb', 'ccc', 'startup')

    def test_clearAll(self):
        group1 = Group('group1', 'group1.group')
        group2 = Group('group2', 'group2.group')

        task1 = Task('task1')
        task2 = Task('task2')
        group1.addSubGroup(group2)
        group1.addTask(task1)
        group1.addTask(task2)
        group1.subscribeEvent('event1', 'task1', 'task2', 'startup', False)

        self.assertEqual(1, len(group1.getSubscriptions()))
        self.assertEqual(2, len(group1.getTasks()))
        self.assertEqual(1, len(group1.getSubGroups()))

        group1.clearAll()
        self.assertEqual(0, len(group1.getSubscriptions()))
        self.assertEqual(0, len(group1.getTasks()))
        self.assertEqual(0, len(group1.getSubGroups()))

    def test_getObject(self):
        group = Group('group', 'fileName')
        subGroup = Group('subGroup', 'fileName')
        task = Task('task')
        subTask = Task('subTask')

        subGroup.addTask(subTask)
        group.addTask(task)
        group.addSubGroup(subGroup)

        self.assertEqual(task, group.getChild('/group/task', recursive=True))
        self.assertEqual(subGroup, group.getChild('/group/subGroup', recursive=True))
        self.assertEqual(subTask, group.getChild('/group/subGroup/subTask', recursive=True))

        self.assertEqual(task, group.getChild('task'))
        self.assertEqual(subGroup, group.getChild('subGroup'))

        self.assertRaises(Exception, group.getChild, 'group')
        self.assertRaises(Exception, group.getChild, '<group>')
        self.assertEqual(group, group.getBlocks('<group>'))

    def test_getSubscription(self):
        """ Test getSubscription method in the Group """
        connection = QueuePubSub()

        aaa = ExampleTask('aaaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])

        group = Group('group', 'fileName', [connection.getPublisher()])
        group.addTask(aaa)
        group.addTask(bbb)

        # test valid subscription
        group.subscribeEvent('event_with_data', 'aaaa', 'bbb', 'command_with_data', False)
        group.subscribeEvent('event2', 'aaaa', 'bbb', 'command2', True)
        subscription = group.getSubscription('event_with_data', 'aaaa', 'bbb', 'command_with_data')
        self.assertEqual(subscription.eventName, 'event_with_data')
        self.assertEqual(subscription.source, 'aaaa')
        self.assertEqual(subscription.destination, 'bbb')
        self.assertEqual(subscription.callback, 'command_with_data')
        self.assertEqual(subscription.breakpoint, False)

        subscription = group.getSubscription('event2', 'aaaa', 'bbb', 'command2')
        self.assertEqual(subscription.eventName, 'event2')
        self.assertEqual(subscription.source, 'aaaa')
        self.assertEqual(subscription.destination, 'bbb')
        self.assertEqual(subscription.callback, 'command2')
        self.assertEqual(subscription.breakpoint, True)

        # test invalid subscription
        subscription = group.getSubscription('event_with_data', 'aaaa', 'bbb', 'command')
        self.assertEqual(subscription, None)

    def test_parameters(self):
        connection = QueuePubSub()

        aaa = Task('aaa', eventPorts=[connection.getPublisher()])
        bbb = Task('bbb', eventPorts=[connection.getPublisher()])
        ccc = Task('ccc', eventPorts=[connection.getPublisher()])
        ddd = Task('ddd', eventPorts=[connection.getPublisher()])

        aaa.parameters = {'param1':None}
        bbb.parameters = {'param2': None}

        # create a group
        group1 = Group('group1', 'fileName1')
        group1.addTask(aaa)
        group1.addTask(bbb)
        group1.addTask(ccc)
        group1.addTask(ddd)

        group1.bindParameter('groupKey', 'param1', 'aaa')
        group1.bindParameter('groupKey', 'param2', 'bbb')

        group1.setParameter({'groupKey':'value1'})

        self.assertEqual('value1', aaa.getParameter()['param1'])
        self.assertEqual('value1', bbb.getParameter()['param2'])

    def test_parameters_nested(self):
        connection = QueuePubSub()

        aaa = ExampleTask('aaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])

        group = Group('group', 'fileName')
        group.addTask(aaa)
        group.addTask(bbb)
        group.bindParameter('groupParam1', 'param1','aaa')
        group.bindParameter('groupParam1', 'param2', 'bbb')

        group.setParameter({'groupParam1': 'groupValue'})

        superGroup = Group('superGroup', 'filename')
        superGroup.addSubGroup(group)
        superGroup.bindParameter('superParam', 'groupParam1', 'group')
        superGroup.setParameter({'superParam': 'superValue'})

        self.assertEqual(superGroup.getParameter()['superParam'], group.getParameter()['groupParam1'])
        self.assertEqual(superGroup.getParameter()['superParam'], aaa.getParameter()['param1'])
        self.assertEqual(superGroup.getParameter()['superParam'], bbb.getParameter()['param2'])

    def test_serialize(self):
        connection = QueuePubSub()

        aaa = ExampleTask('aaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])
        ccc = ExampleTask('ccc', eventPorts=[connection.getPublisher()])
        ddd = ExampleTask('ddd', eventPorts=[connection.getPublisher()])

        # create a group
        group1 = Group('group1', 'fileName1')
        group1.addTask(aaa)
        group1.addTask(bbb)
        group1.addTask(ccc)
        group1.addTask(ddd)

        # add some event relays
        group1.addEventRelay('startup')
        group1.addEventRelay('complete')
        group1.subscribeEvent('startup', 'group1', 'aaa', 'startup', False)
        group1.subscribeEvent('complete', 'aaa', 'bbb', 'startup', False)
        group1.subscribeEvent('complete', 'bbb', 'group1', 'complete', False)
        group1.subscribeEvent('startup', 'group1', 'ccc', 'command1', False)
        group1.subscribeEvent('startup', 'group1', 'ddd', 'shutdown', False)

        group1.bindParameter('groupKey', 'param1', 'aaa')
        group1.bindParameter('groupKey', 'param2', 'bbb')
        group1.setParameter({'groupKey': 'value1'})

        data = group1.serialize()
        pprint(data)

        self.assertEqual(None, jsonschema.validate(data, taskforce_schema))

        self.assertEqual({'groupKey': 'value1'}, data['group']['parameters'])

    def test_status(self):
        group = Group('group', 'group.group')
        status = group.status()

        self.assertEqual('group.group', status['fileName'])
        self.assertEqual('group.group', group.status('fileName'))

    # @unittest.skip("")
    def test_subscribeEvent(self):
        """ Test subscribeEvent method in the Group """
        connection = QueuePubSub()
        eventHandler = EventHandler('handler')
        eventHandler.addPort(connection.getSubscriber())

        aaa = ExampleTask('aaaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])

        group = Group('group', 'fileName', [connection.getPublisher()])
        group.addTask(aaa)
        group.addTask(bbb)

        group.subscribeEvent('event_with_data', 'aaaa', 'bbb', 'command_with_data', False)

        for name, object in group.getChildren().items():
            eventHandler.registerDestination(name, object)
        eventHandler.registerDestination(group.name, group)

        for subscription in group.getSubscriptions():
            eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination,
                                        subscription.callback, False)

        aaa.deploy()
        bbb.deploy()

        eventHandler.startEventLoop()

        aaa.startup()
        time.sleep(0.1)
        self.assertEqual((1, 2, 3), bbb.args)
        self.assertEqual({'kwarg1': True, 'kwarg2': 'blue'}, bbb.kwargs)
        eventHandler.stopEventLoop()

    def test_subscribeEventTaskSameNameAsGroup(self):
        """ Test subscribeEvent method in the Group """
        connection = QueuePubSub()
        eventHandler = EventHandler('handler')
        eventHandler.addPort(connection.getSubscriber())

        aaa = ExampleTask('aaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])

        group = Group('aaa', 'fileName', [connection.getPublisher()])
        group.addTask(aaa)
        group.addTask(bbb)

        group.subscribeEvent('event_with_data', 'aaa', 'bbb', 'command_with_data', False)

        for name, object in group.getChildren().items():
            eventHandler.registerDestination(name, object)
        eventHandler.registerDestination(group.name, group)

        for subscription in group.getSubscriptions():
            eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination,
                                        subscription.callback, False)

        aaa.deploy()
        bbb.deploy()

        eventHandler.startEventLoop()

        aaa.startup()
        time.sleep(0.5)
        self.assertEqual((1, 2, 3), bbb.args)
        self.assertEqual({'kwarg1': True, 'kwarg2': 'blue'}, bbb.kwargs)
        eventHandler.stopEventLoop()

    def test_subscribeEventGroupSameNameAsGroup(self):
        """ Test subscribeEvent method in the Group """
        connection = QueuePubSub()
        eventHandler = EventHandler('handler')
        eventHandler.addPort(connection.getSubscriber())

        aaa = ExampleTask('aaa', eventPorts=[connection.getPublisher()])
        bbb = ExampleTask('bbb', eventPorts=[connection.getPublisher()])

        group = Group('aaa', 'fileName', [connection.getPublisher()])
        group.addTask(aaa)
        group.addTask(bbb)

        group.subscribeEvent('startup', '<group>', 'bbb', 'startup', False)

        for name, object in group.getChildren().items():
            eventHandler.registerDestination(name, object)
        eventHandler.registerDestination(group.name, group)

        for subscription in group.getSubscriptions():
            eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination,
                                        subscription.callback, False)

        aaa.deploy()
        bbb.deploy()

        eventHandler.startEventLoop()

        group.startup()
        time.sleep(0.5)
        self.assertEqual(TaskState.DEPLOYED, aaa.state)
        self.assertEqual(TaskState.COMPLETE, bbb.state)

        eventHandler.stopEventLoop()

    def test_group(self):
        foo_task = Task("foo")
        bar_task = Task("bar")
        foo_group = Group("foo", "foo.group")
        foo_group.addTask(foo_task)
        foo_group.addTask(bar_task)
        foo_group.subscribeEvent("event", "foo", "bar", "startup", False)
        definition = foo_group.serialize()

        factory = GroupFactory()
        group = factory.buildDefinition(definition)
        pass


if __name__ == "__main__":

    unittest.main()
    #test_events()
    #test_parameters()
    #test_Task()
    #test_nestedParameters()
