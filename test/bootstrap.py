################
##
## Use this file to generate two_cows.group and herd.group for bootstrapping development on ver 1.5 of taskforce
##
################

from taskforce_common import QueuePubSub
from taskforce_common import EventHandler

from taskforce_common import TaskGroup, EventRelay
from task_library.Cowtime import Cowtime
from copy import deepcopy
from pprint import pprint

from taskforce_common.utils import saveJsonFile

connection = QueuePubSub()
eventHandler = EventHandler('handler')
eventHandler.addPort(connection.getSubscriber())

clara = Cowtime('clara')
clementine = Cowtime('clementine')

group1 = TaskGroup("TwoCows", "block_library/two_cows.group", [eventHandler])
group1.addEventRelay("startup")
group1.addEventRelay("complete")

group1.addTask(clara)
group1.addTask(clementine)
group1.subscribeEvent("startup", "TwoCows", "clara", "startup")
group1.subscribeEvent("complete", "clara", "clementine", "startup")
group1.subscribeEvent("complete", "clementine", "TwoCows", "complete")


group2 = TaskGroup("TwoCows", "block_library/two_cows.group",[eventHandler])
group2.addEventRelay("startup")
group2.addEventRelay("complete")

group2.addTask(clara)
group2.addTask(clementine)
group2.subscribeEvent("startup", "TwoCows", "clara", "startup")
group2.subscribeEvent("complete", "clara", "clementine", "startup")
group2.subscribeEvent("complete", "clementine", "TwoCows", "complete")

pprint(group1.serialize())

saveJsonFile("block_library/two_cows.group", group1.serialize())

group1.changeName("TwoCows","TwoCows01")
group2.changeName("TwoCows","TwoCows02")

herd = TaskGroup("herd","block_library/herd.group",[eventHandler])
herd.addSubGroup(group1)
herd.addSubGroup(group2)
herd.subscribeEvent("complete","TwoCows01","TwoCows02","startup")


pprint(herd.serialize())

saveJsonFile("block_library/herd.group",herd.serialize())