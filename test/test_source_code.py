import unittest

import os
import sys


from taskforce_common.utils import createObject

TEST_FOLDER = os.path.dirname(os.path.realpath(__file__))
TEST_MODULE = os.path.join(TEST_FOLDER,'TestClass.py')

sys.path += [TEST_FOLDER] + sys.path

CODE_1 = """class TestClass(object):
    def __init__(self, val, kw='keyword'):
        self.val = val
        self.other = 'green'
        self.kw = kw

    def getVal(self):
        return self.val

    def getKw(self):
        return self.kw
"""

CODE_2 = """class TestClass(object):
    def __init__(self, val, kw='keyword'):
        self.val = val
        self.other = 'blue'
        self.kw = kw

    def getVal(self):
        return self.val

    def getKw(self):
        return self.kw

"""

class TestCreateObject(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip("reasons")
    def test_createObject(self):
        with open(TEST_MODULE, 'w') as f_ptr:
            f_ptr.write(CODE_1)

        code1Obj = createObject('TestClass', 'TestClass', args=(1.234,), kwargs={'kw':'test'}, forceReload=True)

        self.assertEqual(1.234, code1Obj.getVal())
        self.assertEqual('green', code1Obj.other)
        self.assertEqual('test', code1Obj.getKw())

        with open(TEST_MODULE, 'w') as f_ptr:
            f_ptr.write(CODE_2)

        code2Obj = createObject('TestClass', 'TestClass', args=(1.234,), kwargs= {'kw':'test'}, forceReload=True)

        self.assertEqual(1.234,  code2Obj.getVal())
        self.assertEqual('blue', code2Obj.other)
        self.assertEqual('test', code2Obj.getKw())



if __name__ == "__main__":
    unittest.main()