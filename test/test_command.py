import unittest
import time

from taskforce_common import Server, PipeReqRep, ExceptionMessage
from taskforce_common import CommandHandler, Command, Response, ErrorResponse, CommandException, CommandInterface

TestClassName = 'TestCommandHandler'

class FakeServer(Server):
    def reply(self, response):
        pass

class TestCommand(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_hash(self):
        command1 = Command(name='command1', source='source1', destination='destination1')
        command2 = Command(name='command2', source='source2', destination='destination2')

        cmd_dict = {command1: 'a', command2: 'b'}

        self.assertEqual('a', cmd_dict[command1])
        self.assertEqual('b', cmd_dict[command2])

        cmd_dict[Command(name='command1', source='source3', destination='destination1')] = 'c'
        self.assertEqual('a', cmd_dict[command1])

    def test_equal(self):
        command1 = Command(name='command1', source='source1', destination='destination1')
        command2 = Command(name='command1', source='source1', destination='destination1')

        self.assertTrue(command1 == command2)
        self.assertFalse(command1.__eq__('apple'))

    def test_not_equal(self):
        command1 = Command(name='command1', source='source1', destination='destination1')
        command2 = Command(name='command1', source='source1', destination='destination1')

        self.assertFalse(command1 != command2)
        self.assertTrue(command1 != 'apple')
        self.assertTrue(command1 != Command(name='command2', source='source1', destination='destination1'))


class TestCommandHandler(unittest.TestCase):

    def setUp(self):
        self.count1 = 0
        self.count2 = 0
        self.count3 = 0
        
        self.conn = PipeReqRep()
        self.server = self.conn.getServer()
        self.client = self.conn.getClient()

        self.command1 = Command(name='callback1', destination=TestClassName)
        self.command2 = Command(name='callback2', destination=TestClassName,args=('Charlie',))
        self.command3 = Command(name='callback3', destination=TestClassName,args=('Delta',),kwargs={'value':10})

        self.response1 = Response(name='response1')
        self.response2 = Response(name='response2')
        self.response3 = Response(name='response3')

    def tearDown(self):
        pass

    #@unittest.skip('reason...')
    def test_CommandHandler(self):
        # should raise exception when trying to construct with non-server
        self.assertRaises(CommandException, CommandHandler, 'testName', [self.client])

        commandHandler = CommandHandler('testName', [self.server])
        self.assertEqual(commandHandler.servers[0], self.server)
        self.assertEqual(commandHandler.commandLoopRunning, False)
        self.assertEqual(commandHandler.getObjects(), {})

    #@unittest.skip('reason...')
    def test_commandLoop(self):
        commandHandler = CommandHandler('name',[self.server])

        commandHandler.registerDestination(TestClassName, self)

        # Start the handler, and assert that it has started
        self.assertFalse(commandHandler.commandLoopRunning)
        commandHandler.startCommandLoop()
        self.assertTrue(commandHandler.commandLoopRunning)
        time.sleep(0.5)
        #self.assertTrue(commandHandler.commandThread.is_alive())

        # Check that when you start when started, nothing really happens
        # i.e., the command loop thread is the same thread
        commandLoopThread = commandHandler.commandThreads[0]
        commandHandler.startCommandLoop()
        self.assertEqual(commandLoopThread, commandHandler.commandThreads[0])
        self.assertTrue(commandLoopThread.is_alive())

        # Test request reply of three different commands
        self.assertEqual(self.client.request(self.command1).data,'callback1')
        self.assertEqual(self.client.request(self.command2).data,'callback2')
        self.assertEqual(self.client.request(self.command3).data,'callback3')

        # Test request / reply of an unrecognized command
        response = self.client.request(Command(name='blarg'))
        self.assertTrue(isinstance(response,ExceptionMessage))
        commandHandler.stopCommandLoop()

    #@unittest.skip('reason...')
    def test_commandLoop_exception(self):
        commandHandler = CommandHandler('name',[self.server])
        commandHandler.registerDestination(TestClassName, self)

        commandHandler.startCommandLoop()
        time.sleep(0.1)

        response = self.client.request('Not a command')
        self.assertTrue(isinstance(response,ErrorResponse))
        self.assertEqual(response.data, 'Received a non-command object')

        commandHandler.stopCommandLoop()

    def test_getObjects(self):
        commandHandler = CommandHandler('name', [self.server])
        commandHandler.registerDestination(TestClassName, self)
        self.assertEqual({TestClassName:self}, commandHandler.getObjects())

    #@unittest.skip('reason...')
    def test_handleCommand(self):
        commandHandler = CommandHandler('name', [FakeServer()])

        commandHandler.registerDestination(TestClassName, self)

        #commandHandler.server.reply_conn = self.client.conn
        commandHandler.handleCommand(self.command1)
        self.assertEqual(self.count1, 1)
        self.assertEqual(self.count2, 0)
        self.assertEqual(self.count3, 0)

        commandHandler.handleCommand(self.command2)
        self.assertEqual(self.count1, 1)
        self.assertEqual(self.count2, 1)
        self.assertEqual(self.count3, 0)

        commandHandler.handleCommand(self.command3)
        self.assertEqual(self.count1, 1)
        self.assertEqual(self.count2, 1)
        self.assertEqual(self.count3, 1)

        self.assertRaises(CommandException, commandHandler.handleCommand, self.response1)

    #@unittest.skip('reason...')
    def test_startCommandLoop_no_command_port(self):
        # Assert raise when starting command loop without setting command port
        commandHandler = CommandHandler('name')
        self.assertRaises(CommandException, commandHandler.startCommandLoop)

    #@unittest.skip('reason...')
    def test_commandInterface(self):
        commandHandler = CommandHandler('name', [self.server])

        commandHandler.registerDestination(TestClassName, self)

        commandInterface = CommandInterface('test', self.client)

        commandHandler.startCommandLoop()
        time.sleep(0.2)

        self.assertEqual(commandInterface.sendCommand('callback1', TestClassName),'callback1')
        self.assertEqual(commandInterface.sendCommand('callback2', TestClassName, 'Lima'),'callback2')
        self.assertEqual(commandInterface.sendCommand('callback3', TestClassName, 'Lima',value=10),'callback3')

        commandHandler.stopCommandLoop()

    def callback1(self):
        self.count1 = self.count1 +1
        return 'callback1'

    def callback2(self, name):
        self.count2 = self.count2 +1
        self.name2  = name
        return 'callback2'

    def callback3(self, name, value=0):
        self.count3 = self.count3 +1
        self.name3  = name
        self.value3 = value
        return 'callback3'
