import unittest
import os

from taskforce_common.utils import FileMap

TEST_FOLDER = os.path.dirname(os.path.abspath(__file__))

FILE_MAP_TEST_FOLDER = os.path.join(TEST_FOLDER,'file_map_test')

root_dir1 = os.path.join(FILE_MAP_TEST_FOLDER,'root_dir1')
root_dir2 = os.path.join(FILE_MAP_TEST_FOLDER,'root_dir2')


class TestFileMap(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_addFolder(self):
        file_map = FileMap()

        # add one folder
        file_map.addFolder(root_dir1)
        self.assertEqual({'root_dir1':root_dir1}, file_map.getFolders())

        # now, add another
        file_map.addFolder(root_dir2)
        self.assertEqual({'root_dir1': root_dir1,
                          'root_dir2': root_dir2}, file_map.getFolders())

        # add and existing folder
        file_map.addFolder(root_dir2)
        self.assertEqual({'root_dir1': root_dir1,
                          'root_dir2': root_dir2}, file_map.getFolders())

    def test_filters_py_files(self):

        # match all the .py files
        expr = '.*py\Z'

        file_map = FileMap(filters=[expr])
        file_map.addFolder(root_dir1)

        map = file_map.getMap()
        test_map = {'root_dir1/subdir1/__init__.py': root_dir1 + '/subdir1/__init__.py',
                    'root_dir1/subdir1/file1.py':    root_dir1 + '/subdir1/file1.py',
                    'root_dir1/subdir1/file2.py':    root_dir1 + '/subdir1/file2.py'}
        self.assertEqual(test_map, map)

    def test_filters_multiple(self):

        # match all the .py files
        expr = ['.*py\Z','(?!__init__.py)']

        file_map = FileMap(filters=expr)
        file_map.addFolder(root_dir1)

        map = file_map.getMap()

        for k,v in map.items():
            print k, ':', v

        test_map = {'root_dir1/subdir1/file1.py': root_dir1 + '/subdir1/file1.py',
                    'root_dir1/subdir1/file2.py': root_dir1 + '/subdir1/file2.py'}
        self.assertEqual(test_map, map)

    def test_filters_multiple2(self):

        # match all the .py files and the .bin files.  However, ignore all the __init__.py files
        expr = ['(.*py)\Z|(.*bin)\Z', '(?!__init__.py)']

        file_map = FileMap(filters=expr)
        file_map.addFolder(root_dir1)

        map = file_map.getMap()

        for k, v in map.items():
            print k, ':', v

        test_map = {
            'root_dir1/subdir1/file1.py': root_dir1 + '/subdir1/file1.py',
            'root_dir1/subdir1/file2.py': root_dir1 + '/subdir1/file2.py',
            'root_dir1/test.bin' : root_dir1 + '/test.bin'}
        self.assertEqual(test_map, map)

    def test_removeFolder(self):
        file_map = FileMap()

        # add one folder
        file_map.addFolder(root_dir1)

        # now, add another
        file_map.addFolder(root_dir2)

        # add and existing folder
        file_map.addFolder(root_dir2)

        file_map.removeFolder(root_dir1)
        self.assertEqual({'root_dir2': root_dir2}, file_map.getFolders())

        file_map.removeFolder(root_dir2)
        self.assertEqual({}, file_map.getFolders())


if __name__ == "__main__":
    unittest.main()