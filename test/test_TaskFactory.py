import unittest
import jsonschema
from taskforce_common import TaskFactory, TaskFactoryException

class TestTaskFactory(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip('skip')
    def test_createTaskFromDefinition(self):
        factory = TaskFactory()

        definition = {
            "task": {
                "name": "task1",
                "module": "task_library.MyTask",
                "class": "MyTask",
                "parameters" : {
                    "param1" : "blue",
                    "param2" : [1,2,3]
                }
            }
        }

        task = factory.createTaskFromDefinition(definition, validate=True)

        self.assertEqual(task.name, "task1")
        self.assertEqual(task.path, "/task1")
        self.assertEqual(task.__class__.__module__, 'task_library.MyTask')
        self.assertEqual(task.__class__.__name__, 'MyTask')
        self.assertEqual(task.getParameter()["param1"], "blue")
        self.assertEqual(task.getParameter()["param2"], [1, 2, 3])

    #@unittest.skip('redo')
    def test_createTaskFromDefinition_no_validate(self):
        factory = TaskFactory()

        definition = {
            "task": {
                "name": "task1",
                "module": "task_library.MyTask",
                "class": "MyTask",
                "parameters": {
                    "param1": "blue",
                    "param2": [1, 2, 3]
                }
            }
        }

        task = factory.createTaskFromDefinition(definition, validate=False)

        self.assertEqual(task.name, "task1")
        self.assertEqual(task.path, "/task1")
        self.assertEqual(task.__class__.__module__, 'task_library.MyTask')
        self.assertEqual(task.__class__.__name__, 'MyTask')
        self.assertEqual(task.getParameter()["param1"], "blue")
        self.assertEqual(task.getParameter()["param2"], [1, 2, 3])

    #@unittest.skip('skip')
    def test_createTaskFromDefinition_wrong_header_exception(self):
        factory = TaskFactory()

        definition = {
            "connection": {
                "name": "cmdConnection",
                "module": "taskforce_common",
                "class": "PipePair",

            }
        }

        self.assertRaises(TaskFactoryException, factory.createTaskFromDefinition, definition, validate=False)

    #@unittest.skip('skip')
    def test_TaskFactory_creation_exception(self):
        # This test asserts an exception is thrown when the TaskFactory cannot create an instance
        factory = TaskFactory()

        definition = {
            "task": {
                "name": "task2",
                "module": "bad_module.Task",
                "class": "Task",
                "parameters" : {}
            }
        }
        self.assertRaises(TaskFactoryException, factory.createTaskFromDefinition, definition, validate=False)

    #@unittest.skip('skip')
    def test_TaskFactory_creation_no_header_exception(self):
        factory = TaskFactory()

        definition = {
            "name": "task1",
            "module": "taskforce_common.task.Task",
            "class": "Task",
            "parameters" : {}
        }
        self.assertRaises(jsonschema.ValidationError, factory.createTaskFromDefinition, definition)

if __name__ == "__main__":
    unittest.main()