import unittest
import logging
from threading import Thread

from taskforce_common import logcfg
from taskforce_common import Command, CommandInterface, Response, ErrorResponse
from taskforce_common import Task, TaskState, TaskException, Event
from taskforce_common import QueuePubSub, QueueReqRep
from taskforce_common import ValidationFactory
import time


class TaskSubclass(Task):

    events = ['onExecute']

    def __init__(self, name):
        super(TaskSubclass,self).__init__(name)
        self.reset()

    def onInitEvent(self):
        self.on_init_event = True

    def onExecuteEvent(self):
        self.on_execute_event = True

    def onStartupEvent(self):
        self.on_startup_event = True

    def onStopEvent(self):
        self.on_stop_event = True

    def onCompleteEvent(self):
        self.on_complete_event = True

    def onDeploy(self):
        self.on_deploy = True
        return True

    def onInit(self):
        self.on_init = True
        return True

    def onStartup(self):
        self.on_startup = True
        return True

    def onExecute(self):
        self.on_execute = True
        self.emit('onExecute')
        return True

    def onStop(self):
        self.on_stop = True
        return True

    def reset(self):
        self.on_deploy = False
        self.on_init = False
        self.on_execute = False
        self.on_startup = False
        self.on_stop = False

        self.on_init_event = False
        self.on_execute_event = False
        self.on_startup_event = False
        self.on_stop_event = False
        self.on_complete_event = False
        self.on_parameter_changed = False
        self.count_on_parameter_changed = 0

    def onParameterChange(self, oldParams, newParams):
        self.on_parameter_changed = True
        self.count_on_parameter_changed += 1
        return True


class TaskRunForever(Task):
    init_count = 0
    startup_count = 0
    execute_count = 0
    stop_count = 0
    complete_count = 0

    def __init__(self, name=None, commandServerPorts=[]):
        Task.__init__(self,name,commandServerPorts)
        self.parameters['jack'] = ''
        self.parameters['jill'] = ''

    def onInit(self, event=None):
        self.init_count = self.init_count + 1
        return True

    def onStartup(self, event=None):
        self.startup_count = self.startup_count + 1
        return True

    def onExecute(self, event=None):
        self.execute_count = self.execute_count + 1
        while(self.state == TaskState.RUNNING):
                    pass

    def onStop(self, event=None):
        self.stop_count = self.stop_count + 1
        return True

    def onComplete(self, event=None):
        self.complete_count = self.complete_count + 1
        return True

class TestTask(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip('reasons...')
    def testSignalTransitions(self):
        connection = QueuePubSub()
        subscriber = connection.getSubscriber()

        testTaskName = 'testTask'
        task = TaskSubclass(testTaskName)
        task.addEventPort(connection.getPublisher())
        task.deploy()


        # Assert that on deploy initialized, executed, startup, or stopped has not run
        self.assertFalse(task.on_init)
        self.assertFalse(task.on_execute)
        self.assertFalse(task.on_startup)
        self.assertFalse(task.on_stop)

        # assert that startup returns true
        self.assertTrue(task.startup())

        # assert that the task has run onInit, onStartup, and onExecute,
        # but not onStop
        self.assertTrue(task.on_init)
        self.assertTrue(task.on_execute)
        self.assertTrue(task.on_startup)
        self.assertFalse(task.on_stop)

        self.assertEqual(subscriber.receive().name, 'initialized')
        self.assertEqual(subscriber.receive().name, 'started')
        self.assertEqual(subscriber.receive().name, 'onExecute')
        self.assertEqual(subscriber.receive().name, 'complete')


        # The task is a one-shot, so task.stop should return an error
        reply = task.stop()
        self.assertEqual(subscriber.receive().name, 'stopped')
        self.assertEqual(reply, True)
        self.assertEqual(task.shutdown(), True)
        self.assertEqual(task.state, TaskState.SHUTDOWN)
        self.assertEqual(subscriber.receive().name, 'shutdown')

    #@unittest.skip('reasons...')
    def testChangeName(self):
        task = TaskRunForever('testChangeName')

        task.changeName('testChangeName', 'newTaskName')
        self.assertEqual('newTaskName', task.name)

        task.changeName('otherTask', 'newOtherTask')
        self.assertEqual('newTaskName', task.name)

    #@unittest.skip('reasons...')
    def testSerialize(self):
        task = TaskRunForever('testSerialize')

        task.setParameter({"jack":"is_here", "jill": ""})

        definition = task.serialize()

        self.assertEqual('TaskRunForever', definition['task']['class'])
        self.assertEqual('testSerialize', definition['task']['name'])
        self.assertEqual('test_task', definition['task']['module'])
        self.assertEqual(definition['task']['parameters'], {"jack":"is_here", "jill":""})

        definition = task.serialize(truncate_names=False)

        self.assertEqual('TaskRunForever', definition['task']['class'])
        self.assertEqual('testSerialize', definition['task']['name'])
        self.assertEqual('test_task', definition['task']['module'])

        validator = ValidationFactory()
        self.assertTrue(validator.validate(definition))

    #@unittest.skip('reasons...')
    def testStartupWhileRunning(self):
        logcfg.setLevel('INFO')

        task = TaskRunForever('testStartupWhileRunning')
        task.deploy()
        taskThread = Thread(target=task.startup)
        taskThread.daemon = True
        taskThread.start()
        while task.state < TaskState.RUNNING:
            time.sleep(0.1)

        self.assertEqual(task.init_count, 1)
        self.assertEqual(task.startup_count, 1)
        self.assertEqual(task.stop_count, 0)
        self.assertEqual(task.complete_count, 0)

        time.sleep(0.1)
        self.assertFalse(task.startup())

        self.assertEqual(task.init_count, 1)
        self.assertEqual(task.startup_count, 1)
        self.assertEqual(task.stop_count, 0)
        self.assertEqual(task.complete_count, 0)

        task.stop()
        taskThread.join()


    #@unittest.skip('reasons...')
    def testFailCustomOnStartup(self):
        class FailOnStartup(TaskRunForever):
            def onStartup(self, event=None):
                self.startup_count = self.startup_count + 1
                return False

        task = FailOnStartup('testFailOnStartup')
        task.deploy()

        self.assertFalse(task.startup())
        self.assertEqual(task.init_count, 1)
        self.assertEqual(task.startup_count, 1)
        self.assertEqual(task.execute_count, 0)
        self.assertEqual(task.stop_count, 0)
        self.assertEqual(task.complete_count,0)
        task.shutdown()

    #@unittest.skip('reasons...')
    def test_addCommandPort(self):
        conn = QueueReqRep()

        task = Task('testSetCommandServerPort')
        clientPort = conn.getClient()
        task.addCommandPort(clientPort)
        self.assertTrue(clientPort in task.commandPorts)

    #@unittest.skip('reasons...')
    def testDefaultOnInit(self):
        task = Task('testDefaultOnInit')
        self.assertTrue(task.onInit())

    #@unittest.skip('reasons...')
    def testDefaultOnExecute(self):
        task = Task('testDefaultOnExecute')
        self.assertTrue(task.onExecute())

    #@unittest.skip('reasons...')
    def testDefaultOnStartup(self):
        task = Task('testDefaultOnStartup')
        self.assertTrue(task.onStartup())

    #@unittest.skip('reasons...')
    def testDefaultOnStop(self):
        task = Task('testDefaultOnStop')
        self.assertTrue(task.onStop())

    #@unittest.skip('reasons...')
    def testParameter(self):
        task1 = TaskRunForever('testParameter1')
        task2 = TaskRunForever('testParameter2')

        # test that setting task in one instance does not change the value in another instance
        task1.setParameter({"jack":"is_here", "jill":42})
        task2.setParameter({"jack":"is_there", "jill":-1})
        self.assertEqual(task1.getParameter("jack"), {"jack":"is_here"})
        self.assertEqual(task1.getParameter("jill"), {"jill":42})
        self.assertEqual(task2.getParameter("jack"), {"jack":"is_there"})
        self.assertEqual(task2.getParameter("jill"), {"jill":-1})

        #self.assertRaises(TaskException,task1.setParameter,{"junk":"hi"})

    def testEmitEvent(self):
        connection = QueuePubSub()
        publisher = connection.getPublisher()
        subscriber = connection.getSubscriber()
        subscriber.connect()

        task = Task('testTask')
        task.addEventPort(publisher)

        task.emit('started')

        event = subscriber.receive()
        self.assertEqual('started', event.name)

    def test_stateString(self):
        task = Task('test')

        task._state = TaskState.COMPLETE
        self.assertEqual("COMPLETE", task.stateString)
        self.assertEqual("COMPLETE", task.getStateString())

        task._state = TaskState.DEPLOYED
        self.assertEqual("DEPLOYED", task.stateString)
        self.assertEqual("DEPLOYED", task.getStateString())

        task._state = TaskState.INITIALIZED
        self.assertEqual("INITIALIZED", task.stateString)
        self.assertEqual("INITIALIZED", task.getStateString())

        task._state = TaskState.RUNNING
        self.assertEqual("RUNNING", task.stateString)
        self.assertEqual("RUNNING", task.getStateString())
        task._state = TaskState.SHUTDOWN

        self.assertEqual("SHUTDOWN", task.stateString)

    def test_onInit(self):
        task = TaskSubclass('test')
        self.assertEqual(TaskState.NOTDEPLOYED, task.state)

        task.deploy()
        task.init()
        self.assertTrue(task.on_deploy)
        self.assertTrue(task.on_init)
        self.assertFalse(task.on_startup)
        self.assertFalse(task.on_execute)
        self.assertFalse(task.on_stop)

    def test_onStartup(self):
        task = TaskSubclass('test')
        self.assertEqual(TaskState.NOTDEPLOYED, task.state)
        task.deploy()
        task.startup()
        self.assertTrue(task.on_deploy)
        self.assertTrue(task.on_init)
        self.assertTrue(task.on_startup)
        self.assertTrue(task.on_execute)
        self.assertFalse(task.on_stop)

    def test_onStop(self):
        task = TaskSubclass('test')
        self.assertEqual(TaskState.NOTDEPLOYED, task.state)
        task.deploy()
        task.stop()
        self.assertTrue(task.on_deploy)
        self.assertFalse(task.on_init)
        self.assertFalse(task.on_startup)
        self.assertFalse(task.on_execute)
        self.assertFalse(task.on_stop)
        task.startup()
        task.stop()
        self.assertTrue(task.on_deploy)
        self.assertTrue(task.on_init)
        self.assertTrue(task.on_startup)
        self.assertTrue(task.on_execute)
        self.assertTrue(task.on_stop)

    def test_onParameterChangeSuccess(self):
        task = TaskSubclass('test')
        task.parameters = {'param1': 1, 'param2': 2}

        # onParameterChange should only happen when task is deployed
        task.deploy()
        self.assertTrue(task.on_deploy)

        self.assertFalse(task.on_parameter_changed)
        self.assertTrue(task.setParameter({'param1': 2}))
        self.assertTrue(task.on_parameter_changed)
        self.assertEqual(task.count_on_parameter_changed, 1)

    def test_onParameterChangeFailed(self):
        task = TaskSubclass('test')
        task.parameters = {'param1': 1, 'param2': 2}

        # onParameterChange should only happen when task is deployed
        task.deploy()
        self.assertTrue(task.on_deploy)

        self.assertFalse(task.on_parameter_changed)
        self.assertFalse(task.setParameter({'param3': 1}))
        self.assertFalse(task.on_parameter_changed)
        self.assertEqual(task.count_on_parameter_changed, 0)

    def test_onParameterChangeSame(self):
        task = TaskSubclass('test')
        task.parameters = {'param1': 1, 'param2': 2}

        # onParameterChange should only happen when task is deployed
        task.deploy()
        self.assertTrue(task.on_deploy)

        self.assertFalse(task.on_parameter_changed)
        self.assertTrue(task.setParameter({'param1': 1}))
        self.assertFalse(task.on_parameter_changed)
        self.assertEqual(task.count_on_parameter_changed, 0)

    def test_onParamterChangeNotDeployed(self):
        task = TaskSubclass('test')
        task.parameters = {'param1': 1, 'param2': 2}

        self.assertFalse(task.on_parameter_changed)
        self.assertTrue(task.setParameter({'param1': 2}))
        self.assertFalse(task.on_parameter_changed)
        self.assertEqual(task.count_on_parameter_changed, 0)

    def test_addEventPort_exception(self):
        task = Task('test')

        port = 'NotAPort'
        self.assertRaises(TaskException, task.addEventPort, [port])

    def test_get_set_LoggerLevel(self):
        task = Task('test')

        level = task.getLogLevel()
        task.setLogLevel('DEBUG')
        self.assertEqual(logging.DEBUG, task.getLogLevel())

        task.setLogLevel('WARN')
        self.assertEqual(logging.WARN, task.getLogLevel())

    def test_getInfo(self):
        task = Task('test')
        info = task.getInfo()
        self.assertEqual('/test', info['path'])
        self.assertEqual('test', info['name'])
        self.assertEqual('Task', info['class'])
        self.assertEqual('taskforce_common.task.Task', info['module'])

    def test_status(self):
        task = Task('test')
        status = task.status()

        self.assertEqual('NOTDEPLOYED', status['state'])
        self.assertEqual({}, status['parameters'])

        self.assertRaises(KeyError, task.status, 'NotAStatus')

    def test_setStatus(self):
        task = Task('test')
        task.parameters = {'a': 0, 'b': False}

        status = {
            'state': 'RUNNING',
            task.CustomStatusKey: 'customValue',
            'parameters': {'a': 1, 'b': True},
            'name': 'NOT_TEST',
        }

        task.setStatus(status)

        self.assertEqual('RUNNING', task.getStateString())
        self.assertEqual('customValue', task.status()['customStatus'])
        self.assertNotEqual('NOT_TEST', task.name)

    def test_setCustomStatus(self):
        task = Task('test')

        self.assertEqual('', task.status()['customStatus'])
        task.setCustomStatus('foo')
        self.assertEqual('foo', task.status()['customStatus'])

if __name__ == "__main__":
    unittest.main()
