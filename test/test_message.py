import unittest

from taskforce_common import Message


class TestMessage(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_match(self):
        message1 = Message('name1', 'source1', 'destination1')

        self.assertTrue(message1.match(Message('name1', 'source1', 'destination1')))
        self.assertFalse(message1.match('apple'))

        self.assertTrue(Message('name1').match(Message('name1')))
        self.assertTrue(Message('name1').match(Message('name1', 'source1')))
        self.assertTrue(Message('name1').match(Message('name1', destination='destination1')))
        self.assertFalse(Message('name1', data='foo').match(Message('name1', data='bar')))
        self.assertTrue(Message('name1').match(Message('name1', data='bar')))


if __name__ == "__main__":
    unittest.main()