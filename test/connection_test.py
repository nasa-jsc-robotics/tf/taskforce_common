import logging
from multiprocessing import Queue
from multiprocessing import Process
import signal
import os
import time

from collections import OrderedDict
import numpy

logger = logging.getLogger('connection_test')

def getTimeString(t):
    """
    @param t Seconds since the epoch.  Return value from time.time()
    @return String of YYYY-MM-DD HH:MM:SS, ex: 2015-05-05 08:36:34
    """
    timeString = '%Y-%m-%d %H:%M:%S'
    return time.strftime(timeString,time.localtime(t))


def subscribeProcess(subscriberType, iterations, **init_args):
    """
    Container for the subscriber process.

    This method will instantiate the subscriber object and revieve a number of transactions.
    This method is meant to be run in its own process.  Once run, it will pause until call the receive method
    until an "iterations" number of transactions have been received and will then return

    @param publisherType Sub-class of the taskforce_common.Publisher class
    @param data Data to send
    @param init_args Dictionary of keyword arguments for the constructor
    """
    logger.debug('subscribe init_args={}'.format(init_args))
    sub = subscriberType(**init_args)
    sub.initialize()
    sub.connect()
    count = 0
    while( count < iterations):
        if sub.receive() != None:
            count = count + 1

def publishProcess(publisherType, iterations, data, **init_args):
    """
    Container for the publish process.

    This method will instantiate the publisher object and send the data a certain number of times.
    This method is meant to be run in its own process.  Once run, it will pause until it receives a
    signal.SIGCONT from the calling process.  This is to allow the setup to not be apart of the timing
    for the test.

    @param publisherType Sub-class of the taskforce_common.Publisher class
    @param data Data to send
    @param init_args Dictionary of keyword arguments for the constructor
    """
    logger.debug('subscribe init_args={}'.format(init_args))
    pub = publisherType(**init_args)
    pub.initialize()
    pub.connect()

    def loop(signum,frame):
        count = 0
        while(count < iterations):
            pub.send(data)
            count = count + 1

    signal.signal(signal.SIGCONT, loop)
    signal.pause()

class ConnectionTestFixture(object):
    """
    The connection test fixture is meant to handle any rtc connection for analysis

    After instantiation, run setupPublisher, setupSubscriber
    code:
        data = MyObject()
        iter = 10
        runs = 10

        fixture = ConnectionTestFixture()
        fixture.setupPublisher(MyPublisherClass, {'address':'localhost','port':1234})
        fixture.setupSubscriber(MySubscriberClass, {'address':'localhost','port':1234})
        fixture.runTestSuite('TestName', data, iter, runs)
    """

    def __init__(self, logfile=None):
        self.isLoggerSetup = False
        self.setupLogging(logfile)

    def setupLogging(self, logfile):
        if not self.isLoggerSetup:
            self.logger = logging.getLogger('connection_test')

            blank_format = logging.Formatter('%(message)s')

            consoleHandler = logcfg.getConsoleHandler()
            consoleHandler.setFormatter(blank_format)
            consoleHandler.setLevel(logging.DEBUG)
            self.logger.addHandler(consoleHandler)

            if logfile != None:
                fileHandler = logging.FileHandler(logfile,mode='w')
                fileHandler.setFormatter(blank_format)
                fileHandler.setLevel(logging.DEBUG)
                self.logger.addHandler(fileHandler)

            self.logger.setLevel(logging.DEBUG)
            self.isLoggerSetup = True

    def setupPublisher(self, publisherType, pubInitArgs):
        """
        Setup the publisher

        @type publisherType type
        @param publisherType Sub-class of a taskforce_common.Connection.Publisher

        @type pubInitArgs dictionary
        @param pubInitArgs Python dictionary of keyword args for the publisher constructor
        """
        self.publisherType = publisherType
        self.pubInitArgs  = pubInitArgs

    def setupSubscriber(self, subscriberType, subInitArgs):
        """
        Setup the subscriber

        @type subscriberType type
        @param subscriberType Sub-class of a taskforce_common.Connection.Subscriber

        @type subInitArgs dictionary
        @param subInitArgs Python dictionary of keyword args for the subscriber constructor
        """

        self.subscriberType = subscriberType
        self.subInitArgs    = subInitArgs

    def runTest(self, testName, data, iterations, printResults=True, sleepTime=0.2):
        """
        Run the test

        @type testName string
        @param testName Name of the test

        @type data Python object
        @param data Data to send over the connection

        @type iterations int
        @param iterations Number of times to send the data

        @type printResults bool
        @param printResults Print the results to the screen at the conclusion of the test
        """
        logcfg.setLevel(logging.INFO)

        self.testName = testName
        p_sub = Process(target=subscribeProcess, args=(self.subscriberType, iterations), kwargs=self.subInitArgs)
        p_sub.start()

        p_pub = Process(target=publishProcess,   args=(self.publisherType, iterations, data), kwargs=self.pubInitArgs)
        p_pub.start()
        pub_pid = p_pub.pid

        time.sleep(sleepTime)

        time_start = time.time()
        os.kill(pub_pid,signal.SIGCONT)
        p_pub.join()
        p_sub.join()
        time_stop = time.time()

        self.totalTime = time_stop - time_start
        self.iterations = iterations
        self.timePerMsg = self.totalTime / self.iterations
        self.data = data

        logcfg.setLevel(logging.DEBUG)

        self.start_time = getTimeString(time_start)
        self.stop_time  = getTimeString(time_stop)

        if printResults:
            self.printResults()

    def printResults(self):
        """
        print the test results
        """
        for k, v in self.results.items():
            self.logger.info('{:14} : {}'.format(k,v))

    @property
    def results(self):
        return OrderedDict([('testName',self.testName),
                            ('publisher',self.publisherType),
                            ('subscriber',self.subscriberType),
                            ('message',str(self.data)),
                            ('iterations',self.iterations),
                            ('start_time',self.start_time),
                            ('stop_time',self.stop_time),
                            ('totalTime(s)',self.totalTime),
                            ('avgMsgTime(ms)',self.timePerMsg*1000)])

    def runTestSuite(self, testName, data, iterations, runs, printResults=True, sleepTime=0.2):
        """
        Run a test multiple times.

        runTestSuite will simply call runTest "runs" number of times.  Afterwards, it
        will calculate the average and stdev of the total time and the avgMsgTime (in ms).

        @type testName string
        @param testName Name of the test

        @type data Python object
        @param data Data to send

        @type iterations int
        @param iterations Number of transactions to send per run

        @type runs int
        @param runs Number of tests to run

        @type printResults bool
        @param printResults Print the results to the screen
        """
        totalTime  = []
        avgMsgTime = []
        suite_start = time.time()
        for i in range(runs):
            self.logger.info('---------------')
            self.logger.info(' run {} of {}'.format(i+1,runs))
            self.logger.info('---------------')
            self.runTest(testName,data,iterations,printResults,sleepTime)
            totalTime.append(self.totalTime)
            avgMsgTime.append(self.timePerMsg*1000)
        suite_stop = time.time()

        avg_totalTime    = numpy.mean(totalTime)
        avg_avgMsgTime   = numpy.mean(avgMsgTime)
        stdev_totalTime  = numpy.std(totalTime)
        stdev_avgMsgTime = numpy.std(avgMsgTime)

        self.suiteResults =  OrderedDict([
            ('avg_totalTime(s)',avg_totalTime),
            ('stdev_totalTime',stdev_totalTime),
            ('avg_avgMsgTime(ms)',avg_avgMsgTime),
            ('stdev_avgMsgTime',stdev_avgMsgTime),
            ('iterations',iterations),
            ('runs',runs),
            ('suite_start',getTimeString(suite_start)),
            ('suite_stop',getTimeString(suite_stop)),
            ('suite_duration(s)',suite_stop - suite_start)])


        if printResults:
            self.printSuite()

    def printSuite(self):
        self.logger.info("-------------------------------")
        self.logger.info("-- Suite results")
        self.logger.info("-------------------------------")
        for k, v in self.suiteResults.items():
            self.logger.info('{:18} : {}'.format(k,v))
