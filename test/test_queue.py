import unittest
import time

from taskforce_common import QueuePortException
from taskforce_common import QueuePubSub, QueuePublisher, QueueSubscriber
from taskforce_common import QueuePair, QueueBidirectional
from taskforce_common import QueueReqRep, QueueServer, QueueClient
from Queue import Queue

from threading import Thread


def subscribeProcess(sub, queue):
    sub.initialize(queue)
    sub.connect()
    sub.receive()


class DataClass(object):
    def __init__(self):
        self.a = 1
        self.b = 2
        self.s = 'this is a string'

    def __str__(self):
        return 'a={},b={},s={}'.format(self.a,self.b,self.s)


def serverProcess(server, req_rep):
    while True:
        req = server.waitForRequest()
        if req is not None:
            if req == 'quit':
                server.reply(None)
                return
            else:
                server.reply(req_rep[req])


class TestQueuePublisher(unittest.TestCase):

    def setUp(self):
        self.queue = Queue()
        self.kwargs = {'connection':self.queue}

    def tearDown(self):
        pass

    #@unittest.skip('refactoring')
    def testInitialize(self):
        pub = QueuePublisher()

        self.assertFalse(pub.initialized)
        pub.initialize()
        self.assertTrue(pub.initialized)
        self.assertEqual(pub.connections, [])

    #@unittest.skip('refactoring')
    def testAddConnection(self):
        pub = QueuePublisher()

        self.assertRaises(QueuePortException,pub.addConnection,{'connection':1})
        pub.addConnection(self.queue)
        self.assertEqual(pub.connections[0], self.queue)

    #@unittest.skip('refactoring')
    def testConnect(self):
        pub = QueuePublisher()

        pub.initialize()
        self.assertFalse(pub.connected)
        pub.connect()
        self.assertTrue(pub.connected)

    #@unittest.skip('refactoring')
    def testDisconnect(self):
        pub = QueuePublisher()
        pub.addConnection(self.queue)

        self.assertFalse(pub.connected)
        pub.initialize()
        pub.connect()
        self.assertTrue(pub.connected)
        pub.disconnect()
        self.assertFalse(pub.connected)
        self.assertTrue(pub.initialized)


    #@unittest.skip('refactoring')
    def testRegisterCallback(self):
        pub = QueuePublisher()
        self.assertRaises(NotImplementedError, pub.registerCallback, None)

    #@unittest.skip('refactoring')
    def testSend(self):
        pub = QueuePublisher()

        data = 'string data'
        pub.initialize()
        self.assertTrue(pub.initialized)
        self.assertFalse(pub.connected)

        pub.send(data)
        self.assertTrue(pub.connected)
        self.assertEqual(self.queue.empty(),True)

        pub.addConnection(self.queue)
        pub.send(data)
        self.assertFalse(self.queue.empty())
        self.assertTrue(self.queue.get())

        data2 = 'more data'
        queue = Queue()
        pub.addConnection(queue)
        pub.send(data2)
        self.assertTrue(self.queue.get(),data2)
        self.assertTrue(queue.get(),data2)

    #@unittest.skip('refactoring')
    def testSend_dictionary(self):
        data = {'a' : 1, 'b' : 2, 'c' : 3}

        pub = QueuePublisher()
        pub.addConnection(self.queue)
        pub.initialize()
        pub.send(data)
        self.assertEqual(self.queue.get(), data)

    #@unittest.skip('refactoring')
    def testSend_object(self):
        data = DataClass()

        pub = QueuePublisher()
        pub.addConnection(self.queue)
        pub.send(data)
        self.assertEqual(str(self.queue.get()), str(data))


class TestQueueSubscriber(unittest.TestCase):

    def setUp(self):
        self.queue = Queue()
        self.kwargs = {'connection':self.queue}

    def tearDown(self):
        pass

    #@unittest.skip('refactoring')
    def testNoPipeConstructor(self):
        self.assertRaises(QueuePortException,QueueSubscriber, **{'connection':int})

    #@unittest.skip('refactoring')
    def testInitialize(self):
        sub = QueueSubscriber(**self.kwargs)

        self.assertFalse(sub.initialized)
        sub.initialize()
        self.assertTrue(sub.initialized)
        self.assertEqual(sub.conn, self.queue)

    #@unittest.skip('refactoring')
    def testConnect(self):
        sub = QueueSubscriber(**self.kwargs)

        sub.initialize()
        self.assertFalse(sub.connected)
        sub.connect()
        self.assertTrue(sub.connected)

    #@unittest.skip('refactoring')
    def testDisconnect(self):
        sub = QueueSubscriber(**self.kwargs)

        self.assertFalse(sub.connected)
        sub.initialize()
        sub.connect()
        self.assertTrue(sub.connected)
        sub.disconnect()
        self.assertFalse(sub.connected)
        self.assertTrue(sub.initialized)

    #@unittest.skip('refactoring')
    def testRegisterCallback(self):
        sub = QueueSubscriber(**self.kwargs)
        self.assertRaises(NotImplementedError, sub.registerCallback, None)

    #@unittest.skip('refactoring')
    def testReceive(self):
        sub = QueueSubscriber(**self.kwargs)
        data = 'string data'
        self.queue.put(data)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)
        self.assertTrue(sub.receive(),data)
        self.assertTrue(sub.initialized)
        self.assertTrue(sub.connected)

    #@unittest.skip('refactoring')
    def testReceive_dictionary(self):
        sub = QueueSubscriber(**self.kwargs)
        data = {'a' : 1, 'b' : 2, 'c' : 3}
        self.queue.put(data)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)
        self.assertTrue(sub.receive(),data)
        self.assertTrue(sub.initialized)
        self.assertTrue(sub.connected)

    #@unittest.skip('refactoring')
    def testReceive_object(self):
        sub = QueueSubscriber(**self.kwargs)
        data = DataClass()
        self.queue.put(data)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)
        self.assertTrue(sub.receive(),data)
        self.assertTrue(sub.initialized)
        self.assertTrue(sub.connected)


class TestPipePubSub(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip('skipping')
    def testGetPublisher(self):
        connection = QueuePubSub()

        self.assertEqual(connection.getPublisher(),connection.publisher)

    #@unittest.skip('refactoring')
    def testGetSubscriber(self):
        connection = QueuePubSub()

        pub = connection.getPublisher()
        self.assertEqual(len(pub.connections), 0)

        sub1 = connection.getSubscriber()
        self.assertEqual(len(pub.connections), 1)

        sub2 = connection.getSubscriber()
        self.assertEqual(len(pub.connections), 2)

        sub3 = connection.getSubscriber()
        self.assertEqual(len(pub.connections), 3)

        test_message = 'test_message'
        pub.send(test_message)

        self.assertEqual(sub1.receive(),test_message)
        self.assertEqual(sub2.receive(),test_message)
        self.assertEqual(sub3.receive(),test_message)


class TestQueueBidirectional(unittest.TestCase):
    def setUp(self):
        self.queue = Queue()

    def tearDown(self):
        pass

    #@unittest.skip('refactoring')
    def testNoConnectionConstructor(self):
        self.assertRaises(QueuePortException,QueueBidirectional,{'connection':1})

    #@unittest.skip('refactoring')
    def testRegisterCallback(self):
        port = QueueBidirectional((self.queue,self.queue))
        self.assertRaises(NotImplementedError, port.registerCallback,None)

    #@unittest.skip('refactoring')
    def testAssertConnectionsNotATuple(self):
        self.assertRaises(QueuePortException, QueueBidirectional, self.queue)

    #@unittest.skip('refactoring')
    def testSendReceive(self):
        queue_AtoB = Queue()
        queue_BtoA = Queue()

        portA = QueueBidirectional((queue_AtoB,queue_BtoA))
        portB = QueueBidirectional((queue_BtoA,queue_AtoB))

        dataAB = 'a to b'
        dataBA = 'b to a'

        self.assertFalse(portA.initialized)
        self.assertFalse(portB.initialized)
        self.assertFalse(portA.connected)
        self.assertFalse(portB.connected)

        portA.send(dataAB)
        portB.send(dataBA)

        self.assertEqual(portA.receive(), dataBA)
        self.assertEqual(portB.receive(), dataAB)

        self.assertTrue(portA.initialized)
        self.assertTrue(portB.initialized)
        self.assertTrue(portA.connected)
        self.assertTrue(portB.connected)

    #@unittest.skip('refactoring')
    def testReceive_dictionary(self):
        queue_AtoB = Queue()
        queue_BtoA = Queue()

        portA = QueueBidirectional((queue_AtoB,queue_BtoA))
        portB = QueueBidirectional((queue_BtoA,queue_AtoB))

        dataAB = {'a' : 1, 'b' : 2, 'c' : 3}
        dataBA = {'d' : 4, 'e' : 5, 'f' : 6}

        self.assertFalse(portA.initialized)
        self.assertFalse(portB.initialized)
        self.assertFalse(portA.connected)
        self.assertFalse(portB.connected)

        portA.send(dataAB)
        portB.send(dataBA)

        time.sleep(0.1)

        self.assertEqual(portA.receive(), dataBA)
        self.assertEqual(portB.receive(), dataAB)

        self.assertTrue(portA.initialized)
        self.assertTrue(portB.initialized)
        self.assertTrue(portA.connected)
        self.assertTrue(portB.connected)

    #@unittest.skip('refactoring')
    def testReceive_object(self):
        pass


class TestQueuePair(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip('refactoring')
    def testGetParent(self):
        connection = QueuePair()
        portA = connection.getParent()
        self.assertEqual(portA, connection.parent)

    #@unittest.skip('refactoring')
    def testGetChild(self):
        connection = QueuePair()
        portA = connection.getChild()
        self.assertEqual(portA, connection.child)

    #@unittest.skip('refactoring')
    def testSendReceive(self):
        connection = QueuePair()
        portA = connection.getParent()
        portB = connection.getChild()

        dataAB = {'a' : 1, 'b' : 2, 'c' : 3}
        dataBA = {'d' : 4, 'e' : 5, 'f' : 6}
        portA.send(dataAB)
        portB.send(dataBA)

        self.assertEqual(portA.receive(), dataBA)
        self.assertEqual(portB.receive(), dataAB)


class TestQueueServer(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip('refactoring')
    def testNoConnectionConstructor(self):
        self.assertRaises(QueuePortException, QueueServer, 'test')

    #@unittest.skip('refactoring')
    def testNoConnection(self):
        server = QueueServer()

        req = server.waitForRequest()
        self.assertEqual(req, None)

    #@unittest.skip('refactoring')
    def testOneConnection(self):
        sender = Queue()
        receiver = Queue()

        server = QueueServer((sender,receiver))

        msg = 'test_message'
        receiver.put(msg)
        req = server.waitForRequest()
        self.assertEqual(req, msg)
        self.assertEqual(server.reply_conn, sender)

        msg_reply = 'reply'
        server.reply(msg_reply)
        self.assertEqual(sender.get(),msg_reply)

    #@unittest.skip('refactoring')
    def testMultiConnections(self):
        s1Toc1 = Queue()
        s2Toc2 = Queue()
        s3Toc3 = Queue()

        c1Tos1 = Queue()
        c2Tos2 = Queue()
        c3Tos3 = Queue()

        server = QueueServer()
        server.addConnections((s1Toc1, c1Tos1))
        server.addConnections((s2Toc2, c2Tos2))
        server.addConnections((s3Toc3, c3Tos3))

        req1 = 'req1'
        req2 = 'req2'
        req3 = 'req3'

        rep1 = 'rep1'
        rep2 = 'rep2'
        rep3 = 'rep3'

        c1Tos1.put(req1)

        self.assertEqual(server.waitForRequest(),req1)
        server.reply(rep1)
        self.assertEqual(s1Toc1.get(), rep1)

        c2Tos2.put(req2)
        self.assertEqual(server.waitForRequest(),req2)
        server.reply(rep2)
        self.assertEqual(s2Toc2.get(), rep2)

        c3Tos3.put(req3)
        self.assertEqual(server.waitForRequest(),req3)
        server.reply(rep3)
        self.assertEqual(s3Toc3.get(), rep3)

    #@unittest.skip('refactoring')
    def testReplyBeforeRequest(self):
        server = QueueServer()
        self.assertRaises(QueuePortException, server.reply, 'test')

    #@unittest.skip('refactoring')
    def testRequestBeforeReply(self):
        sender = Queue()
        receiver = Queue()
        server = QueueServer((sender, receiver))
        msg = 'test_message'
        receiver.put(msg)
        req = server.waitForRequest()
        self.assertRaises(QueuePortException, server.waitForRequest)

    #@unittest.skip('refactoring')
    def testAddConnections(self):
        server = QueueServer()
        queue = Queue()

        req = server.waitForRequest()
        self.assertEqual(req, None)
        self.assertRaises(QueuePortException, server.addConnections, ['test'])
        self.assertRaises(QueuePortException, server.addConnections, ['test', 'test'])
        self.assertRaises(QueuePortException, server.addConnections, [queue, 'test'])


class TestQueueReqRep(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip('refactoring')
    def testGetServer(self):
        connection = QueueReqRep()
        server = connection.getServer()
        self.assertEqual(server, connection.server)
        self.assertEqual(server.connections, {})

    #@unittest.skip('refactoring')
    def testGetClient(self):
        connection = QueueReqRep()
        server = connection.getServer()
        client1 = connection.getClient()
        client2 = connection.getClient()
        client3 = connection.getClient()

        req1 = 'req1'
        req2 = 'req2'
        req3 = 'req3'

        rep1 = 'rep1'
        rep2 = 'rep2'
        rep3 = 'rep3'

        req_rep = {req1 : rep1,
                   req2 : rep2,
                   req3 : rep3}

        p = Thread(target=serverProcess, args=(server,req_rep))
        p.start()

        self.assertEqual(client1.request(req1),rep1)
        self.assertEqual(client2.request(req2),rep2)
        self.assertEqual(client3.request(req3),rep3)

        client1.request('quit')
        p.join()

    #@unittest.skip('refactoring')
    def testClient(self):
        conn = QueueReqRep()
        server = conn.getServer()
        client = conn.getClient()

        req1 = 'req1'
        rep1 = 'rep1'

        server.connect()

        req_rep = {req1 : rep1}

        p = Thread(target=serverProcess, args=(server,req_rep))
        p.start()
        time.sleep(0.1)
        self.assertEqual(client.request(req1),rep1)

        client.request('quit')
        p.join()

if __name__ == "__main__":
    unittest.main()
