import unittest
import jsonschema

from taskforce_common import convert_block_definition
from taskforce_common import TASKFORCE_GROUP_VERSION
from taskforce_common import taskforce_schema

false = False
TWO_COWS_BLOCK = {
                        "block": {
                            "file_name": "taskforce_example_library/cow/two_cows.block",
                            "name": "two_cows",
                            "sub_blocks": [],
                            "tasks": [
                                {
                                    "task": {
                                        "breakPoint": false,
                                        "class": "BlockTask",
                                        "module": "taskforce_common.task.BlockTask",
                                        "name": "<block>",
                                        "parameters": {},
                                        "subscribedEvents": [
                                            {
                                                "callback": "exitBlock",
                                                "event": {
                                                    "name": "complete",
                                                    "source": "clementine"
                                                }
                                            }
                                        ]
                                    }
                                },
                                {
                                    "task": {
                                        "breakPoint": false,
                                        "class": "Cowtime",
                                        "module": "taskforce_example_library.cow.Cowtime",
                                        "name": "clara",
                                        "parameters": {},
                                        "subscribedEvents": [
                                            {
                                                "callback": "startup",
                                                "event": {
                                                    "name": "enterBlock",
                                                    "source": "<block>"
                                                }
                                            }
                                        ]
                                    }
                                },
                                {
                                    "task": {
                                        "breakPoint": false,
                                        "class": "Cowtime",
                                        "module": "taskforce_example_library.cow.Cowtime",
                                        "name": "clementine",
                                        "parameters": {},
                                        "subscribedEvents": [
                                            {
                                                "callback": "startup",
                                                "event": {
                                                    "name": "complete",
                                                    "source": "clara"
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }

TWO_COWS_GROUP = {
    "group": {
        "eventRelays": [
            {
                "eventName": "complete",
                "sourceName": "<group>"
            },
            {
                "eventName": "startup",
                "sourceName": "<group>"
            }
        ],
        "fileName": "taskforce_example_library/cow/two_cows.group",
        "name": "two_cows",
        "parameterBindings": [],
        "parameters": {},
        "subGroups": [],
        "subscriptions": [
            {
                "callback": "complete",
                "destination": "<group>",
                "eventName": "complete",
                "source": "clementine"
            },
            {
                "callback": "startup",
                "destination": "clara",
                "eventName": "startup",
                "source": "<group>"
            },
            {
                "callback": "startup",
                "destination": "clementine",
                "eventName": "complete",
                "source": "clara"
            }
        ],
        "tasks": [
            {
                "task": {
                    "class": "Cowtime",
                    "module": "taskforce_example_library.cow.Cowtime",
                    "name": "clara",
                    "parameters": {}
                }
            },
            {
                "task": {
                    "class": "Cowtime",
                    "module": "taskforce_example_library.cow.Cowtime",
                    "name": "clementine",
                    "parameters": {}
                }
            }
        ]
    },
    "version": 2.0
}


class TestConvertBlockFileDefinition(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_convert_block_definition(self):
        definition = convert_block_definition(TWO_COWS_BLOCK)

        self.assertNotEqual(None, definition.get('group', None))
        self.assertEqual(TASKFORCE_GROUP_VERSION, definition.get('version'))
        self.assertEqual(['group', 'version'], sorted(definition.keys()))
        self.assertEqual(TWO_COWS_GROUP['group']['eventRelays'], definition['group']['eventRelays'])
        self.assertEqual(TWO_COWS_GROUP['group']['fileName'], definition['group']['fileName'])
        self.assertEqual(TWO_COWS_GROUP['group']['name'], definition['group']['name'])
        self.assertEqual(TWO_COWS_GROUP['group']['parameterBindings'], definition['group']['parameterBindings'])
        self.assertEqual(TWO_COWS_GROUP['group']['parameters'], definition['group']['parameters'])
        self.assertEqual(TWO_COWS_GROUP['group']['subGroups'], definition['group']['subGroups'])
        self.assertEqual(TWO_COWS_GROUP['group']['subscriptions'], definition['group']['subscriptions'])
        self.assertEqual(TWO_COWS_GROUP['group']['tasks'], definition['group']['tasks'])

        # validate the test input and output against the schema
        jsonschema.validate(TWO_COWS_GROUP, taskforce_schema)
        jsonschema.validate(definition, taskforce_schema)


if __name__ == "__main__":
    unittest.main()
