from taskforce_common import TaskGroup
from taskforce_common.utils import saveJsonFile
from task_library.Cowtime import Cowtime
from pprint import pprint

ten_cows = TaskGroup("10_cows","block_library/10_cows.group")
ten_cows.addEventRelay("startup")
ten_cows.addEventRelay("complete")

ten_cows.addTask(Cowtime("cow00"))
ten_cows.addTask(Cowtime("cow01"))
ten_cows.addTask(Cowtime("cow02"))
ten_cows.addTask(Cowtime("cow03"))
ten_cows.addTask(Cowtime("cow04"))
ten_cows.addTask(Cowtime("cow05"))
ten_cows.addTask(Cowtime("cow06"))
ten_cows.addTask(Cowtime("cow07"))
ten_cows.addTask(Cowtime("cow08"))
ten_cows.addTask(Cowtime("cow09"))

ten_cows.subscribeEvent("startup","ten_cows","cow00","startup")
ten_cows.subscribeEvent("complete","cow00","cow01","startup")
ten_cows.subscribeEvent("complete","cow01","cow02","startup")
ten_cows.subscribeEvent("complete","cow02","cow03","startup")
ten_cows.subscribeEvent("complete","cow03","cow04","startup")
ten_cows.subscribeEvent("complete","cow04","cow05","startup")
ten_cows.subscribeEvent("complete","cow05","cow06","startup")
ten_cows.subscribeEvent("complete","cow06","cow07","startup")
ten_cows.subscribeEvent("complete","cow07","cow08","startup")
ten_cows.subscribeEvent("complete","cow08","cow09","startup")
ten_cows.subscribeEvent("startup","cow09","ten_cows","complete")


saveJsonFile("block_library/10_cows.group",ten_cows.serialize())


one_hundred_cows = TaskGroup("100_cows","block_library/100_cows.group")
one_hundred_cows.addEventRelay("startup")
one_hundred_cows.addEventRelay("complete")
for i in range(10):
    group = TaskGroup("herd_{}".format(i),"block_library/10_cows.group")
    one_hundred_cows.addSubGroup(group)

for i in range(10-1):
    one_hundred_cows.subscribeEvent("complete","herd_{}".format(i),"herd_{}".format(i+1),"startup")
one_hundred_cows.subscribeEvent("startup","100_cows","herd_0","startup")
one_hundred_cows.subscribeEvent("complete","herd_9", "100_cows","complete")


saveJsonFile("block_library/100_cows.group",one_hundred_cows.serialize())


one_thousand_cows = TaskGroup("1000_cows","block_library/1000_cows.group")
one_thousand_cows.addEventRelay("startup")
one_thousand_cows.addEventRelay("complete")
for i in range(10):
    group = TaskGroup("century_{}".format(i),"block_library/100_cows.group")
    one_thousand_cows.addSubGroup(group)

for i in range(10-1):
    one_thousand_cows.subscribeEvent("complete","century_{}".format(i),"century_{}".format(i+1),"startup")
one_thousand_cows.subscribeEvent("startup","1000_cows","century_0","startup")
one_thousand_cows.subscribeEvent("complete","century_9", "1000_cows","complete")


saveJsonFile("block_library/1000_cows.group",one_thousand_cows.serialize())
