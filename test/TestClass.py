class TestClass(object):
    def __init__(self, val, kw='keyword'):
        self.val = val
        self.other = 'blue'
        self.kw = kw

    def getVal(self):
        return self.val

    def getKw(self):
        return self.kw

