import os

from taskforce_common import EventHandler, CommandHandler, CommandInterface
from taskforce_common import logcfg
from taskforce_common import GroupDefinitionLoader, GroupFactory
from taskforce_common import QueuePubSub, QueueReqRep
from taskforce_common import Event, Command
import time

TEST_DIRECTORY = os.path.dirname(os.path.abspath(__file__))
from pprint import pprint

if __name__ == "__main__":

    logcfg.configure_common_logging()


    file_map = {}
    file_map["block_library/herd.group"]     = os.path.join(TEST_DIRECTORY, "block_library","herd.group")
    file_map["block_library/two_cows.group"] = os.path.join(TEST_DIRECTORY, "block_library","two_cows.group")

    definition = GroupDefinitionLoader.loadDefinition("block_library/herd.group", file_map)

    eventConnection = QueuePubSub()
    eventHandler = EventHandler("handler")
    eventHandler.addPort(eventConnection.getSubscriber())
    eventHandler.startEventLoop()

    commandConnection = QueueReqRep()
    commandHandler = CommandHandler("handler")
    commandHandler.addPort(commandConnection.getServer())
    commandHandler.startCommandLoop()

    commandInterface = CommandInterface("engine", commandConnection.getClient())


    factory = GroupFactory()
    group = factory.buildDefinition(definition)
    group.addEventPort(eventConnection.getPublisher())

    for subscription in group.getSubscriptions():
        callback = getattr(group.getObject(subscription.destination),subscription.callback)
        eventHandler.subscribeEvent(Event(subscription.eventName,subscription.source),callback)


    objects = group.getObjects()
    for object in objects.values():
        commandNames = object.getCommandNames()
        for command in commandNames:
            commandHandler.registerCommand(Command(command,"engine",object.name),getattr(object,command))


    rval = commandInterface.sendCommand("startup", "herd/TwoCows01")
    print "---------------"
    print rval
    print "---------------"

    commandHandler.stopCommandLoop()
    eventHandler.stopEventLoop()

    pprint(group.serialize())
