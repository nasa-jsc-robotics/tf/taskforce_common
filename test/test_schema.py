#!/usr/bin/env python
import unittest

import jsonschema
import taskforce_common


class TestSchema(unittest.TestCase):

    def setUp(self):
        self.schema = taskforce_common.taskforce_schema

    def tearDown(self):
        pass

    def test_parameters(self):
        data = {
            "parameters": {
                "param1": True,
                "param2": [
                    1, 2, 3
                ],
                "param3": 3.14,
                "param4": {
                    "a": 1,
                    "b": "green"
                }
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))

    def test_eventRelay(self):
        data = {
            "eventRelay": {
                "eventName": "event1",
                "sourceName": "sourceName"
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))

    def test_subscription(self):
        data = {
            "subscription": {
                "eventName": "event1",
                "source": "task1",
                "destination": "task2",
                "callback": "myFunction",
                "breakpoint": False
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))

    def test_task(self):
        data = {
            "task": {
                "name": "MyTask",
                "module": "taskforce.common.Tasks",
                "class": "Task",
                "parameters": {
                    "param1": True,
                    "param2": [1, 2, 3]
                }
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))

    def test_subGroup(self):
        data = {
            "subGroup": {
                "name": "Subgroup1",
                "fileName": "file_name.txt"
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))

    def test_binding(self):
        data = {
            "binding": {
                'destinationKey': 'param1',
                'target': 'aaa'
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))

    def test_parameterBinding(self):
        data = {
            "parameterBinding": {
                "bindings": [
                    {
                        'destinationKey': 'param1',
                        'target': 'aaa'
                    },
                    {
                        'destinationKey': 'param2',
                        'target': 'bbb'
                    }
                ],
                "sourceKey": "groupNameParamKey"
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))

    def test_group(self):
        data = {
            'group': {
                'eventRelays': [],
                'fileName': 'fileName',
                'name': 'group',
                'parameterBindings': [
                    {
                        'bindings': [
                            {
                                'destinationKey': 'param1',
                                'target': 'aaa'
                            },
                            {
                                'destinationKey': 'param2',
                                'target': 'bbb'
                            }
                        ],
                        'sourceKey': 'groupParam1'
                    }
                ],
                'parameters': {
                    'groupParam1': 'superValue'
                },
                'subGroups': [],
                'subscriptions': [],
                'tasks': [
                    {
                        'task': {
                            'class': 'TestTask',
                            'module': '__main__',
                            'name': 'aaa',
                            'parameters': {
                                'param1': True,
                                'param2': 'blue',
                                'param3': 42
                            }
                        }
                    },
                    {
                        'task': {
                            'class': 'TestTask',
                            'module': '__main__',
                            'name': 'bbb',
                            'parameters': {
                                'param1': True,
                                'param2': 'blue',
                                'param3': 42
                            }
                        }
                    }
                ]
            }
        }
        self.assertEqual(None, jsonschema.validate(data, self.schema))


if __name__ == "__main__":
    unittest.main()
