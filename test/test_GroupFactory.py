import unittest
import os
from taskforce_common import GroupDefinitionLoader, GroupFactory, build_group_from_file
from pprint import pprint
from taskforce_common.utils import FileMap

TEST_FOLDER = os.path.dirname(os.path.abspath(__file__))


class TestGroupFactory(unittest.TestCase):

    def setUp(self):
        self.fileMap = {}
        self.fileMap['block_library/level0.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level0.group')
        self.fileMap['block_library/level1.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level1.group')
        self.fileMap['block_library/level2.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level2.group')
        self.fileMap['block_library/level3.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level3.group')
        self.fileMap['block_library/10_cows.group'] = os.path.join(TEST_FOLDER, 'block_library', '10_cows.group')
        self.fileMap['block_library/100_cows.group'] = os.path.join(TEST_FOLDER, 'block_library', '100_cows.group')
        self.fileMap['block_library/1000_cows.group'] = os.path.join(TEST_FOLDER, 'block_library', '1000_cows.group')
        self.fileMap['block_library/level0_wrong_version.group'] = os.path.join(TEST_FOLDER, 'block_library',
                                                                                'level0_wrong_version.group')

    def tearDown(self):
        pass

    @unittest.skip("TODO")
    def test_buildDefinition_two_level(self):
        definition = GroupDefinitionLoader.loadDefinition('block_library/level1.group', self.fileMap)

        pprint(definition)

        factory = GroupFactory()
        group = factory.buildDefinition(definition)
        pass

    @unittest.skip("TODO")
    def test_buildDefinition_deep_nest(self):
        definition = GroupDefinitionLoader.loadDefinition('block_library/level3.group', self.fileMap)

        print definition

        factory = GroupFactory()
        group = factory.buildDefinition(definition)

        level3_subgroup_a = group.getSubGroups()['level3/subgroup_a']
        level3_subgroup_b = group.getSubGroups()['level3/subgroup_b']

        print level3_subgroup_a.getSubGroups().keys()
        level3_subgroup_a_subgroup_a = level3_subgroup_a.getSubGroups()['level3/subgroup_a/subgroup_a']

    @unittest.skip("TODO")
    def test_1000_cows(self):
        definition = GroupDefinitionLoader.loadDefinition('block_library/1000_cows.group', self.fileMap)
        factory = GroupFactory()
        group = factory.buildDefinition(definition)

        tasks = group.getTasks()
        pprint(sorted(tasks.keys()))
        pass

    def test_buildDefinition_two_level(self):
        group = build_group_from_file('block_library/level1.group', self.fileMap)

        pass

    def test_build_group_from_file_bad_version(self):
        self.assertRaises(Exception, build_group_from_file, 'block_library/level0_wrong_version.group', self.fileMap)
