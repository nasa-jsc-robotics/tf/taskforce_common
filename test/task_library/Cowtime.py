# standard imports.  Add more imports as needed.
import subprocess
import time

from taskforce_common import Task, TaskState, Response, ErrorResponse

class Cowtime(Task):
    events = []
    commands = []

    def onExecute(self):
        s = '{} says {}'.format(self.path, time.ctime(time.time()))
        subprocess.call('cowsay {}'.format(s), shell=True)
        return True
