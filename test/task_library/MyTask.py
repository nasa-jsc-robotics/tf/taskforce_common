import logging
import time

from taskforce_common import Task, TaskState

logger = logging.getLogger(name='MyTask')

class MyTask(Task):

    parameters = {"param1" : None, "param2" : None}

    def onInit(self):
        self.logger = logger.getChild(self.name)
        self.eventHandler.subscribeEvent('getCount',self.getCount)
        self.eventHandler.subscribeEvent('resetCount',self.resetCount)

    def onStartup(self):
        self.count = 0
        self.logger.debug('{} start'.format(self.name))

    def onExecute(self):
        while(self.state == TaskState.RUNNING):
            self.logger.debug('Count={}, pid={}'.format(self.count,self.pid))
            self.count = self.count + 1
            time.sleep(1)

    def getCount(self):
        return self.count

    def resetCount(self):
        self.count = 0

class SingleShotTask(Task):
    def onInit(self):
        self.logger = logger.getChild(self.name)

    def onExecute(self):
        self.logger.info('Single shot!  POW!!!')
