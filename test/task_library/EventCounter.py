from taskforce_common import Task, TaskState

class EventCounter(Task):

    events = ['goalReached']
    commands = ['setGoal','resetCount','increaseCount']

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)

        self.count = 0
        self.goal = 10
        self.goal_reached = False

    def onInit(self):
        self.count = 0
        self.goal = 10
        self.goal_reached = False

        return True

    def onStartup(self):
        self.count = 0
        self.goal_reached = False

        return True

    def onExecute(self):
        while (self.state == TaskState.RUNNING):
            if not self.goal_reached:
                if self.count >= self.goal:
                    self.goal_reached=True
                    self.emit('goalReached')

        return True

    def setGoal(self, goal):
        self.goal = goal
        return self.goal

    def resetCount(self):
        self.count = 0
        return self.count

    def increaseCount(self):
        if self.count < self.goal:
            self.goal_reached = False
            self.count = self.count + 1
            self.logger.info('count = {}'.format(self.count))
        return self.count
