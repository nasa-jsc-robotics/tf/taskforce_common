import os
import time
from taskforce_common import GroupDefinitionLoader, GroupFactory
from taskforce_common import EventHandler
from taskforce_common import TaskState
from taskforce_common import QueuePubSub
from pprint import pprint
from taskforce_common.utils import FileMap

TEST_FOLDER = os.path.dirname(os.path.abspath(__file__))

fileMap = {}
fileMap['block_library/level0.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level0.group')
fileMap['block_library/level1.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level1.group')
fileMap['block_library/level2.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level2.group')
fileMap['block_library/level3.group'] = os.path.join(TEST_FOLDER, 'block_library', 'level3.group')
fileMap['block_library/10_cows.group'] = os.path.join(TEST_FOLDER, 'block_library', '10_cows.group')
fileMap['block_library/100_cows.group'] = os.path.join(TEST_FOLDER, 'block_library', '100_cows.group')
fileMap['block_library/1000_cows.group'] = os.path.join(TEST_FOLDER, 'block_library', '1000_cows.group')


definition = GroupDefinitionLoader.loadDefinition('block_library/100_cows.group', fileMap)
factory = GroupFactory()
group = factory.buildDefinition(definition)

tasks = group.getTasks()
pprint(sorted(tasks.keys()))

subscriptions = group.getSubscriptions()
for subscription in subscriptions:
    print (subscription)


connection = QueuePubSub()

eventHandler = EventHandler('handler')
eventHandler.addPort(connection.getSubscriber())

group.addEventPort(connection.getPublisher(), recursive=True)

subGroups = group.getSubGroups()

eventHandler.registerObject(group.path, group)

for task in tasks.values():
    eventHandler.registerObject(task.path, task)

for subGroup in subGroups.values():
    eventHandler.registerObject(subGroup.path, subGroup)

for subscription in subscriptions:
    eventHandler.subscribeEvent(subscription.eventName, subscription.source, subscription.destination, subscription.callback)

eventHandler.startEventLoop()

while not eventHandler.eventLoopRunning:
    time.sleep(0.1)

for task in tasks.values():
    print "deploy {}".format(task.path)
    task.deploy()

group.startup()

last_cow = tasks[sorted(tasks.keys())[-1]]
print "last_cow = {}".format(last_cow.path)
while last_cow.state < TaskState.COMPLETE:
    time.sleep(0.5)

eventHandler.stopEventLoop()
