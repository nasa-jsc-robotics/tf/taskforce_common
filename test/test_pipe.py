import unittest

from taskforce_common import PipePortException
from taskforce_common import PipePubSub, PipePublisher, PipeSubscriber
from taskforce_common import PipePair, PipeBidirectional
from taskforce_common import PipeReqRep, PipeServer, PipeClient
from multiprocessing import Pipe
from multiprocessing import Process
from threading import Thread
import time



def subscribeProcess(sub, queue):
    sub.initialize(queue)
    sub.connect()
    sub.receive()


class DataClass(object):
    def __init__(self):
        self.a = 1
        self.b = 2
        self.s = 'this is a string'

    def __str__(self):
        return 'a={},b={},s={}'.format(self.a,self.b,self.s)


def serverProcess(server, reply):
    while True:
        if server.waitForRequest() is not None:
            server.reply(reply)
            return

class TestPipePublisher(unittest.TestCase):

    def setUp(self):
        self.parent_conn, self.child_conn = Pipe()
        self.kwargs = {'connection':self.parent_conn}

    def tearDown(self):
        pass

    def testInitialize(self):
        pub = PipePublisher()

        self.assertFalse(pub.initialized)
        pub.initialize()
        self.assertTrue(pub.initialized)
        self.assertEqual(pub.connections, [])

    def testAddConnection(self):
        pub = PipePublisher()

        self.assertRaises(PipePortException,pub.addConnection,{'connection':1})
        pub.addConnection(self.parent_conn)
        self.assertEqual(pub.connections[0], self.parent_conn)

    def testConnect(self):
        pub = PipePublisher()

        pub.initialize()
        self.assertFalse(pub.connected)
        pub.connect()
        self.assertTrue(pub.connected)


    def testDisconnect(self):
        pub = PipePublisher()
        pub.addConnection(self.parent_conn)

        self.assertFalse(pub.connected)
        pub.initialize()
        pub.connect()
        self.assertTrue(pub.connected)
        pub.disconnect()
        self.assertFalse(pub.connected)
        self.assertTrue(pub.initialized)

        # asserting EOFError shows that the other end of the pipe has been closed
        self.assertRaises(EOFError, self.child_conn.recv)


    def testRegisterCallback(self):
        pub = PipePublisher()
        self.assertRaises(NotImplementedError, pub.registerCallback, None)


    def testSend(self):
        pub = PipePublisher()

        data = 'string data'
        pub.initialize()
        self.assertTrue(pub.initialized)
        self.assertFalse(pub.connected)

        pub.send(data)
        self.assertTrue(pub.connected)
        self.assertEqual(self.child_conn.poll(),False)

        pub.addConnection(self.parent_conn)
        pub.send(data)
        self.assertTrue(self.child_conn.poll(),True)
        self.assertTrue(self.child_conn.recv(),data)

        data2 = 'more data'
        child_conn2, parent_conn2 = Pipe()
        pub.addConnection(parent_conn2)
        pub.send(data2)
        self.assertTrue(self.child_conn.recv(),data2)
        self.assertTrue(child_conn2.recv(),data2)

    #@unittest.skip('refactoring')
    def testSend_dictionary(self):
        data = {'a' : 1, 'b' : 2, 'c' : 3}

        pub = PipePublisher()
        pub.addConnection(self.parent_conn)
        pub.initialize()
        pub.send(data)
        self.assertEqual(self.child_conn.recv(), data)

    #@unittest.skip('refactoring')
    def testSend_object(self):
        data = DataClass()

        pub = PipePublisher()
        pub.addConnection(self.parent_conn)
        pub.send(data)
        self.assertEqual(str(self.child_conn.recv()), str(data))

class TestPipeSubscriber(unittest.TestCase):

    def setUp(self):
        self.parent_conn, self.child_conn = Pipe()
        self.kwargs = {'connection':self.child_conn}

    def tearDown(self):
        pass

    def testNoPipeConstructor(self):
        self.assertRaises(PipePortException,PipeSubscriber, **{'connection':int})

    def testInitialize(self):
        sub = PipeSubscriber(**self.kwargs)

        self.assertFalse(sub.initialized)
        sub.initialize()
        self.assertTrue(sub.initialized)
        self.assertEqual(sub.conn, self.child_conn)

    def testConnect(self):
        sub = PipeSubscriber(**self.kwargs)

        sub.initialize()
        self.assertFalse(sub.connected)
        sub.connect()
        self.assertTrue(sub.connected)

    def testDisconnect(self):
        sub = PipeSubscriber(**self.kwargs)

        self.assertFalse(sub.connected)
        sub.initialize()
        sub.connect()
        self.assertTrue(sub.connected)
        sub.disconnect()
        self.assertFalse(sub.connected)
        self.assertTrue(sub.initialized)

    def testRegisterCallback(self):
        sub = PipeSubscriber(**self.kwargs)
        self.assertRaises(NotImplementedError, sub.registerCallback, None)

    def testReceive(self):
        sub = PipeSubscriber(**self.kwargs)
        data = 'string data'
        self.parent_conn.send(data)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)
        self.assertTrue(sub.receive(),data)
        self.assertTrue(sub.initialized)
        self.assertTrue(sub.connected)

    def testReceive_dictionary(self):
        sub = PipeSubscriber(**self.kwargs)
        data = {'a' : 1, 'b' : 2, 'c' : 3}
        self.parent_conn.send(data)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)
        self.assertTrue(sub.receive(),data)
        self.assertTrue(sub.initialized)
        self.assertTrue(sub.connected)

    def testReceive_object(self):
        sub = PipeSubscriber(**self.kwargs)
        data = DataClass()
        self.parent_conn.send(data)

        self.assertFalse(sub.initialized)
        self.assertFalse(sub.connected)
        self.assertTrue(sub.receive(),data)
        self.assertTrue(sub.initialized)
        self.assertTrue(sub.connected)

class TestPipePubSub(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    #@unittest.skip('skipping')
    def testGetPublisher(self):
        connection = PipePubSub()

        self.assertEqual(connection.getPublisher(),connection.publisher)

    def testGetSubscriber(self):
        connection = PipePubSub()

        pub = connection.getPublisher()
        self.assertEqual(len(pub.connections), 0)

        sub1 = connection.getSubscriber()
        self.assertEqual(len(pub.connections), 1)

        sub2 = connection.getSubscriber()
        self.assertEqual(len(pub.connections), 2)

        sub3 = connection.getSubscriber()
        self.assertEqual(len(pub.connections), 3)

        test_message = 'test_message'
        pub.send(test_message)
        self.assertTrue(sub1.conn.poll(),True)
        self.assertTrue(sub2.conn.poll(),True)
        self.assertTrue(sub3.conn.poll(),True)

        self.assertEqual(sub1.receive(),test_message)
        self.assertEqual(sub2.receive(),test_message)
        self.assertEqual(sub3.receive(),test_message)

class TestPipeBidirectional(unittest.TestCase):
    def setUp(self):
        self.child_conn, self.parent_conn = Pipe()

    def tearDown(self):
        pass

    def testNoConnectionConstructor(self):
        self.assertRaises(PipePortException,PipeBidirectional,{'connection':1})

    def testRegisterCallback(self):
        port = PipeBidirectional(self.parent_conn)
        self.assertRaises(NotImplementedError, port.registerCallback,None)

    def testSendReceive(self):
        portA = PipeBidirectional(self.parent_conn)
        portB = PipeBidirectional(self.child_conn)

        dataAB = 'a to b'
        dataBA = 'b to a'

        self.assertFalse(portA.initialized)
        self.assertFalse(portB.initialized)
        self.assertFalse(portA.connected)
        self.assertFalse(portB.connected)

        portA.send(dataAB)
        portB.send(dataBA)

        self.assertEqual(portA.receive(), dataBA)
        self.assertEqual(portB.receive(), dataAB)

        self.assertTrue(portA.initialized)
        self.assertTrue(portB.initialized)
        self.assertTrue(portA.connected)
        self.assertTrue(portB.connected)

    def testReceive_dictionary(self):
        portA = PipeBidirectional(self.parent_conn)
        portB = PipeBidirectional(self.child_conn)

        dataAB = {'a' : 1, 'b' : 2, 'c' : 3}
        dataBA = {'d' : 4, 'e' : 5, 'f' : 6}

        self.assertFalse(portA.initialized)
        self.assertFalse(portB.initialized)
        self.assertFalse(portA.connected)
        self.assertFalse(portB.connected)

        portA.send(dataAB)
        portB.send(dataBA)

        self.assertEqual(portA.receive(), dataBA)
        self.assertEqual(portB.receive(), dataAB)

        self.assertTrue(portA.initialized)
        self.assertTrue(portB.initialized)
        self.assertTrue(portA.connected)
        self.assertTrue(portB.connected)

    def testReceive_object(self):
        pass

class TestPipePair(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetParent(self):
        connection = PipePair()
        portA = connection.getParent()
        self.assertEqual(portA, connection.parent)

    def testGetChild(self):
        connection = PipePair()
        portA = connection.getChild()
        self.assertEqual(portA, connection.child)

    def testSendReceive(self):
        connection = PipePair()
        portA = connection.getParent()
        portB = connection.getChild()

        dataAB = {'a' : 1, 'b' : 2, 'c' : 3}
        dataBA = {'d' : 4, 'e' : 5, 'f' : 6}
        portA.send(dataAB)
        portB.send(dataBA)

        self.assertEqual(portA.receive(), dataBA)
        self.assertEqual(portB.receive(), dataAB)

class TestPipeServer(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testNoConnectionConstructor(self):
        self.assertRaises(PipePortException, PipeServer, 'test')

    def testNoConnection(self):
        server = PipeServer()

        req = server.waitForRequest()
        self.assertEqual(req, None)

    def testOneConnection(self):
        child_conn, parent_conn = Pipe()

        server = PipeServer(parent_conn)

        msg = 'test_message'
        child_conn.send(msg)

        req = server.waitForRequest()
        self.assertEqual(req, msg)
        self.assertEqual(server.reply_conn, parent_conn)

        msg_reply = 'reply'
        server.reply(msg_reply)
        self.assertEqual(child_conn.recv(),msg_reply)

    def testMultiConnections(self):
        c1, p1 = Pipe()
        c2, p2 = Pipe()
        c3, p3 = Pipe()

        server = PipeServer()
        server.addConnection(p1)
        server.addConnection(p2)
        server.addConnection(p3)

        req1 = 'req1'
        req2 = 'req2'
        req3 = 'req3'

        rep1 = 'rep1'
        rep2 = 'rep2'
        rep3 = 'rep3'

        c1.send(req1)
        c2.send(req2)
        c3.send(req3)

        self.assertEqual(server.waitForRequest(),req1)
        server.reply(rep1)
        self.assertEqual(c1.recv(), rep1)

        self.assertEqual(server.waitForRequest(),req2)
        server.reply(rep2)
        self.assertEqual(c2.recv(), rep2)

        self.assertEqual(server.waitForRequest(),req3)
        server.reply(rep3)
        self.assertEqual(c3.recv(), rep3)

    def testReplyBeforeRequest(self):
        server = PipeServer()
        self.assertRaises(PipePortException, server.reply, 'test')

    def testRequestBeforeReply(self):
        child_conn, parent_conn = Pipe()
        server = PipeServer(parent_conn)
        msg = 'test_message'
        child_conn.send(msg)
        req = server.waitForRequest()
        self.assertRaises(PipePortException, server.waitForRequest)

    def testAddConnections(self):
        server = PipeServer()

        req = server.waitForRequest()
        self.assertEqual(req, None)

        child_conn, parent_conn = Pipe()
        self.assertRaises(PipePortException, server.addConnection, 'test')


class TestPipeReqRep(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testGetServer(self):
        connection = PipeReqRep()
        server = connection.getServer()
        self.assertEqual(server, connection.server)
        self.assertEqual(server.connections, [])

    def testGetClient(self):
        connection = PipeReqRep()
        client1 = connection.getClient()
        client2 = connection.getClient()
        client3 = connection.getClient()

        server = connection.getServer()

        req1 = 'req1'
        req2 = 'req2'
        req3 = 'req3'

        rep1 = 'rep1'
        rep2 = 'rep2'
        rep3 = 'rep3'

        c1 = Thread(target=client1.request, args=(req1,))
        c2 = Thread(target=client2.request, args=(req2,))
        c3 = Thread(target=client3.request, args=(req3,))

        c1.start()
        c2.start()
        c3.start()
        time.sleep(0.1)

        self.assertEqual(server.waitForRequest(), req1)
        server.reply(rep1)

        self.assertEqual(server.waitForRequest(), req2)
        server.reply(rep2)

        self.assertEqual(server.waitForRequest(), req3)
        server.reply(rep3)

    def testClient(self):
        conn = PipeReqRep()
        client = conn.getClient()

        req1 = 'req1'
        rep1 = 'rep1'
        server = conn.getServer()

        p = Process(target=serverProcess, args=(server,'rep1'))
        p.start()

        self.assertEqual(client.request(req1),rep1)
        p.join()

if __name__ == "__main__":
    unittest.main()
