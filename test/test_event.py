import unittest
import logging
from multiprocessing import Pipe
import time

from taskforce_common import Event, EventHandler, EventException, Subscription
from taskforce_common import PipeSubscriber, PipePublisher, PipeBidirectional

logger = logging.getLogger(name='test_event')

TestClassName = "TestClassName"


class Destination(object):

    def callback1(self):
        pass

    def callback2(self):
        pass

    def callback3(self):
        pass

    def callback4(self):
        pass


class TestEvent(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testHashable(self):
        event1 = Event('testEvent')
        event2 = Event('testEvent')

        eventTable = {}
        eventTable[event1] = True
        self.assertTrue(event1 in eventTable)
        self.assertTrue(event2 in eventTable)

        self.assertTrue(eventTable[event1])

        eventTable[event2] = False
        self.assertFalse(eventTable[event1])

    def testHashableWithArgs(self):
        event1 = Event('testEvent', source='testSource', args=(1, 2, 3))
        event2 = Event('testEvent', source='testSource')

        self.assertEqual(event1, event2)

    def testEqual(self):
        event1 = Event('testEvent', source='testSource', args=(1, 2, 3))

        self.assertTrue(event1 == Event('testEvent', source='testSource'))
        self.assertFalse(event1 == 'apple')

    def testNotEqual(self):
        event1 = Event('testEvent', source='testSource', args=(1, 2, 3))

        self.assertFalse(event1 != Event('testEvent', source='testSource'))
        self.assertTrue(event1 != 'apple')


class TestEventHandler(unittest.TestCase):

    def setUp(self):
        self.parent_conn, self.child_conn = Pipe()
        self.publisher = PipePublisher(connection=self.parent_conn)
        self.subscriber = PipeSubscriber(connection=self.child_conn)
        self.count1 = 0
        self.count2 = 0
        self.count3 = 0
        self.count4 = 0

    def tearDown(self):
        self.publisher.disconnect()
        self.subscriber.disconnect()

    def testSubscribeEvent(self):
        e = Event('testEvent')

        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        handler.subscribeEvent('testEvent', None, TestClassName, 'callback1', False)
        self.assertEqual(len(handler.eventSubscriptions), 1)
        self.assertEqual(handler.eventSubscriptions[e], [Subscription('testEvent', None, TestClassName, 'callback1', False)])

        # Test to make sure the same event and callback does not get subscribed more than once
        handler.subscribeEvent('testEvent', None, TestClassName, 'callback1', False)
        self.assertEqual(len(handler.eventSubscriptions), 1)
        self.assertEqual(handler.eventSubscriptions[e], [Subscription('testEvent', None, TestClassName, 'callback1', False)])

        handler.subscribeEvent('testEvent', None, TestClassName, 'callback2', False)
        self.assertEqual(handler.eventSubscriptions[e], [Subscription('testEvent', None, TestClassName, 'callback1', False),
                                                         Subscription('testEvent', None, TestClassName, 'callback2', False)])

    def testSubscribeEvent_invalidDestination(self):
        handler = EventHandler('name')
        self.assertRaises(EventException, handler.subscribeEvent, "complete", "task1", "task2", "startup", False)

    def testSubscribeEvent_invalidCallback(self):
        handler = EventHandler('name')

        handler.registerDestination("test", self)
        self.assertRaises(EventException, handler.subscribeEvent, "event", "eventSource", "test", "invalid_method_name", False)

    def testUnsubscibeEvent(self):
        handler = EventHandler('handler')

        # throw exception when event doesn't exist
        self.assertRaises(EventException, handler.unsubscribeEvent, 'testEvent', 'someSource', 'someDestination')

        destination = Destination()
        handler.registerDestination(TestClassName, destination)

        handler.subscribeEvent('testEvent', 'someSource', TestClassName, 'callback1', False)
        self.assertEqual(len(handler.eventSubscriptions), 1)

        handler.unsubscribeEvent('testEvent', 'someSource', TestClassName)
        self.assertEqual(len(handler.eventSubscriptions), 0)

    def testUnsubscribeEvent_one_callback(self):
        e = Event('testEvent', source='someSource')

        handler = EventHandler('handler')

        # throw exception when event doesn't exist
        self.assertRaises(EventException, handler.unsubscribeEvent, 'testEvent', 'someSource', 'someDestination')

        destination = Destination()
        handler.registerDestination(TestClassName, destination)

        handler.subscribeEvent('testEvent', 'someSource', TestClassName, 'callback1', False)
        handler.subscribeEvent('testEvent', 'someSource', TestClassName, 'callback2', False)
        handler.subscribeEvent('testEvent', 'someSource', TestClassName, 'callback3', False)

        subscriptions = handler.getEventSubscriptions()[e]
        self.assertEqual(3, len(subscriptions))

        # Test unsubscribing from just one callback
        handler.unsubscribeEvent('testEvent', 'someSource', TestClassName, callback='callback1')
        callbacks = handler.getEventSubscriptions()[e]
        self.assertEqual(2, len(callbacks))

        handler.unsubscribeEvent('testEvent', 'someSource', TestClassName, callback='callback2')
        handler.unsubscribeEvent('testEvent', 'someSource', TestClassName, callback='callback3')

        # assert that unsubscribing the last event removes all subscriptions
        self.assertEqual(0, len(handler.getEventSubscriptions()))

    def testUnsubscribeEvent_multiple_subscribers(self):
        destination1 = Destination()
        destination2 = Destination()

        handler = EventHandler('handler')
        handler.registerDestination('sub_task1', destination1)
        handler.registerDestination('sub_task2', destination2)

        handler.subscribeEvent('shutdown', 'MainTask', 'sub_task1', 'callback1', False)
        handler.subscribeEvent('shutdown', 'MainTask', 'sub_task2', 'callback1', False)

        handler.unsubscribeEvent('shutdown', 'MainTask', 'sub_task1')

        subscriptions = handler.getEventSubscriptions()
        self.assertEqual(1, len(subscriptions))

    def testUnsubscribeEvent_active_breakpoint(self):
        parent_conn, child_conn = Pipe()
        publisher = PipePublisher(connection=parent_conn)
        subscriber = PipeSubscriber(connection=child_conn)

        destination1 = Destination()
        destination2 = Destination()

        handler = EventHandler('handler')
        handler.registerDestination('sub_task1', destination1)
        handler.registerDestination('sub_task2', destination2)
        event1 = Event(name='shutdown', source='MainTask', destination='sub_task1')

        handler.subscribeEvent('shutdown', 'MainTask', 'sub_task1', 'callback1', True)
        handler.subscribeEvent('shutdown', 'MainTask', 'sub_task2', 'callback1', False)

        handler.addPort(subscriber)
        handler.startEventLoop()

        publisher.send(event1)

        # See that breakpoint has been reached
        handler.breakpointChanged.wait(1)
        current_breakpoints = handler.getWaitingBreakpoints()
        event_info = ('shutdown', 'MainTask', 'sub_task1', 'callback1', str(()), str({}))
        self.assertTrue(event_info in current_breakpoints)

        handler.unsubscribeEvent('shutdown', 'MainTask', 'sub_task1')
        subscriptions = handler.getEventSubscriptions()
        self.assertEqual(1, len(subscriptions))
        current_breakpoints = handler.getWaitingBreakpoints()
        self.assertFalse(event_info in current_breakpoints)
        handler.stopEventLoop()

    def testAddConnection(self):
        handler = EventHandler('name')

        handler.addPort(self.subscriber)
        self.assertEqual(len(handler.subscribers), 1)
        self.assertEqual(handler.subscribers[0], self.subscriber)
        self.assertRaises(EventException, handler.addPort, self.publisher)

    def testIncomingEventLoop(self):
        parent_conn, child_conn = Pipe()
        publisher = PipePublisher(connection=parent_conn)
        subscriber = PipeSubscriber(connection=child_conn)

        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination='destination1')
        event2 = Event(name='event2', source='source1', destination=None)
        event3 = Event(name='event3', source=None, destination=None)
        event4 = Event(name='event4', source='source1', destination=None)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event3', None, TestClassName, 'callback3', False)
        handler.subscribeEvent('event4', 'source1', TestClassName, 'callback4', False)

        handler.addPort(subscriber)
        handler.startEventLoop()

        publisher.send(event1)
        publisher.send(event2)
        publisher.send(event3)
        publisher.send(event4)
        publisher.send(Event('other'))

        # putting a sleep in here so that we don't turn off
        # the loop before the eventLoop has received all events
        time.sleep(1)
        handler.stopEventLoop()

        self.assertEqual(self.count1, 1)
        self.assertEqual(self.count2, 1)
        self.assertEqual(self.count3, 1)
        self.assertEqual(self.count4, 1)

    def testBreakpointEventLoop(self):
        parent_conn, child_conn = Pipe()
        publisher = PipePublisher(connection=parent_conn)
        subscriber = PipeSubscriber(connection=child_conn)

        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination=None)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', True)

        handler.addPort(subscriber)
        handler.startEventLoop()

        publisher.send(event1)

        # See that breakpoint has been reached
        handler.breakpointChanged.wait(1)
        self.assertEqual(self.count1, 0)
        current_breakpoints = handler.getWaitingBreakpoints()
        event_info = ('event1', 'source1', TestClassName, 'callback1', str(()), str({}))
        self.assertTrue(event_info in current_breakpoints)

        # Test that we can continue from breakpoint
        continue_beakpoint_event = Event(name='event1', source='source1', destination=None, kwargs={'continue_breakpoint': True})
        publisher.send(continue_beakpoint_event)
        handler.breakpointChanged.wait(1)
        current_breakpoints = handler.getWaitingBreakpoints()
        self.assertEqual(len(current_breakpoints), 0)
        time.sleep(0.5)
        self.assertEqual(self.count1, 1)
        handler.stopEventLoop()

    def testBreakPointEventLoopDifferentArgs(self):
        parent_conn, child_conn = Pipe()
        publisher = PipePublisher(connection=parent_conn)
        subscriber = PipeSubscriber(connection=child_conn)

        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination=None)
        event2 = Event(name='event1', source='source1', destination=None, args=(1, 2))

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', True)

        handler.addPort(subscriber)
        handler.startEventLoop()

        publisher.send(event1)
        # See that breakpoint has been reached
        handler.breakpointChanged.wait(1)
        self.assertEqual(self.count1, 0)
        current_breakpoints = handler.getWaitingBreakpoints()
        event_info = ('event1', 'source1', TestClassName, 'callback1', str(()), str({}))
        self.assertTrue(event_info in current_breakpoints)

        # Send other event with different args, see that it is also waiting
        publisher.send(event2)
        handler.breakpointChanged.wait(1)
        self.assertEqual(self.count1, 0)
        current_breakpoints = handler.getWaitingBreakpoints()
        event_info = ('event1', 'source1', TestClassName, 'callback1', str((1, 2)), str({}))
        self.assertTrue(event_info in current_breakpoints)

        # Test that we can continue from breakpoint
        continue_beakpoint_event = Event(name='event1', source='source1', destination=None, kwargs={'continue_breakpoint': True})
        publisher.send(continue_beakpoint_event)
        handler.breakpointChanged.wait(1)
        current_breakpoints = handler.getWaitingBreakpoints()
        self.assertEqual(len(current_breakpoints), 1)
        time.sleep(0.5)
        self.assertEqual(self.count1, 1)

        continue_beakpoint_event = Event(name='event1', source='source1', destination=None, args=(1, 2), kwargs={'continue_breakpoint': True})
        publisher.send(continue_beakpoint_event)
        handler.breakpointChanged.wait(1)
        current_breakpoints = handler.getWaitingBreakpoints()
        self.assertEqual(len(current_breakpoints), 0)
        time.sleep(0.5)
        self.assertEqual(self.count1, 2)

        handler.stopEventLoop()

    def testBreakpointLiveUpdate(self):
        parent_conn, child_conn = Pipe()
        publisher = PipePublisher(connection=parent_conn)
        subscriber = PipeSubscriber(connection=child_conn)

        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination=None)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', True)

        handler.addPort(subscriber)
        handler.startEventLoop()

        # Turn breakpoint off and send event
        self.assertTrue(handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', False))
        publisher.send(event1)
        time.sleep(0.5)
        current_breakpoints = handler.getWaitingBreakpoints()
        self.assertEqual(len(current_breakpoints), 0)
        self.assertEqual(self.count1, 1)

        # Turn breakpoint back on and send event
        self.assertTrue(handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', True))
        publisher.send(event1)
        handler.breakpointChanged.wait(1)
        current_breakpoints = handler.getWaitingBreakpoints()
        self.assertEqual(len(current_breakpoints), 1)

        handler.stopEventLoop()

    def testBreakPointEventLoopSameArgs(self):
        parent_conn, child_conn = Pipe()
        publisher = PipePublisher(connection=parent_conn)
        subscriber = PipeSubscriber(connection=child_conn)

        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination=None)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', True)

        handler.addPort(subscriber)
        handler.startEventLoop()

        publisher.send(event1)
        # See that breakpoint has been reached
        handler.breakpointChanged.wait(1)
        self.assertEqual(self.count1, 0)
        current_breakpoints = handler.getWaitingBreakpoints()
        event_info = ('event1', 'source1', TestClassName, 'callback1', str(()), str({}))
        self.assertTrue(event_info in current_breakpoints)

        # Send same event, see that breakpoint event doesnt' get changed
        publisher.send(event1)
        handler.breakpointChanged.wait(1)

        handler.stopEventLoop()

    def testStartEventLoop_pipes(self):
        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination='destination1')
        event2 = Event(name='event2', source='source1', destination=None)
        event3 = Event(name='event3', source=None, destination=None)
        event4 = Event(name='event4', source='source1', destination=None)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event3', None, TestClassName, 'callback3', False)
        handler.subscribeEvent('event4', 'source1', TestClassName, 'callback4', False)

        pubs = []
        subs = []

        num_connections = 50

        try:
            for i in range(num_connections):
                parent_conn, child_conn = Pipe()
                pub = PipePublisher(connection=parent_conn)
                sub = PipeSubscriber(connection=child_conn)
                pub.initialize()
                sub.initialize()
                pubs.append(pub)
                subs.append(sub)
                handler.addPort(sub)
        except OSError as e:
            print e
            raise e

        time_start = time.time()
        time_out = 5
        handler.startEventLoop()

        for i in range(num_connections):
            pubs[i].send(event1)
            pubs[i].send(event2)
            pubs[i].send(event3)
            pubs[i].send(event4)

        while(time.time() - time_start < time_out):
            if (self.count1 == 1*num_connections) and  \
               (self.count2 == 1*num_connections) and \
               (self.count3 == 1*num_connections) and \
               (self.count4 == 1*num_connections):
                break
            time.sleep(0.1)

        # stopEventLoop will block until all threads are done
        handler.stopEventLoop()

        self.assertEqual(self.count1, 1*num_connections)
        self.assertEqual(self.count2, 1*num_connections)
        self.assertEqual(self.count3, 1*num_connections)
        self.assertEqual(self.count4, 1*num_connections)

        for i in range(num_connections):
            pubs[i].disconnect()
            subs[i].disconnect()

    def testRemovePort(self):
        handler = EventHandler('name')

        handler.addPort(self.subscriber)
        self.assertEqual(len(handler.subscribers), 1)
        self.assertEqual(handler.subscribers[0], self.subscriber)

        self.assertEqual(len(handler.subscribers), 1)
        handler.removePort(self.subscriber)
        self.assertEqual(len(handler.subscribers), 0)

        self.assertRaises(EventException, handler.removePort, self.subscriber)

    def testRemovePort_whileRunning(self):
        handler = EventHandler('name')

        handler.addPort(self.subscriber)

        handler.startEventLoop()
        self.assertEqual(True, handler.eventLoopRunning)

        handler.removePort(self.subscriber)
        self.assertEqual(True, handler.eventLoopRunning)

        handler.stopEventLoop()

    def testGetEventSubscriptions(self):
        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination='destination1')
        event2 = Event(name='event2', source='source1', destination=None)
        event3 = Event(name='event3', source=None, destination='destination1')
        event4 = Event(name='event4', source='source1', destination='destination1')

        self.assertEqual(handler.getEventSubscriptions(), {})

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event3', None, TestClassName, 'callback3', False)
        handler.subscribeEvent('event4', 'source1', TestClassName, 'callback4', False)

        eventSubscriptions = handler.getEventSubscriptions()
        self.assertTrue(event1 in eventSubscriptions.keys())
        self.assertTrue(event2 in eventSubscriptions.keys())
        self.assertTrue(event3 in eventSubscriptions.keys())
        self.assertTrue(event4 in eventSubscriptions.keys())

        self.assertTrue(Subscription('event1', 'source1', TestClassName, 'callback1', False) in eventSubscriptions[event1])
        self.assertTrue(Subscription('event1', 'source1', TestClassName, 'callback2', False) in eventSubscriptions[event1])
        self.assertTrue(Subscription('event2', 'source1', TestClassName, 'callback2', False) in eventSubscriptions[event2])
        self.assertTrue(Subscription('event3', None, TestClassName, 'callback3', False) in eventSubscriptions[event3])
        self.assertTrue(Subscription('event4', 'source1', TestClassName, 'callback4', False) in eventSubscriptions[event4])

    def testSubscribedEventNames(self):
        handler = EventHandler('name')

        self.assertEqual(handler.getSubscribedEventNames(), [])

        testClass = Destination()

        handler.registerDestination(TestClassName, testClass)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event3', None, TestClassName, 'callback3', False)
        handler.subscribeEvent('event4', 'source1', TestClassName, 'callback4', False)

        events = handler.getSubscribedEventNames()
        self.assertTrue('event1' in events)
        self.assertTrue('event2' in events)
        self.assertTrue('event3' in events)
        self.assertTrue('event4' in events)

    def testBidirectional_eventLoop(self):
        parent_conn, child_conn = Pipe()
        parent_port = PipeBidirectional(parent_conn)
        child_port = PipeBidirectional(child_conn)

        handler = EventHandler('name')
        handler.addPort(child_port)
        handler.registerDestination(TestClassName, self)

        event1 = Event(name='event1', source='source1', destination='destination1')
        event2 = Event(name='event2', source='source1', destination=None)
        event3 = Event(name='event3', source=None,      destination=None)
        event4 = Event(name='event4', source='source1', destination=None)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event3', None, TestClassName, 'callback3', False)
        handler.subscribeEvent('event4', 'source1', TestClassName, 'callback4', False)

        handler.startEventLoop()

        parent_port.initialize()
        parent_port.connect()

        parent_port.send(event1)
        parent_port.send(event2)
        parent_port.send(event3)
        parent_port.send(event4)
        parent_port.send(Event('other'))

        # putting a sleep in here so that we don't turn off
        # the loop before the eventLoop has received all events
        time.sleep(0.5)
        handler.stopEventLoop()

        self.assertEqual(self.count1, 1)
        self.assertEqual(self.count2, 1)
        self.assertEqual(self.count3, 1)
        self.assertEqual(self.count4, 1)

    def testEventLoopDoesntBlock(self):
        """
        Test that the event loop doesn't block

        The strategy for this test is to put one long-running event into the event queue, followed by
        two short-running events.  Although the events will enqueue and be serviced in order, they will
        execute in their own thread.  This should mean the long-running event will not block the short-running
        events.
        """
        global event1_called
        global event2_called

        event1_called = 0
        event2_called = 0

        class EventLoopTestClass:

            def longRunningCallback(self):
                global event1_called
                time.sleep(2)
                event1_called += 1

            def shortRunningCallback(self):
                global event2_called
                time.sleep(0.01)
                event2_called += 1

        testClass = EventLoopTestClass()
        parent_conn, child_conn = Pipe()
        parent_port = PipeBidirectional(parent_conn)
        child_port = PipeBidirectional(child_conn)

        handler = EventHandler('name')
        handler.addPort(child_port)
        handler.registerDestination('testClass', testClass)

        event1 = Event(name='event1', source='source1')
        event2 = Event(name='event2', source='source1')

        handler.subscribeEvent('event1', 'source1', 'testClass', 'longRunningCallback', False)
        handler.subscribeEvent('event2', 'source1', 'testClass', 'shortRunningCallback', False)
        handler.startEventLoop()

        parent_port.connect()
        parent_port.send(event1)
        parent_port.send(event2)
        parent_port.send(event2)
        parent_port.send(event2)

        time.sleep(1)

        self.assertEqual(3, event2_called)
        self.assertEqual(0, event1_called)

        time.sleep(2)

        self.assertEqual(3, event2_called)
        self.assertEqual(1, event1_called)

        handler.stopEventLoop()

    def testKillEventsOnShutdown(self):

        class TestClass:
            def blockingCallback(self):
                while(True):
                    pass

        parent_conn, child_conn = Pipe()
        parent_port = PipeBidirectional(parent_conn)
        child_port = PipeBidirectional(child_conn)

        testClass = TestClass()
        handler = EventHandler('name')
        handler.addPort(child_port)
        handler.registerDestination('testClass', testClass)

        event1 = Event(name='event1', source='source1')

        handler.subscribeEvent('event1', 'source1', 'testClass', 'blockingCallback', False)
        handler.startEventLoop()

        parent_port.connect()
        parent_port.send(event1)

        time.sleep(0.5)

        threadStatus = handler.getEventThreadStatus()
        self.assertEqual(1, len(threadStatus))

        handler.stopEventLoop()
        self.assertEqual(0, len(handler.getEventThreadStatus()))

    def test_changeName(self):
        handler = EventHandler('name')
        handler.registerDestination(TestClassName, self)

        handler.subscribeEvent('event1', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback1', False)
        handler.subscribeEvent('event2', 'source1', TestClassName, 'callback2', False)
        handler.subscribeEvent('event3', None, TestClassName, 'callback3', False)
        handler.subscribeEvent('event4', 'source1', TestClassName, 'callback4', False)

        handler.changeName('source1', 'source001')
        handler.changeName('destination1', 'destination001')

        handler.addPort(self.subscriber)
        handler.startEventLoop()

        self.publisher.initialize()
        self.publisher.connect()

        self.publisher.send(Event(name='event1', source='source001', destination='destination001'))
        self.publisher.send(Event(name='event2', source='source001', destination=None))
        self.publisher.send(Event(name='event3', source=None, destination=TestClassName))
        self.publisher.send(Event(name='event4', source='source001', destination=None))
        self.publisher.send(Event('other'))

        # putting a sleep in here so that we don't turn off
        # the loop before the eventLoop has received all events
        time.sleep(0.1)
        handler.stopEventLoop()

        self.assertEqual(self.count1, 1)
        self.assertEqual(self.count2, 1)
        self.assertEqual(self.count3, 1)
        self.assertEqual(self.count4, 1)

    def callback1(self, *args):
        self.count1 = self.count1 + 1

    def callback2(self):
        self.count2 = self.count2 + 1

    def callback3(self):
        self.count3 = self.count3 + 1

    def callback4(self):
        self.count4 = self.count4 + 1


if __name__ == "__main__":
    unittest.main()
