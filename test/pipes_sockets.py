from multiprocessing import Pipe
import os

#!/usr/bin/env python

import subprocess

from taskforce_common import logcfg
import logging
import time
import signal
logcfg.setLevel(logging.INFO)

def getProcessId(processName):
    system_call = 'ps aux | pgrep {}'.format(processName)
    output = subprocess.check_output(system_call,shell=True)
    return int(output)


def getOpenSocketsPid(processId):
    system_call = 'lsof -a -p {} | grep socket | wc -l'.format(processId)
    output = subprocess.check_output(system_call,shell=True)
    return int(output)

def getOpenSockets(processName):
    pid = getProcessId(processName)
    sockets = getOpenSocketsPid(pid)
    return sockets

def getOpenFileDescriptors(*filters, **kwargs):
    system_call = 'lsof'
    if kwargs.get('pid',None) != None:
        system_call = system_call + ' -a -p {}'.format(kwargs['pid'])

    for f in filters:
        system_call = system_call + ' | grep {}'.format(f)

    system_call = system_call + ' | wc -l'
    print system_call
    output = subprocess.check_output(system_call, shell=True)
    return int(output)


def pipe_test():

    num_connections = 32760


    pid = os.getpid()
    fd_start     = getOpenFileDescriptors(pid=pid)
    socket_start = getOpenFileDescriptors('socket',pid=pid)

    pipes = []

    try:
        for i in range(num_connections):
            pipes.append(Pipe())
    except OSError as e:
        print e
        print 'created {} pipes'.format(i+1)

    fd_open     = getOpenFileDescriptors(pid=pid)
    socket_open = getOpenFileDescriptors('socket',pid=pid)

    for pipe in pipes:
        pipe[0].close()
        pipe[1].close()

    fd_close     = getOpenFileDescriptors(pid=pid)
    socket_close = getOpenFileDescriptors('socket',pid=pid)

    fd_delta     = fd_open - fd_start
    socket_delta = socket_open - socket_start

    fd_per_conn     = fd_delta / num_connections
    socket_per_conn = socket_delta / num_connections

    print 'num pubs        : {}'.format(num_connections)
    print 'fd_start        : {}'.format(fd_start)
    print 'fd_open         : {}'.format(fd_open)
    print 'fd_close        : {}'.format(fd_close)
    print 'fd_delta        : {}'.format(fd_delta)
    print 'fd_per_conn     : {}'.format(fd_per_conn)
    print 'socket_start    : {}'.format(socket_start)
    print 'socket_open     : {}'.format(socket_open)
    print 'socket_close    : {}'.format(socket_close)
    print 'socket_delta    : {}'.format(socket_delta)
    print 'socket_per_conn : {}'.format(socket_per_conn)

if __name__ == "__main__":
    pipe_test()
