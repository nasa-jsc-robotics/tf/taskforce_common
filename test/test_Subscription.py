import unittest

from taskforce_common import Subscription


class TestEvent(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testEq(self):
        subscription1 = Subscription('testEvent', 'sourceName', 'destinationName', 'callback', False)
        self.assertEqual(subscription1, Subscription('testEvent', 'sourceName', 'destinationName', 'callback', False))

    def testHashable(self):
        subscription1 = Subscription('testEvent', 'sourceName', 'destinationName', 'callback', False)
        subscription2 = Subscription('testEvent', 'sourceName', 'destinationName', 'callback', False)

        eventTable = {}
        eventTable[subscription1] = True
        self.assertTrue(subscription1 in eventTable)
        self.assertTrue(subscription2 in eventTable)

        self.assertTrue(eventTable[subscription1])

        eventTable[subscription2] = False
        self.assertFalse(eventTable[subscription1])

    def test_item_in_list(self):
        subscription1 = Subscription('testEvent1', 'sourceName', 'destinationName', 'callback', False)
        subscription2 = Subscription('testEvent2', 'sourceName', 'destinationName', 'callback', False)
        subscription3 = Subscription('testEvent3', 'sourceName', 'destinationName', 'callback', False)
        lst = [subscription1, subscription2, subscription3]

        self.assertTrue(Subscription('testEvent1', 'sourceName', 'destinationName', 'callback', False) in lst)
        self.assertFalse(Subscription('testEvent1', 'sourceName', 'destinationName', 'callback2', False) in lst)

        lst.remove(Subscription('testEvent1', 'sourceName', 'destinationName', 'callback', False))
        self.assertEqual(len(lst), 2)

    def test_subscription_in_set(self):
        subscription1 = Subscription('testEvent1', 'sourceName', 'destinationName', 'callback', False)
        subscription2 = Subscription('testEvent2', 'sourceName', 'destinationName', 'callback', False)
        subscription3 = Subscription('testEvent3', 'sourceName', 'destinationName', 'callback', False)
        s = set()
        s.add(subscription1)
        s.add(subscription2)
        s.add(subscription3)

        self.assertTrue(subscription2 in s)

        test_subscription = Subscription('testEvent3', 'sourceName', 'destinationName', 'callback', False)
        self.assertTrue(test_subscription in s)

        s.remove(test_subscription)

        self.assertFalse(test_subscription in s)
        self.assertFalse(subscription3 in s)


if __name__ == "__main__":
    unittest.main()
