import unittest

from taskforce_common import PubSub, ReqRep, Pair


class TestConnection(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_PubSub(self):
        class MyPubSub(PubSub):
            pass

        connection = MyPubSub()
        self.assertRaises(NotImplementedError, connection.getPublisher)
        self.assertRaises(NotImplementedError, connection.getSubscriber)

    def test_ReqRep(self):
        class MyReqRep(ReqRep):
            pass

        connection = MyReqRep()
        self.assertRaises(NotImplementedError, connection.getClient)
        self.assertRaises(NotImplementedError, connection.getServer)

    def test_Pair(self):
        class MyPair(Pair):
            pass

        connection = MyPair()
        self.assertRaises(NotImplementedError, connection.getChild)
        self.assertRaises(NotImplementedError, connection.getParent)

