import unittest
import logging

from taskforce_common import Block, BlockException


class BlockSubclass(Block):

    commands = ['method_a']

    def method_a(self):
        pass


class BlockWithParameters(Block):

    parameters = {
        "int_param" : 123,
        "bool_param" : False,
        "float_param": 3.14159,
        "str_param" : "hello world!"
    }


class TestBlock(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_invalidName(self):
        self.assertRaises(BlockException, Block, '/invalidName')

    def test_parent(self):
        childBlock = Block("childBlock")
        parentBlock = Block("parentBlock")

        self.assertEqual("/childBlock", childBlock.path)
        self.assertEqual("/parentBlock", parentBlock.path)

        childBlock.parent = parentBlock
        self.assertEqual("/parentBlock/childBlock", childBlock.path)
        self.assertEqual("/parentBlock", parentBlock.path)

        self.assertEqual("parentBlock.childBlock", childBlock.logger.name)

    def test_setState(self):
        block = Block('testBlock')
        block._setState(10)

        self.assertTrue(10, block.state)
        self.assertTrue('10', block.getStateString())

    def test_changeName(self):
        block = Block('testBlock')

        self.assertEqual('testBlock', block.name)
        self.assertEqual('/testBlock', block.path)
        self.assertEqual('testBlock', block.logger.name)

        block.changeName('otherBlock', 'newBlock')

        self.assertEqual('testBlock', block.name)
        self.assertEqual('/testBlock', block.path)
        self.assertEqual('testBlock', block.logger.name)

        block.changeName('testBlock', 'newBlock')
        self.assertEqual('newBlock', block.name)
        self.assertEqual('/newBlock', block.path)
        self.assertEqual('newBlock', block.logger.name)

        self.assertRaises(BlockException, block.changeName, 'newBlock', '/newBlock')

    def test_getCommandNames(self):
        block = Block('test')
        command_names = block.getCommandNames()
        self.assertEqual(sorted(block._commands), sorted(command_names))

    def test_getInfo(self):
        childBlock = Block("childBlock")
        parentBlock = Block("parentBlock")
        childBlock.parent = parentBlock

        childInfo = childBlock.getInfo()
        self.assertEqual("/parentBlock/childBlock", childInfo["path"])
        self.assertEqual("childBlock", childInfo["name"])
        self.assertEqual("taskforce_common.task.Block", childInfo["module"])
        self.assertEqual("Block", childInfo["class"])

        self.assertEqual(logging.NOTSET, childBlock.getInfo("logLevel"))

    def test_getLogLevel(self):
        block = Block('test')

        block.setLogLevel('DEBUG')
        self.assertEqual(logging.DEBUG, block.getLogLevel())

        block.setLogLevel(logging.WARN)
        self.assertEqual(logging.WARN, block.getLogLevel())

    def test_getParameter(self):
        block = BlockWithParameters('test')

        self.assertEqual({"int_param": 123,
                          "bool_param": False,
                          "float_param": 3.14159,
                          "str_param": "hello world!"}, block.getParameter())

        self.assertEqual({"int_param": 123}, block.getParameter("int_param"))

        self.assertRaises(BlockException, block.getParameter, "invalid_key")

    def test_getParameterCopy(self):
        """
        This tests that modifying the return value does not change the internal storage of the parameter
        """
        block = BlockWithParameters('test')

        params = block.getParameter()
        self.assertEqual({"int_param": 123,
                          "bool_param": False,
                          "float_param": 3.14159,
                          "str_param": "hello world!"}, block.getParameter())

        params["int_param"] = 999

        self.assertEqual({"int_param": 123,
                          "bool_param": False,
                          "float_param": 3.14159,
                          "str_param": "hello world!"}, block.getParameter())

    def test_getParameterCopySingle(self):
        block = BlockWithParameters('test')

        params = block.getParameter("int_param")
        params["int_param"] = 999

        self.assertEqual({"int_param": 123}, block.getParameter("int_param"))

    def test_getStateString(self):
        block = Block('test')
        self.assertEqual('None', block.getStateString())

    def test_registerCommand(self):
        block = Block('test')

        def boundMethod():
            pass

        self.assertRaises(BlockException, block.registerCommand, 'boundMethod')

        block.boundMethod = boundMethod
        block.registerCommand('boundMethod')

        self.assertTrue('boundMethod' in block.getCommandNames())

    def test_registerEvent(self):
        block = Block('test')
        block.registerEvent('newEvent')

        self.assertEqual(1, len(block.registeredEvents))
        self.assertEqual(['newEvent'], block.getEventNames())

        block.registerEvent('anotherEvent')

        self.assertEqual(2, len(block.registeredEvents))
        self.assertEqual(['anotherEvent','newEvent'], block.getEventNames())

    def test_setLogLevel(self):
        block = Block('test')

        block.setLogLevel('DEBUG')
        self.assertEqual(logging.DEBUG, block.logger.level)

        block.setLogLevel(logging.WARN)
        self.assertEqual(logging.WARN, block.logger.level)

    def test_setParameter(self):
        block1 = BlockWithParameters('block1')
        block2 = BlockWithParameters('block2')

        # Verify setting a parameter for one doesn't set it for the other Block
        self.assertTrue(block1.setParameter({"int_param": 456}))

        self.assertEqual({"int_param": 456,
                          "bool_param": False,
                          "float_param": 3.14159,
                          "str_param": "hello world!"}, block1.getParameter())

        self.assertEqual({"int_param": 123,
                          "bool_param": False,
                          "float_param": 3.14159,
                          "str_param": "hello world!"}, block2.getParameter())

        # Should return False if setting an invalid parameter
        self.assertFalse(block1.setParameter({"invalid_param": "some_value"}))

    def test_status(self):
        block = BlockWithParameters('test')
        
        status = block.status()

        self.assertEqual('None', status['state'])
        self.assertEqual({"int_param": 123,
                          "bool_param": False,
                          "float_param": 3.14159,
                          "str_param": "hello world!"}, status['parameters'])
        self.assertEqual(30, status['logLevel'])
        self.assertTrue(len(status['emitEventNames']) == 0)
        self.assertEqual("BlockWithParameters", status['class'])

    def test_startup(self):
        block = Block('test')
        self.assertTrue(block.startup())

    def test_stop(self):
        block = Block('test')
        self.assertTrue(block.stop())

    def test_shutdown(self):
        block = Block('test')
        self.assertTrue(block.shutdown())

    def test_init(self):
        block = Block('test')
        self.assertTrue(block.init())
