from source_code import createObject
from json_utils import openJsonFile

"""
Plugin :
{
    name : str
    type : str
    module: str
    class: str
    args: ()
    kwargs: {}
    parameters: {}
}
"""


def importPlugins(fileName):
    """Create a dictionary of plugin objects from a plugin definition file

    Args:
        fileName: path to the plugin definition file

    Returns:
        list of plugin objects
    """
    definition = openJsonFile(fileName)

    return createPluginDictionary(definition)


def createPluginDictionary(definition):
    """Recursively parse the definition file, and create an object for every "Plugin" object

    Args:
        definition: Plugin definition as a Python dictionary

    Returns:
        list of plugin objects
    """
    if type(definition) == dict:
        d = {}
        for key, value in definition.items():
            if key == "Plugin":
                obj = createPlugin(value)
                return obj
            d[key] = createPluginDictionary(value)
        return d

    elif type(definition) == list:
        lst = []
        for item in definition:
            lst.append(createPluginDictionary(item))
        return lst


def createPlugin(pluginDef):
    """Create a plugin object from the definition

    Args:
        pluginDef: Plugin definition as a dictionary

    Returns:
        New instance of a plugin object
    """
    moduleName = pluginDef['module']
    className = pluginDef['class']
    args = pluginDef['args']
    kwargs = pluginDef['kwargs']
    plugin = createObject(moduleName, className, args, kwargs)
    return plugin
