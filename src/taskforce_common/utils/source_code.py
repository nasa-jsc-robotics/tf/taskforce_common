import importlib
import inspect
import os
import sys
import types


def createObject(moduleName, className, args=(), kwargs={}, forceReload=False):
    """Create a python object

    NOTE: This method assumes that the module is in your python path.

    Args:
        moduleName: Name of the module as a string (i.e. 'package.subpackage.module'
        className: Name of the class in the module as a string
        args: positional args for the constructor
        kwargs: key-word args for the constructor
        forceReload: Force the Python interpreter to reload the module definition

    Returns:
        Created instance of the object
    """
    cls = getClassDefinition(moduleName, className, forceReload)
    return cls(*args, **kwargs)


def getSourceCode(module_name):
    """Get the source code for an arbitrary python module

    Because of how imports occur, using inspect.getsource may miss
    important things like import statements at the top of a file, depending
    on what your __init__.py looks like.  Therefore, we get the file name
    and read the lines explicitly.  This gives us EXACTLY the same file contents.

    Args:
        module_name: String of the module name, ex: 'taskforce_demo.Cowtime'

    Returns:
        Source code of the module as a string
    """
    source_file_name = getSourceCodeFileName(module_name)
    with open(source_file_name, 'r') as f:
        source_code = f.read()
        f.close()

    return source_code


def getSourceCodeFileName(moduleName, forceReload=False):
    """Get the name of the source code file for an arbitrary python module

    Args:
        moduleName: name of the module as a string
        forceReload: Force the Python interpreter to reload the module definition

    Returns:
        File path as a string
    """
    if moduleName in sys.modules and forceReload:
        reload_module_by_name(sys.modules[moduleName])
    module = importlib.import_module(moduleName)
    return inspect.getsourcefile(module)


def getClassDefinition(moduleName, className, forceReload=False):
    """Get a type object from a module and class

    Args:
        moduleName: Name of the module (i.e. 'package.subpackage.module')
        className: Name of the class within the module
        forceReload: Force the Python interpreter to reload the module definition

    Returns:
        Type object
    """
    if forceReload:
        reload_module_by_name(moduleName)

    m = importlib.import_module(moduleName)
    if inspect.isclass(m):
        return m

    c = getattr(m, className)
    return c


def getFunctionDefinitionString(obj):
    """Return a string of a Python function or member method definition.

    Args:
        obj: Python callable

    Returns:
        String showing the function/method definition
    """
    if not callable(obj):
        raise Exception('{} is not callable'.format(obj))
    sourceLines = inspect.getsourcelines(obj)[0]
    raw_def = sourceLines[0]
    def_line = raw_def.strip()
    if def_line.startswith('def'):
        def_line = def_line[3:]
    def_line = def_line.strip().rstrip(':')
    return def_line


def reload_module(root_module):
    """Reload a module and all sub-modules

    Args:
        root_module: Python module object
    """
    package_name = root_module.__name__
    reload_module_by_name(package_name)


def reload_module_by_name(module_name):
    """Reload a Python module by name

    Args:
        module_name: name of a python module as a string
    """
    # if the .pyc file exists, it needs to be purged
    if module_name in sys.modules:
        source_path = inspect.getsourcefile(sys.modules[module_name])
        pyc_file = source_path + 'c'
        if os.path.exists(pyc_file):
            os.remove(pyc_file)

    # get a reference to each loaded module
    loaded_modules = dict([
        (key, value) for key, value in sys.modules.items()
        if key.startswith(module_name) and isinstance(value, types.ModuleType)])

    # delete references to these loaded modules from sys.modules
    for key in loaded_modules:
        del sys.modules[key]

    # load each of the modules again;
    # make old modules share state with new modules
    for key in loaded_modules:
        newmodule = __import__(key)
        oldmodule = loaded_modules[key]
        oldmodule.__dict__.clear()
        oldmodule.__dict__.update(newmodule.__dict__)
