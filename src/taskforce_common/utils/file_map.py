#!/usr/bin/env python
import os
import re


class FileMap(object):
    """
    Create a map of filenames to their absolute path.

    When a folder is added to the map, every file with the
    proper extension is added to the map, where the entry is
    relative to folder added.

    Example:
    /usr/local/share/block_library
                        /folder_a
                            file1.ext
                            file2.ext
                        /folder_b
                            file1.ext
                            file2.ext
                        otherFile.ext

    This would create the following dictionary:

    {
        "block_library/folder_a/file1.ext" : "/usr/local/share/block_library/folder_a/file1.ext"
        "block_library/folder_a/file2.ext" : "/usr/local/share/block_library/folder_a/file2.ext"
        "block_library/folder_b/file1.ext" : "/usr/local/share/block_library/folder_b/file1.ext"
        "block_library/folder_b/file2.ext" : "/usr/local/share/block_library/folder_b/file2.ext"
        "block_library/otherFile.ext"      : "/usr/local/share/block_library/otherFile.ext"
    }

    """

    def __init__(self, filters=['.*']):
        """
        Args:
            filters: list of regular expressions to filter files
        """
        self.filters = []
        self.map = {}
        self.reverseMap = {}
        self.folders = {}
        self.setFilters(filters)

    def addFolder(self, path):
        """Add a folder to the file map

        Args:
            path: path of the folder as a string
        """
        folder_name = os.path.basename(path)
        if folder_name not in self.folders.keys():
            if os.path.exists(path):
                self.folders[folder_name] = path
                self.applyFilters()

    def applyFilters(self, filters=None):
        """Apply regex filters to the FileMap

        Args:
            filters: List of regex.  If None, use the current filters
        """
        self.map = {}
        self.reverseMap = {}

        if filters is not None:
            self.setFilters(filters)

        for folder_name, path in self.getFolders().items():
            if os.path.exists(path):
                for p, dirs, files in os.walk(path, followlinks=True):
                    for file in files:
                        match = []
                        for filt in self.filters:
                            if filt.match(file):
                                match.append(True)
                            else:
                                match.append(False)
                        if all(match):
                            rel_path = os.path.join(p, file)
                            abs_path = os.path.abspath(rel_path)
                            key = rel_path.split(path, 1)[1].strip('/')
                            key = os.path.join(folder_name, key)
                            self.map[key] = abs_path
                            self.reverseMap[abs_path] = key

    def clear(self):
        """Remove all entries from the map"""
        self.map = {}
        self.reverseMap = {}
        self.folders = {}

    def getFolders(self):
        """Get the top-level folders as a list of strings"""
        return self.folders

    def getMap(self):
        """Get a dictionary of the map"""
        return self.map

    def getReverseMap(self):
        """Get the reverse of the map"""
        return self.reverseMap

    def removeFolder(self, path):
        """Remove a folder from the map"""
        folder_name = os.path.basename(path)
        self.folders.pop(folder_name)
        self.applyFilters()

    def setFilters(self, filters):
        """Set regex filters for the FileMap

        Args:
            filters: regex filters as a list of strings
        """
        for filter in filters:
            filt = re.compile(filter)
            self.filters.append(filt)
