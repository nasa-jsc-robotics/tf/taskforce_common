import json


def openJsonFile(filename):
    """Get a python dictionary from a JSON file.

    Args:
        filename: Path to the file to open as a string

    Returns:
        JSON file contents as a Python dictionary
    """
    with open(filename, 'r') as filePtr:
        definition = json.load(filePtr)
    filePtr.close()
    return definition


def saveJsonFile(filename, definition):
    """Save a Python dictionary to a JSON file.

    Args:
        filename: Path to the file as a string
        definition: JSON-formatted python dictionary
    """
    with open(filename, 'w') as filePtr:
        # NOTE: By default, the seperator between items is ", ".  So to avoid an extra whitespace
        # at the end of the line, we explicitly define the seperator
        json.dump(definition,filePtr,indent=4,sort_keys=True, separators=(',',': '))
        filePtr.write("\n") # Adding a newline to the bottom of the file because Py JSON does not
    filePtr.close()


def jsonToString(definition):
    """Convert a JSON object / dictionary to a string

    Args:
        definition: JSON object as a Python dictionary

    Returns:
        String
    """
    return json.dumps(definition,indent=4,sort_keys=True)


def stringToJson(string):
    """ Convert a JSON string into a dictionary

    Args:
        string: JSON object as a string

    Returns:
        JSON object as a Python dictionary
    """
    return json.loads(string)
