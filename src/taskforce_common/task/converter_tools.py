import copy

from taskforce_common.utils import openJsonFile, saveJsonFile

from .Group import TASKFORCE_GROUP_VERSION

replace_vals = {
    "enterBlock": "startup",
    "exitBlock": "complete",
    "<block>": "<group>"
}


def convert_block_definition(definition, target_version=TASKFORCE_GROUP_VERSION):
    """Covert a TaskForce 1.0 block file to a new target version.

    Args:
        definition: Block file definition as a Python dictionary
        target_version: version to convert to, as a string

    Returns:
        converted definition as a Python dictionary
    """
    new_def = None
    root_name = "<group>"

    if target_version == 2.0:
        new_def = {
                        "version": target_version,
                        "group" : {
                            "eventRelays" : [],
                            "fileName" : definition["block"]["file_name"].replace('.block', '.group'),
                            "name": definition["block"]["name"],
                            "parameters" : {},
                            "parameterBindings" : [],
                            "subGroups": [],
                            "subscriptions": [],
                            "tasks": []
                        }
                    }
        tasks = []
        subscriptions = []
        subGroups = []
        eventRelays_set = set()

        def parseEvent(event, subscriberName):
            """Parse an Event object and replace deprecated values

            Args:
                event: event definition as a dictionary
                subscriberName: subscriber name as a string
            """

            callback = event["callback"]
            eventName = event["event"]["name"]
            destination = subscriberName
            source = event["event"]["source"]

            eventName = definition_replace(eventName, lookup=True)
            callback = definition_replace(callback, lookup=True)

            if source == "<block>":
                relay = {
                    "eventName": eventName,
                    "sourceName": root_name
                }
                source = root_name
                eventRelays_set.add(tuple(relay.items()))

            if destination == "<block>":
                relay = {
                    "eventName": eventName,
                    "sourceName": root_name
                }
                destination = root_name
                eventRelays_set.add(tuple(relay.items()))

            subscription = {
                "callback": callback,
                "destination": destination,
                "eventName": eventName,
                "source": source,
            }
            subscriptions.append(subscription)

        for task_def in definition["block"]["tasks"]:
            task = {
                "task": {
                    "class": task_def["task"]["class"],
                    "module": task_def["task"]["module"],
                    "name": task_def["task"]["name"],
                    "parameters": task_def["task"]["parameters"]
                }
            }
            for event in task_def["task"]["subscribedEvents"]:
                parseEvent(event, task_def["task"]["name"])

            if task["task"]["name"] != "<block>":
                tasks.append(task)

        for sub_block_def in definition["block"]["sub_blocks"]:
            subGroup = {
                "name": sub_block_def["name"],
                "fileName": sub_block_def["file_name"].replace('.block', '.group')
            }

            for event in sub_block_def["subscribedEvents"]:
                parseEvent(event, sub_block_def["name"])
            subGroups.append(subGroup)

        new_def["group"]["subGroups"] = subGroups
        new_def["group"]["subscriptions"] = subscriptions
        new_def["group"]["tasks"] = tasks
        new_def["group"]["eventRelays"] = map(lambda item: dict(item), eventRelays_set)
    return new_def


def definition_replace(item, lookup=False):
    """Checks to see if an item is/has a deprecated value, and if so, replaces deprecated value in the code to valid ones

    Args:
        item: This might be either a key to the "replace_vals" dictionary or it will be a string of concatenated words
        lookup: Boolean that indicates whether the item is a key or a concatenated string to search

    Returns:
        If the item is a key (where the key is a deprecated name) to the "replace_vals" dictionary, it will return the
        key's new valid equivalent. Otherwise, it will be a string of concatenated words which will be searched for any
        existence of deprecated values, and replaced with their valid equivalent value.

    """
    if lookup:
        try:
            return replace_vals[item]
        except KeyError:
            return item
    else:
        new_item = item
        for key, value in replace_vals.items():
            new_item = new_item.replace(key, value)
        return new_item


def update_definitions(items):
    """Recursively find and replace any use of deprecated values with valid ones

    Args:
        items: Object to search for deprecated values

    Returns:
        If the object had deprecated values, replaces them and returns the object with the new valid values.
        Otherwise, if the item did not have deprecated values, just returns the items unchanged.
    """
    recursive = isinstance(items, dict) or isinstance(items, tuple)
    if recursive:
        new_val = dict()
        for name, obj in items.items():
            new_name = definition_replace(name)
            new_obj = update_definitions(obj)
            new_val[new_name] = new_obj
        return new_val
    else:
        if isinstance(items, str) or isinstance(items, unicode):
            return definition_replace(items)
        elif isinstance(items, list):
            for i in range(len(items)):
                val = items[i]
                items[i] = update_definitions(val)
            return items
        else:
            return items


def convert_scene_definition(definition, target_version):
    """Convert .scene file

    Args:
        definition: scene file content as a JSON dictionary
        target_version: target TaskForce version

    Returns:
        converted definition as a dictionary

    """
    new_def = None
    if target_version == 2.0:
        if "BlockItems" in definition.keys():
            new_def = {
                "BlockItems": {}
            }

            for name, item in definition["BlockItems"].items():
                new_items = item.items()

                new_name = definition_replace(name)
                new_blockitem_value = dict()

                for item_name, item_objects in new_items:
                    new_item_name = definition_replace(item_name)
                    new_item_objects = update_definitions(item_objects)
                    new_blockitem_value[new_item_name] = new_item_objects

                new_def["BlockItems"][new_name] = new_blockitem_value

        if "TaskViews" in definition.keys():
            new_def["TaskViews"] = []

            task_views = copy.deepcopy(definition["TaskViews"])
            new_def["TaskViews"] = task_views

        else:
            def_copy = copy.deepcopy(definition)
            new_def = def_copy
    return new_def


def convert_block_file(input_path, output_path, target_version=TASKFORCE_GROUP_VERSION):
    """Convert a block file to a new target version

    Args:
        input_path: Path to the input file
        output_path: Path to the output file
        target_version: Target version as a string
    """
    print 'converting "{}" --> "{}"'.format(input_path, output_path)
    input_definition = openJsonFile(input_path)
    output_definition = convert_block_definition(input_definition, target_version)
    saveJsonFile(output_path, output_definition)


def convert_block_scene(input_path, output_path, target_version=TASKFORCE_GROUP_VERSION):
    """Convert a scene file to a new target version

    Args:
        input_path: Path to the input .scene file
        output_path: Path to the output file
        target_version: Target version as a string
    """
    print 'converting "{}" --> "{}"'.format(input_path, output_path)
    scene_input_def = openJsonFile(input_path)
    scene_output_def = convert_scene_definition(scene_input_def, target_version)
    saveJsonFile(output_path, scene_output_def)
