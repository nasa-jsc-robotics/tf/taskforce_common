{
    "$schema": "http://json-schema.org/draft-04/schema#",
    "definitions" : {
        "parameters" : {
            "title" : "Parameters",
            "description" : "Key-value pairs of parameters",
            "type"  : "object"
        },
        "binding" :{
            "description" : "Parameter binding between the group and a contained Task/SubGroup",
            "type" : "object",
            "properties": {
                "destinationKey" : {
                    "description" : "Key of the target Task / Group",
                    "type" : "string"
                },
                "target" : {
                    "description" : "Name of the target Task / Group",
                    "type" : "string"
                }
            },
            "required" : ["destinationKey", "target"],
            "additionalProperties" : false
        },
        "parameterBinding" : {
            "type" : "object",
            "properties" : {
                "bindings" : {
                    "type": "array",
                    "items": {"$ref": "#/definitions/binding" }
                },
                "sourceKey" : {
                    "description" : "Group parameter key",
                    "type" : "string"
                }
            },
            "required" : ["bindings","sourceKey"],
            "additionalProperties" : false
        },
        "parameterBindings" : {
            "type" : "array",
            "items" : {"$ref" : "#/definitions/parameterBinding"}
        },
        "eventRelay" : {
            "type" : "object",
            "properties" : {
                "eventName" : {
                    "type" : "string",
                    "description" : "Name of the event"
                },
                "sourceName" : {
                    "type" : "string",
                    "description" : "Name of the Source"
                }
            }
        },
        "eventRelays" : {
            "description" : "List of event relays for this group",
            "type" : "array",
            "items" : {"$ref" : "#/definitions/eventRelay"}
        },
        "subscription" : {
            "type" : "object",
            "properties" : {
                "eventName" : {
                    "type" : "string",
                    "description" : "Name of the event"
                },
                "source" : {
                    "type" : "string",
                    "description" : "Name of the source of the event"
                },
                "destination" : {
                    "type" : "string",
                    "description" : "Name of the destination (subscriber) of the event"
                },
                "callback" : {
                    "type" : "string",
                    "description" : "Callback for this event"
                },
                "breakpoint" : {
                    "type" : "boolean",
                    "description" : "Whether or not this subscription has a breakpoint"
                }
            },
            "required" : ["eventName", "source", "destination", "callback"],
            "optional" : ["breakpoint"],
            "additionalProperties" : false
        },
        "subscriptions" : {
            "description" : "List of subscriptions for this Group",
            "type" : "array",
            "items" : {"$ref" : "#/definitions/subscription"}
        },
        "task" : {
            "title" : "Task",
            "description" : "Definition of a TaskForce Task",
            "type" : "object",
            "properties" : {
                "name" : {
                    "type" : "string",
                    "description" : "Instance name of the task"
                },
                "module" : {
                    "type" : "string",
                    "description" : "Module which contains the definition of this task"
                },
                "class" : {
                    "type" : "string",
                    "description" : "Class contained inside the module which defines this task"
                },
                "parameters" : {
                    "$ref" : "#/definitions/parameters"
                }
            },
            "required" : ["name", "module", "class", "parameters"],
            "additionalProperties" : false
        },
        "tasks" : {
            "description" : "list of tasks for this block",
            "type" : "array",
            "items" : {
                "type" : "object",
                "properties" : {
                    "task" : {"$ref" : "#/definitions/task"}
                },
                "additionalProperties" : false
            },
            "additionalProperties" : false
        },
        "subGroup" : {
            "description" : "A group within this group",
            "type" : "object",
            "properties" : {
                "name" : {
                    "type"        : "string",
                    "description" : "Name of this instance of the group"
                },
                "fileName" : {
                    "type" : "string",
                    "description" : "File name of the block"
                },
                "eventRelays" : {"$ref" : "#/definitions/eventRelays"},
                "parameters" : {"$ref" : "#/definitions/parameters"}
            },
            "required" : ["name", "fileName"],
            "additionalProperties" : false
        },
        "subGroups" : {
            "description" : "Array of sub-blocks for this block",
            "type"  : "array",
            "items" : {"$ref" : "#/definitions/subGroup"}
        },
        "version" : {
            "type" : "number",
            "description" : "Version of the file"
        },
        "group" : {
            "description" : "Full group description",
            "type" : "object",
            "properties" : {
                "name" : {
                    "type"        : "string",
                    "description" : "Name of this instance of the Group"
                },
                "fileName" : {
                    "type"        : "string",
                    "description" : "File name of this block"
                },
                "eventRelays" : {"$ref" : "#/definitions/eventRelays"},
                "parameterBindings" : {"$ref" : "#/definitions/parameterBindings"},
                "parameters" : {"$ref" : "#/definitions/parameters"},
                "subGroups"  : {"$ref" : "#/definitions/subGroups"},
                "subscriptions" : {"$ref" : "#/definitions/subscriptions"},
                "tasks"       : {"$ref" : "#/definitions/tasks"},
                "version"     : {"$ref" : "#definitions/version"}
            },
            "additionalProperties" : false
        }
    },
    "type" : "object",
    "properties" : {
        "parameters"       : {"$ref" : "#/definitions/parameters"},
        "eventRelay"       : {"$ref" : "#/definitions/eventRelay"},
        "subscription"     : {"$ref" : "#/definitions/subscription"},
        "task"             : {"$ref" : "#/definitions/task"},
        "group"            : {"$ref" : "#/definitions/group"},
        "subGroup"         : {"$ref" : "#/definitions/subGroup"},
        "binding"          : {"$ref" : "#/definitions/binding"},
        "parameterBinding" : {"$ref" : "#/definitions/parameterBinding"},
        "version"          : {"$ref" : "#/definitions/version"}
    },
    "additionalProperties" : false
}
