from collections import deque
from copy import deepcopy
import logging
import threading
import time

from taskforce_common import Subscriber, Bidirectional, Event
from .Subscription import Subscription

logger = logging.getLogger(__name__)


class EventException(Exception):
    pass


EVENT_THREAD_JOIN_TIMEOUT = 0.1  # timeout when trying to join an event thread


class EventHandler(object):
    """ Handle incoming events.

    The EventHandler polls it's Ports (Subscriber, or Bidirectional Port types) for incoming Event messages.  When
    an Event is received, the handler attempts to match the event with the current set of subscriptions to that event.
    For every match, the EventHandler executes the callback subscribed to the Event in it's own thread.
    """

    StopLoopMessage = 'STOP_LOOP'

    def __init__(self, name):
        """
        Args:
            name: Name of the handler as a string.  This name is used in log messages.
        """
        self.name = name
        self.subscribers = []

        # map of Events : Subscriptions
        self.eventSubscriptions = {}

        self._logger = logger.getChild('EventHandler')
        self.eventLoopRunning = False

        self.incomingEventsEnabled = True

        # map of Threads : Status, where status is a dictionary with keys ={'name', 'source', 'callback'}
        self.eventThreads = {}
        # set of breakpoints threads waiting to be run
        self.breakpointThreads = set()

        self.registeredDestinations = {}

        # thread for the incomingEventLoop
        self.eventLoopThread = None

        self.eventQueue = deque()

        self.synchEvent = threading.Event()
        self.synchEvent.clear()
        self.subscriberThreads = []

        # event for others to track when a breakpoint has changed
        self.breakpointChanged = threading.Event()
        self.breakpointChanged.clear()

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, logger):
        self._logger = logger

    def addPort(self, port):
        """
        Add a port to the handler.  Port must be of type "Subscriber" or "Bidirectional".  If it is not, an
        EventException will be raised.

        Args:
            port: Subscriber port

        Returns:
            True on success
        """
        port.logger = self.logger
        if isinstance(port, Subscriber):
            self.logger.debug('[{}] adding subscriber: {}'.format(self.name, str(port)))

            running = self.eventLoopRunning
            self.stopEventLoop()
            self.subscribers.append(port)
            if running:
                self.startEventLoop()

            return True

        elif isinstance(port, Bidirectional):
            self.logger.debug('[{}] adding subscriber: {}'.format(self.name, str(port)))

            running = self.eventLoopRunning
            self.stopEventLoop()
            self.subscribers.append(port)

            # If the event loop is already running, we need to restart
            # so that a thread will service our new port
            if running:
                self.startEventLoop()
            return True

        raise EventException(
            'Trying to add port which is not of type "Subscriber" or "Bidirectional".  Port={}, type={}'.format(
                port, type(port)))

    def changeName(self, oldName, newName):
        """Change all references from oldName to newName

        Args:
            oldName: name to replace
            newName: new name
        """
        if self.name == oldName:
            self.name = newName

        for event, callbacks in self.eventSubscriptions.items():
            update = False
            newEvent = deepcopy(event)
            if event.source == oldName:
                newEvent.source = newName
                update = True
            if event.destination == oldName:
                newEvent.destination = newName
                update = True
            if update:
                self.eventSubscriptions.pop(event)
                self.eventSubscriptions[newEvent] = callbacks

    def disconnect(self):
        """Disconnect all Ports in the handler"""
        for subscriber in self.subscribers:
            subscriber.disconnect()

    def eventLoop(self):
        """Handle all events in the event queue"""

        while self.eventLoopRunning:
            self.waitForIncomingEvents()
            if self.eventLoopRunning:
                while len(self.eventQueue) > 0:
                    event = self.eventQueue.popleft()
                    # Match the event with subscriptions
                    subscriptions = self.eventSubscriptions.get(event, [])
                    for subscription in subscriptions:
                        # the destination can be used in the event for an additional filter
                        # Only trigger callbacks for events who have a matching destination or if the
                        # event destination is None
                        if event.destination is None or event.destination == subscription.destination:
                            destination = self.registeredDestinations[subscription.destination]
                            callback = getattr(destination, subscription.callback)
                            # Create a thread object call all subscribed callbacks
                            continue_breakpoint = event.kwargs.pop('continue_breakpoint', False)
                            thread = threading.Thread(target=callback, args=event.args, kwargs=event.kwargs)
                            thread.daemon = True
                            threadInfo = (event.name,
                                          event.source,
                                          subscription.destination,
                                          subscription.callback,
                                          str(event.args),
                                          str(event.kwargs))
                            if subscription.breakpoint:
                                if threadInfo in self.breakpointThreads:
                                    if continue_breakpoint:
                                        self.breakpointThreads.remove(threadInfo)
                                        self.breakpointChanged.set()
                                        self.breakpointChanged.clear()
                                    else:
                                        self.logger.warning('[{}] got event where breakpoint is currently active. \
                                                             Ignoring event {} for subscription {}'.format(self.name, event, subscription))
                                        continue
                                else:
                                    self.breakpointThreads.add(threadInfo)
                                    self.breakpointChanged.set()
                                    self.breakpointChanged.clear()
                                    continue

                            self.eventThreads[thread] = threadInfo
                            thread.start()
                            self.logger.debug('{}: [{}] handling event subscription:{}'.format(time.time(), self.name, subscription))

            # cleanup dead event threads
            for eventThread in list(self.eventThreads.keys()):
                if not eventThread.is_alive():
                    self.eventThreads.pop(eventThread)

    def waitForIncomingEvents(self):
        """Check for incoming events on subscriber ports"""
        self.synchEvent.wait()
        self.synchEvent.clear()

    def enqueueEvent(self, event, priority=False):
        """Add an event to the queue.

        Args:
            event: Event to add to the queue as an event object
            priority: If true, this event will jump to the front of the line
        """
        if priority:
            self.eventQueue.appendleft(event)
        else:
            self.eventQueue.append(event)

    def getEventSubscriptions(self):
        """Get all the subscribed events"""
        return self.eventSubscriptions

    def getEventThreadStatus(self):
        """Get the status of the event threads"""
        return self.eventThreads.values()

    def getSubscribedEventNames(self):
        """Get a list of the names of all the subscribed events"""
        names = []
        for event in self.eventSubscriptions:
            names.append(event.name)
        return sorted(names)

    def getSubscribedEventMatches(self, name=None, source=None, destination=None, callback=None):
        """Return a list of all events that are subscribed, and match the input fields.

        NOTE: Any item with None, matches anything.

        Args:
            name : Name of the event as a string
            source: source object name as a string
            destination: target object name as a string
            callback: Callback method in the destination object as a string

        Returns:
            List of matching Event objects
        """
        matchEvent = Event(name, source, destination)
        events = []
        for event, callback_list in self.eventSubscriptions.items():
            if event.match(matchEvent):
                if (not callback) or (callback in callback_list):
                    events.append(event)
        return events

    def getWaitingBreakpoints(self):
        """Get all the subscriptions that are waiting to run"""
        return self.breakpointThreads

    def registerDestination(self, name, object):
        """Register object with the handler.

        Args:
            name: Name of the object.  This should be the "destination" of a command.
            object: Object to be registered.
        """
        self.logger.debug('[{}] register destination:"{}"'.format(self.name, name))
        self.registeredDestinations[name] = object

    def removePort(self, port):
        port_found = False
        if port in self.subscribers:
            running = self.eventLoopRunning
            if running:
                self.stopEventLoop()
            self.subscribers.remove(port)
            if running:
                self.startEventLoop()
            port_found = True

        if not port_found:
            raise EventException('Trying to remove port, but that port does not exist: Port={}, type={}'.format(port, type(port)))

    def setLogLevel(self, level):
        """Set the log level

        Args:
            level: level defined in Python's logging module
        """
        self.logger.setLevel(level)

    def startEventLoop(self):
        """Create and start a thread for every event subscriber"""
        if not self.eventLoopRunning:
            self.eventLoopRunning = True

            # Start the handler loop
            # This loop is what empties out the incoming event queue
            self.eventLoopThread = threading.Thread(target=self.eventLoop)
            self.eventLoopThread.start()

            self.subscriberThreads = []
            for subscriber in self.subscribers:
                subscriberThread = threading.Thread(target=self.subscriberThread, args=[subscriber])
                subscriberThread.daemon = True
                subscriberThread.start()
                self.subscriberThreads.append(subscriberThread)

    def stopEventLoop(self):
        """Stop all event subscriber threads"""
        if self.eventLoopRunning:
            self.eventLoopRunning = False

            # stop all the subscriberThreads
            for thread in self.subscriberThreads:
                del thread
            self.subscriberThreads = []

            # wait for the eventLoopThread to stop.  This will ensure no new threads are created
            if self.eventLoopThread is not None:
                self.enqueueEvent(EventHandler.StopLoopMessage)
                self.synchEvent.set()
                if self.eventLoopThread.is_alive():
                    self.eventLoopThread.join()

            # wait for all outstanding event threads
            for thread in self.eventThreads.keys():
                if thread.is_alive():
                    thread.join(EVENT_THREAD_JOIN_TIMEOUT)
                self.eventThreads.pop(thread)

            # clear waiting breakpoints
            if len(self.breakpointThreads) > 0:
                self.breakpointThreads.clear()
                self.breakpointChanged.set()
                self.breakpointChanged.clear()

    def subscribeEvent(self, eventName, source, destination, callback, breakpoint):
        """Subscribe a destination block to an Event

        Args:
            eventName : Name of the event as a string
            source: source block name as a string
            destination: target block name as a string
            callback: Callback method in the destination block as a string
            breakpoint: Whether or not the subscription has a breakpoint
        """
        subscription = Subscription(eventName, source, destination, callback, breakpoint)

        dest = self.registeredDestinations.get(destination, None)
        if dest is None:
            raise EventException("Subscribing to an unregistered destination. {}".format(subscription))

        if not hasattr(dest, callback):
            raise EventException("Subscribing to an event with an invalid callback. {}".format(subscription))

        event = Event(eventName, source)

        if event in self.eventSubscriptions.keys():
            if subscription in self.eventSubscriptions[event]:
                for sub in self.eventSubscriptions[event]:
                    if subscription == sub:
                        if subscription.breakpoint == sub.breakpoint:
                            return False
                        else:
                            sub.breakpoint = subscription.breakpoint
                            return True

        else:
            # if this event has not been subscribed before, create a callback vector
            self.eventSubscriptions[event] = []

        self.logger.debug('[{}] subscribe event:"{}"'.format(self.name, subscription))
        self.eventSubscriptions[event].append(subscription)
        return True

    def subscriberThread(self, subscriber):
        while self.eventLoopRunning:
            event = subscriber.receive()
            self.enqueueEvent(event)
            self.synchEvent.set()

    def unregisterDestination(self, name):
        try:
            self.registeredDestinations.pop(name)
        except KeyError:
            raise EventException('Trying to unregister destination which is not registered.  name:"{}"'.format(name))

    def unsubscribeEvent(self, eventName, source, destination, callback=None):
        """Unsubscribe an event.

        Args:
            eventName : Name of the event as a string
            source: source object name as a string
            destination: target object name as a string
            callback: Callback method in the destination object as a string
        """
        try:
            event = Event(eventName, source, destination, callback)
            subscriptions = self.eventSubscriptions[event]
            keep = []
            if callback is None:
                # if callback is none, we want to keep the subscriptions to all other destinations
                keep = [x for x in subscriptions if x.destination != destination]
            else:
                # if callback is set, we want to keep subscriptions to other destinations and
                # subscriptions to this destination but a different callback
                for subscription in subscriptions:
                    if subscription.destination != destination:
                        keep.append(subscription)
                    elif subscription.callback != callback:
                        keep.append(subscription)

            # Check if we are unsubscribing from an active breakpoint
            for breakpoint_info in list(self.breakpointThreads):
                s = Subscription(breakpoint_info[0], breakpoint_info[1], breakpoint_info[2], breakpoint_info[3], False)
                if s not in keep:
                    self.breakpointThreads.remove(breakpoint_info)

            if len(keep) == 0:
                self.eventSubscriptions.pop(event)
            else:
                self.eventSubscriptions[event] = keep

        except KeyError:
            raise EventException('Unsubscribing an event that has not been subscribed: event={}'.format(event))
