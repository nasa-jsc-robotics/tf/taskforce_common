from .Block import Block, BlockException
from taskforce_common import Event, Response, Publisher, Client


class TaskException(BlockException):
    pass


class TaskState:
    """Enumerations and maps to describe the states of a Task"""
    NOTDEPLOYED = 0
    DEPLOYED    = 10
    INITIALIZED = 20
    STARTED     = 30
    RUNNING     = 40
    STOPPED     = 50
    PAUSED      = 60
    COMPLETE    = 70
    FAILED      = 80
    SHUTDOWN    = 90


    # Map of state values to strings
    stateToString = {
        NOTDEPLOYED : 'NOTDEPLOYED',
        DEPLOYED: 'DEPLOYED',
        INITIALIZED: 'INITIALIZED',
        STARTED: 'STARTED',
        RUNNING: 'RUNNING',
        COMPLETE: 'COMPLETE',
        STOPPED: 'STOPPED',
        PAUSED: 'PAUSED',
        FAILED: 'FAILED',
        SHUTDOWN: 'SHUTDOWN'
    }

    # Map of strings to state values
    stringToState = {
        'NOTDEPLOYED' : NOTDEPLOYED,
        'DEPLOYED' : DEPLOYED,
        'INITIALIZED' : INITIALIZED,
        'STARTED' : STARTED,
        'RUNNING' : RUNNING,
        'COMPLETE' : COMPLETE,
        'STOPPED' : STOPPED,
        'PAUSED' : PAUSED,
        'FAILED' : FAILED,
        'SHUTDOWN' : SHUTDOWN,

    }


class Task(Block):
    """
    The class contains the state of the Task object and provides an API for changing the state of the object at
    run-time.  This class is meant to be sub-classed with the following methods filled in:

        onDeploy()   - Called when the Task's 'deploy' method is executed.
        onInit()     - Called on 'init' command, or when node gets a 'start' command and has not been initialized.
        onStartup()  - Called once when the node is started with a 'start' command.  Will then call 'onExecute'.
        onExecute()  - Main code for the task.  When appropriate, put your code in a 'while(True)' loop
        onStop()     - Called when the node stops.

    Note the difference between the "name" and the "path" properties.  The "name" is local to this Task, and is
    generally NOT namepsaced.  The "path", however, is the full name-spaced "path" which is calculated by calling
    this Task's parent.
    """
    # override events, commands, and parameters
    events   = []

    # don't override these:
    _events   = ['started', 'stopped', 'status', 'paused', 'resumed', 'complete', 'failed', 'shutdown']

    CustomStatusKey = 'customStatus'

    def __init__(self, name, eventPorts=[], commandPorts=[]):
        """

        Args:
            name: Name of the Task as a string
            eventPorts: List of Publisher or Bidirectional Port objects for broadcasting Events
            commandPorts: List of Ports used for commands
        """
        super(Task, self).__init__(name)

        self.customStatus = ''

        # we have this deployed flag so that we can use non-deployed Task objects as proxies for the real ones
        self._deployed = False

        self.eventPorts = set()
        self.commandPorts = []

        for port in eventPorts:
            self.addEventPort(port)

        # Register the default and user-defined events
        allEvents = self._events + self.events
        for name in allEvents:
            self.registerEvent(name)

        self._state = TaskState.NOTDEPLOYED

        self.statusThread = None

    @property
    def stateString(self):
        return TaskState.stateToString[self.state]

    def _onExecute(self):
        self.logger.info('executing {}'.format(self.name))
        self._state = TaskState.RUNNING
        rval = self.onExecute()
        if rval:
            # if TaskState != running then the task was stopped
            if self.state == TaskState.RUNNING:
                self._onComplete()
        else:
            self.logger.warn('failed onExecute')
            self._onFailed()
        return rval

    def _onComplete(self):
        self.logger.info('complete')
        self._state = TaskState.COMPLETE
        self.emit('complete')
        return True

    def _onFailed(self):
        self.logger.warning('failed')
        self._state = TaskState.FAILED
        #self.emit('failed')
        return False

    def addCommandPort(self, port):
        if not isinstance(port, Client):
            raise TaskException("Trying to add command port of type {}.  Must be of type 'Client'".format(type(port)))
        self.commandPorts.append(port)

    def addEventPort(self, port):
        if not isinstance(port, Publisher):
            raise TaskException("Trying to add event port of type {}.  Must be of type 'Publisher'".format(type(port)))
        self.eventPorts.add(port)

    def deploy(self):
        """Deploy this Task

        Returns:
            rval for onDeploy
        """
        super(Task, self).deploy()
        if self.state >= TaskState.DEPLOYED:
            raise TaskException('Attempting to deploy Task "{}", but it is already deployed.', self.path)

        rval = self.onDeploy()
        if rval:
            self.logger.info('deployed')
            self._state = TaskState.DEPLOYED
            self._deployed = True
        return rval

    def emit(self, name, *args, **kwargs):
        """
        Emit an event.

        Args:
            name: name of the event
            *args: (optional) arguments for the event
            **kwargs: (optional) key-word arguments for the event
        """
        event = Event(name, source=self.path, args=args, kwargs=kwargs)
        for port in self.eventPorts:
            port.send(event)

    def getStateString(self):
        return self.stateString

    def init(self):
        """Initialize the task.  This will call the child's onInit() method.

        If a task is initialized before it is deployed, a TaskException will be raised.

        Returns:
            Return value from onInit()
        """
        if self.state < TaskState.DEPLOYED:
            raise TaskException("Attempting to init Task before it has been deployed")

        rval = self.onInit()
        if rval:
            self.logger.info('initializing...')
            self._state = TaskState.INITIALIZED
            self.emit('initialized')
        else:
            self.logger.warning('Failed on init')
            self._onFailed()
        return rval

    def onDeploy(self):
        """override in sub-class"""
        return True

    def onExecute(self):
        """ override in sub-class"""
        return True

    def onInit(self):
        """ override in sub-class"""
        return True

    def onShutdown(self):
        """ override in sub-class"""
        return True

    def onStartup(self):
        """ override in sub-class"""
        return True

    def onStop(self):
        """ override in sub-class"""
        return True

    def onParameterChange(self, oldParams, newParams):
        """ override in sub-class"""
        return True

    def setCustomStatus(self, value):
        """Set the custom status of the Task

        Args:
            value: value for the custom status as a Python object

        Returns:

        """
        self.customStatus = value

    def setParameter(self, keyValues={}):
        oldParams = self.getParameter()
        rval = super(Task, self).setParameter(keyValues)
        if rval and self._state not in [TaskState.NOTDEPLOYED, TaskState.SHUTDOWN]:
            if oldParams != self.parameters:
                self.onParameterChange(oldParams, keyValues)
        return rval

    def startup(self):
        """Called when starting this Task with the 'startup' command.

        If the Task is already running, this method will return False,
		If the Task has not been initialized, the _onInit method will be
        called as well.  After completing the startup, the onExecute method is
        called.
        """
        if self.state == TaskState.RUNNING:
            self.logger.warn('Task is already running, returning...')
            return False

        if self.state < TaskState.INITIALIZED:
            if not self.init():
                return False

        self._state = TaskState.STARTED
        self.logger.info('starting...')
        rval = self.onStartup()
        if rval:
            self.emit('started')
            rval = self._onExecute()
        else:
            self.logger.warn('failed onStartup')
            self._onFailed()
        return rval

    def status(self, keyword=None):
        """Get the status of the Task

        Args:
            keyword: keyword Status element as a string.  If none, all status will be returned

        Returns:
            Status as a dictionary
        """
        status = super(Task, self).status()
        status[Task.CustomStatusKey] = str(self.customStatus)

        if keyword is None:
            return status
        else:
            return status[keyword]

    def stop(self):
        """Stop the task.  This will trigger the onStop() callback.

        Returns:
            Returns a the return value of onStop().  If the Task is not running, returns False
        """
        if self._state < TaskState.INITIALIZED:
            return True
            
        self.logger.info('stopping')
        rval = self.onStop()
        self._state = TaskState.STOPPED
        self.emit('stopped')
        return rval

    def serialize(self, truncate_names=True):
        """Generate the JSON definition of this Task

        Args:
            truncate_names: truncate any namespaces in the name

        Returns:
            Task definition as a Python dictionary
        """
        name = self.name
        if truncate_names:
            name = name.split('/')[-1]
        module = self.module
        className = self.className
        definition = {'task' :
                          { 'name': name,
                            'module' : module,
                            'class' : className,
                            'parameters' : self.parameters
                          }
                      }
        return definition

    def setStatus(self, status):
        """Set the status of this Task.

        The primary purpose is when the using an instance of this Task as a proxy for another Task.

        Args:
            status: new Status as a dictionary
        """
        if not self._deployed:
            self._state = TaskState.stringToState[status['state']]
            self.customStatus = status.get(Task.CustomStatusKey, None)

    def shutdown(self):
        """Shutdown the Task.  This will call onShutdown()"""
        self.logger.info('shutting down.')
        rval = self.onShutdown()
        self._state = TaskState.SHUTDOWN
        self.emit('shutdown')
        return rval
