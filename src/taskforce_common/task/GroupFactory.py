import jsonschema

from taskforce_common.utils import openJsonFile

from .factory import taskforce_schema
from .Group import Group, TASKFORCE_GROUP_VERSION
from .TaskFactory import TaskFactory


class GroupDefinitionLoader(object):
    """This class provides staticmethods for recursively loading Group files into a single definition dictionary.

    The main method is "loadDefinition" which is used by supplying a file name and a mapping of file names to full
    file paths.
    """

    def __init__(self, file_map):
        self.file_map = file_map

    @staticmethod
    def getDefinitionFromFile(file_name, file_map, validate=True):
        """Open a single Group definition file.

        Args:
            file_name: Path to the Group file as a string
            file_map: Dictionary of relative file-path stubs to fully-qualified file paths
            validate: Boolean to validate the input against a schema

        Returns:
            Group definition as a Python dictionary
        """
        try:
            filepath = file_map[file_name]
        except KeyError:
            raise KeyError('filename {} is not in the map'.format(file_name))

        definition = openJsonFile(filepath)
        if validate:
            jsonschema.validate(definition, taskforce_schema)
        return definition

    @staticmethod
    def loadDefinition(file_name, file_map, new_name=None):
        """Load a Group definition and resolve all subGroup definitions.

        Args:
            file_name: Path to the Group file as a string
            file_map: Dictionary of relative file-path stubs to fully-qualified file paths

        Returns:
            Group definition as a Python dictionary
        """
        group = GroupDefinitionLoader.getDefinitionFromFile(file_name, file_map)
        parameter_bindings = group['group']['parameterBindings']
        subGroups = group['group']['subGroups']
        tasks = group['group']['tasks']
        for subGroup in subGroups:
            subGroupDef = GroupDefinitionLoader.loadDefinition(subGroup['fileName'], file_map)
            if 'parameters' in subGroup:  # Don't break older taskforce 2.0 groups
                subGroupDef['group']['parameters'] = subGroup['parameters']
            subGroup[u'group'] = subGroupDef['group']

        for parameter_binding in parameter_bindings:
            param = group['group']['parameters'][parameter_binding['sourceKey']]
            bindings = parameter_binding['bindings']
            for binding in bindings:
                for subGroup in subGroups:
                    if binding['target'] == subGroup['name']:
                        subGroup['group']['parameters'][binding['destinationKey']] = param
                        break

                for task in tasks:
                    if binding['target'] == task['task']['name']:
                        task['task']['parameters'][binding['destinationKey']] = param
                        break

        if new_name is not None:
            group['name'] = new_name

        return group


class GroupFactory(object):
    """Factory class to construct Task and Group objects from a Group definition dictionary"""

    def __init__(self):
        self.task_factory = TaskFactory()

    def createTaskFromDefinition(self, definition, namespace=None):
        """Create a Task from the "Task" portion of a Group definition

        Args:
            definition: Task definition as a Python dictionary
            namespace: optional namespace for the created Task

        Returns:
            New taskforce_common.Task object
        """
        return self.task_factory.createTaskFromDefinition(definition, namespace)

    def buildDefinition(self, definition):
        version = definition.get('version', 'unknown')

        if version != TASKFORCE_GROUP_VERSION:
            raise Exception("Trying to parse Group definition version:{}, current:{}".format(version,
                                                                                             TASKFORCE_GROUP_VERSION))

        group = self._buildDefinition(definition)
        return group

    def _buildDefinition(self, definition):
        """
        Recursively build Task, SubGroups, etc from the definition
        Args:
            definition: group definition as a python dictionary

        Returns:
            A constructed TaskGroup object
        """
        instanceName = definition.get('name', None)
        definitionName = definition['group']['name']

        if instanceName is not None:
            groupName = instanceName
        else:
            groupName = definitionName

        fileName = definition['group']['fileName']
        group = Group(groupName, fileName)

        for taskDefinition in definition['group']['tasks']:
            task = self.createTaskFromDefinition(taskDefinition)
            group.addTask(task)

        for eventRelayDefinition in definition['group']['eventRelays']:
            group.addEventRelay(eventRelayDefinition['eventName'])

        for parameterBinding in definition['group']['parameterBindings']:
            sourceKey = parameterBinding['sourceKey']
            for binding in parameterBinding['bindings']:
                group.bindParameter(sourceKey, binding['destinationKey'], binding['target'])

        for subscription in definition['group']['subscriptions']:
            bp = subscription.get('breakpoint', False)  # Don't break existing taskforce 2 groups
            group.subscribeEvent(subscription['eventName'],
                                 subscription['source'],
                                 subscription['destination'],
                                 subscription['callback'],
                                 bp)

        for subGroupDefinition in definition['group']['subGroups']:
            subGroup = self._buildDefinition(subGroupDefinition)
            group.addSubGroup(subGroup)

        group.setParameter(definition['group']['parameters'])

        return group


def build_group_from_file(file_name, file_map):
    """Build a Group from a Group definition file.

    Args:
        file_name: path to the GroupFile name
        file_map: Dictionary of relative file-path stubs to fully-qualified file paths

    Returns:
        A constructed TaskGroup object
    """
    factory = GroupFactory()
    definition = GroupDefinitionLoader.loadDefinition(file_name, file_map)
    return factory.buildDefinition(definition)
