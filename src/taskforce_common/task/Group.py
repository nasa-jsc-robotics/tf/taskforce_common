"""
A Group is a logical, hierarchical container for Blocks.  A Group is composed of Tasks, and other Groups.  A
Group passes events through this hierarchy using EventRelays.  A Group can map its parameters to the parameters
of its children using Parameter Bindings.
"""
from copy import copy
from types import MethodType

from .Block import Block, BlockException
from .Task import Task
from .Subscription import Subscription
from taskforce_common import Event

TASKFORCE_GROUP_VERSION = 2.0


class EventRelay:
    def __init__(self, eventName, parent, eventPorts=[]):
        """Means for sending Events through the TaskGroup hierarchy.

        The EventRelay is an object that sends events on behalf of it's parent.  The typical use-case is to use within
        a TaskGroup.  When the event is sent, the sourceName will be populated with the path of the parent.

        Args:
            eventName: Name of the outgoing event as a string
            parent: Parent object
            eventPorts: Publisher ports for the event
        """
        self._parent = parent
        self.name = eventName
        self.eventName = eventName
        self.eventPorts = set()
        for port in eventPorts:
            self.addEventPort(port)
        setattr(self, eventName, MethodType(self.emit, self))

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        self._parent = value

    def addEventPort(self, port):
        """Add a Port object for emitting events

        Args:
            port: A Publisher or Bidirectional Port object
        """
        self.eventPorts.add(port)

    def emit(self, *args, **kwargs):
        """Emit an event on all Ports

        Args:
            *args: args of the Event
            **kwargs: key-word arguments of the Event

        """
        event = Event(self.eventName, source=self.parent.path, args=args[1:], kwargs=kwargs)
        for port in self.eventPorts:
            port.send(event)

    def serialize(self):
        """Get the definition of this object as a Python dictionary"""
        definition = {"sourceName" : self.parent.GroupName,
                      "eventName"  : self.eventName}
        return definition


class ParameterBinding(object):
    """Data structure to hold a Parameter binding information"""

    def __init__(self, destinationKey, target):
        """

        Args:
            destinationKey: Key for the parameter name that exists on the target as a string
            target: name of the target object as a string
        """
        self._destinationKey = destinationKey
        self._target = target

    @property
    def destinationKey(self):
        return self._destinationKey

    @property
    def target(self):
        return self._target

    def changeName(self, oldName, newName):
        """Change the name from oldName to newName

        Args:
            oldName: old name to match
            newName: new name

        Returns:

        """
        if self.target == oldName:
            self._target = newName

    def serialize(self, truncate_names=True):
        """Serialize this object to a Python dictionary

        Args:
            truncate_names: boolean to remove any name-space prefixes

        Returns:
            Serialization of this class as a Python dictionary
        """
        target = self.target
        if truncate_names:
            target = target.split('/')[-1]

        definition = {"destinationKey" : self.destinationKey,
                      "target"         : target}
        return definition

    def __str__(self):
        return str(self.serialize())

    def __repr__(self):
        return str(self.serialize())

    def __eq__(self, other):
        return ((self.destinationKey, self.target) == (other.destinationKey, other.target))

    def __ne__(self, other):
        return ((self.destinationKey, self.target) != (other.destinationKey, other.target))

    def __lt__(self, other):
        return ((self.destinationKey, self.target) < (other.destinationKey, other.target))

    def __le__(self, other):
        return ((self.destinationKey, self.target) <= (other.destinationKey, other.target))

    def __gt__(self, other):
        return ((self.destinationKey, self.target) > (other.destinationKey, other.target))

    def __ge__(self, other):
        return ((self.destinationKey, self.target) >= (other.destinationKey, other.target))

    def __hash__(self):
        return hash(self.destinationKey) + hash(self.target)

class GroupException(BlockException):
    pass


class Group(Block):
    """A Group acts as a container for Tasks and other Groups.

    Events can be passed in an out of a group using an EventRelay"
    """
    GroupName = '<group>'
    defaultRelays = ["startup", "complete", "stop", "shutdown"]

    def __init__(self, name, fileName, eventPorts=[]):
        """

        Args:
            name: name of the Group as a string
            fileName: relative-path for this file
            eventPorts: list of Publisher or Bidirectional Port objects for emitting events
        """
        super(Group, self).__init__(name)
        self._fileName = fileName

        self.commands = []
        self.eventRelays = {}
        self.subGroups = {}
        self.tasks = {}

        self.parameterBindings = {}

        # Set of subscription objects
        self.subscriptions = set()

        # The event ports help organize event communication between other blocks
        self.eventPorts = set()
        [self.addEventRelay(relay) for relay in Group.defaultRelays]    # The relays need to be added before the event ports are added
        [self.addEventPort(port) for port in eventPorts]                # Now the event ports can be added

    @property
    def fileName(self):
        return self._fileName

    def addEventPort(self, port, recursive=False):
        """Add an Event port to the Group.

        At a minimum, the port will be added to any EventRelay in this Group.  If recursive = True, the port will be
        added to all subGroups and Tasks as well.

        Args:
            port: Event port as a Publisher or Bidirectional Port object.
            recursive: boolean for adding the port to subGroups and Tasks
        """
        self.eventPorts.add(port)
        for relay in self.eventRelays.values():
            relay.addEventPort(port)

        if recursive:
            for task in self.tasks.values():
                task.addEventPort(port)

            for subGroup in self.subGroups.values():
                subGroup.addEventPort(port, recursive)

    def addEventRelay(self, eventName):
        """Create and add a new EventRelay

        Args:
            eventName: name of the event to relay as a string
        """
        relay = EventRelay(eventName, self, self.eventPorts)
        self.eventRelays[eventName] = relay
        setattr(self, eventName, MethodType(relay.emit, self))
        self.registerCommand(eventName)
        self.registerEvent(eventName)

    def addSubGroup(self, group):
        """Add a sub-group

        Args:
            group: Group object
        """
        if not isinstance(group, Group):
            raise GroupException("Trying to add a Group, but it is type:'{}'".format(type(group)))
        if group.name in self.getChildren(recursive=False):
            raise GroupException("Trying to add a Group named:'{}', but a child with that name already exists".format(group.name))
        group.parent = self
        name = str(group.name)
        self.subGroups[name] = group

    def addTask(self, task):
        """Add a Task to the Group

        Args:
            task: Task object
        """
        if not isinstance(task, Task):
            raise GroupException("Trying to add a Task, but it is type:'{}'".format(type(task)))
        if task.name in self.getChildren(recursive=False):
            raise GroupException("Trying to add a Task named:'{}', but a child with that name already exists".format(task.name))
        task.parent = self
        name = str(task.name)
        self.tasks[name] = task

    def bindParameter(self, sourceKey, destinationKey, target):
        """Bind a Task or sub-group parameter to a Group parameter

        Args:
            sourceKey: key for this group's parameter store
            destinationKey: key for the target's parameter store
            target: Target Task or Group for the parameter as a string
        """
        bindings = self.parameterBindings.get(sourceKey, set())
        bindings.add(ParameterBinding(destinationKey, target))
        self.parameterBindings[sourceKey] = bindings
        if sourceKey in self.parameters:
            self.updateParameters(sourceKey, self.parameters[sourceKey])

    def changeName(self, oldName, newName):
        """Change the name of a task of group.

        Args:
            oldName: current/old name of the object as a string
            newName: new name for the object as a string
        """
        super(Group, self).changeName(oldName, newName)

        if oldName in self.tasks:
            task = self.tasks.pop(oldName)
            task.changeName(oldName, newName)
            self.tasks[newName] = task

        if oldName in self.subGroups:
            group = self.subGroups.pop(oldName)
            group.changeName(oldName, newName)
            self.subGroups[newName] = group

        for parameterBindings in self.parameterBindings.values():
            for binding in parameterBindings:
                binding.changeName(oldName, newName)

        # Because subscriptions are held in a Python set, they have to be removed, changed and added.  This is because
        # items in a Python set are immutable.
        subscription_changes = []
        for subscription in self.subscriptions:
            if oldName in [subscription.destination, subscription.source]:
                subscription_changes.append(subscription)

        for subscription in subscription_changes:
            self.subscriptions.remove(subscription)
            subscription.changeName(oldName, newName)
            self.subscriptions.add(subscription)

    def clearAll(self):
        """Clear children, bindings, and subscriptions"""
        self.clearSubscriptions()
        self.clearBindings()
        self.clearChildren()

    def clearChildren(self):
        """Remove all children"""
        self.tasks = {}
        self.subGroups = {}

    def clearBindings(self):
        """Remove all parameter bindings"""
        self.parameterBindings = {}

    def clearSubscriptions(self):
        """Remove all subscriptions"""
        self.subscriptions = set()

    def getBlocks(self, name=None, recursive=False):
        """Get itself AND child tasks/groups

        Args:
            name: Name of the path or subgroup as a string.
            recursive: Boolean to indicate whether or not to attach all tasks/subgroups recursively

        Returns:
            Dictionary of names to objects where objects are Tasks or Groups, including a reference to itself with the
            "<group>" name. This method is necessary for when a root level block is referenced.
        """
        blocks = {}
        blocks[self.GroupName] = self
        blocks.update(self.getChildren(recursive))
        if name is not None:
            return blocks[name]
        return blocks

    def getChild(self, name, recursive=False):
        """Return the name of a child Task or Group by name

        Args:
            name: name or path of the Task or SubGroup as a string
            recursive: If True, you must supply the full path of the object.  This is the only way to directly get
                       objects stored in a subGroup.

        Returns:
            Task or Group object
        """
        objects = self.getChildren(recursive)
        try:
            return objects[name]
        except KeyError:
            raise GroupException("Trying to get object named: {}, but no object by that name exists".format(name))

    def getChildren(self, recursive=True):
        """Get all Tasks and subGroups

        Args:
            recursive: If true, getObjects from all subGroups as well

        Returns:
            Dictionary of names to objects where objects are Tasks or Groups
        """
        children = {}
        children.update(self.getSubGroups(recursive))
        children.update(self.getTasks(recursive))
        return children

    def getParameterBindings(self):
        return self.parameterBindings

    def getSubGroups(self, recursive=True):
        """Get all subGroups

        Args:
            recursive: If True, getSubGroups from all subGroups recursively

        Returns:
            Dictionary of names to subgroups
        """
        groups = self.subGroups
        if recursive:
            groups = {}
            for subGroup in self.subGroups.values():
                groups[subGroup.path] = subGroup
                groups.update(subGroup.getSubGroups(recursive))
        return groups

    def getSubscription(self, eventName, source, destination, callback):
        """
        Get the subscription with the specified parameters if it exists.

        Args:
            eventName : Name of the event as a string
            source: Source Group / Task name as a string
            destination: Target Group / Task subscribing to the event as a string
            callback: Callback method in the destination as a string

        Returns:
            The relevant subscription or None if not found
        """
        subscription = Subscription(eventName, source, destination, callback, False)
        subscriptions = self.getSubscriptions(False)
        for s in subscriptions:
            if subscription == s:
                return s
        return None

    def getSubscriptions(self, recursive=True):
        """Get all the subscriptions in this group

        Args:
            recursive: If True, getSubscriptions from all subGroups recursively

        Returns:
            A set of Subscriptions
        """
        subscriptions = self.subscriptions

        if recursive:
            subscriptions = set()
            objects = self.getBlocks(recursive=False)

            objects[Group.GroupName] = self

            for subscription in self.subscriptions:
                source = str(subscription.source)
                if source is not None:
                    source = objects[source].path

                destination = str(subscription.destination)
                if destination is not None:
                    destination = objects[destination].path
                subscriptions.add(Subscription(subscription.eventName, source, destination, subscription.callback, subscription.breakpoint))

            for group in self.subGroups.values():
                subscriptions = subscriptions.union(group.getSubscriptions(recursive))

        return subscriptions

    def getTasks(self, recursive=True):
        """Get a dictionary of the contained Tasks.

        Args:
            recursive: If True, also get sub-tasks.  This will resolve the fully-qualified path of each Task.

        Returns:
            Dictionary of {name : Task}
        """
        tasks = self.tasks
        if recursive:
            tasks = {}
            for task in self.tasks.values():
                tasks[task.path] = task
            for subGroup in self.subGroups.values():
                tasks.update(subGroup.getTasks(recursive))
        return tasks

    def removeChild(self, name):
        """Remove a child from the group.  This will also unsubscribe any events associated with this child

        Args:
            name: name of the child as a string

        Returns:
            Removed child block
        """
        child = None
        if name in self.tasks:
            child = self.tasks.pop(name)
        if name in self.subGroups:
            child = self.subGroups.pop(name)

        if child is not None:
            for subscription in copy(self.subscriptions):
                if subscription.destination == child.name or subscription.source == child.name:
                    self.unsubscribeEvent(subscription.eventName, subscription.source, subscription.destination, subscription.callback)
        return child

    def serialize(self, truncate_names=True):
        """Serialize this object as a Python dictionary

        Args:
            truncate_names: remove namespace prefixes

        Returns:
            Serialization definition of this object and sub-objects as a Python dictionary
        """
        subGroups = []
        for subGroup in self.subGroups.values():
            subGroupDef = subGroup.serialize(truncate_names)["group"]
            subGroups.append({"name": subGroupDef["name"],
                              "fileName": subGroupDef["fileName"],
                              "parameters": subGroupDef["parameters"],
                              "eventRelays": subGroupDef["eventRelays"]})

        tasks = []
        for task in self.tasks.values():
            tasks.append(task.serialize(truncate_names))

        parameterBindings = []
        for sourceKey, bindings in self.parameterBindings.items():
            bindingDefs = []
            for binding in bindings:
                bindingDefs.append(binding.serialize(truncate_names))
            parameterBindings.append({"sourceKey": sourceKey,
                                      "bindings": bindingDefs})

        eventRelays = []
        for relay in self.eventRelays.values():
            eventRelays.append(relay.serialize())

        subscriptions = []
        for subscription in self.subscriptions:
            subscriptions.append(subscription.serialize())

        definition = {
            "group": {
                "name": self.name,
                "fileName": self.fileName,
                "parameters": self.parameters,
                "parameterBindings": sorted(parameterBindings),
                "eventRelays": sorted(eventRelays),
                "tasks": sorted(tasks),
                "subscriptions": sorted(subscriptions),
                "subGroups": sorted(subGroups)
            },
            "version": TASKFORCE_GROUP_VERSION
        }
        return definition

    def setParameter(self, keyValues, override=False):
        """Set the parameters using a dictionary

        This will also set bound parameters as well.

        Args:
            keyValues: Dictionary of all parameters to set
            override: True if you are deleting values not specified in keyValues

        Returns:
            True
        """
        if override:
            self.parameters = {}

        for key, value in keyValues.items():
            self.parameters[key] = value
            self.updateParameters(key, value)

        # Remove bindings that map to keys that no longer exist
        if override:
            for sourceKey in list(self.parameterBindings.keys()):
                if sourceKey not in self.parameters and sourceKey in self.parameterBindings:
                    del self.parameterBindings[sourceKey]

        return True

    def shutdown(self):
        return True

    def status(self, keyword=None):
        """Gets the status of a group and adds additional "fileName" information

        Args:
            keyword: The name of the key used to index an item in the dictionary. If None, returns whole dictionary

        Returns:
            If keyword is None, returns the status information of the Block as a dictionary. Otherwise, returns
            the item indexed by the keyword.
        """
        status = Block.status(self, None)
        status['fileName'] = self.fileName

        if keyword is None:
            return status
        else:
            return status[keyword]

    def subscribeEvent(self, eventName, source, destination, callback, breakpoint):
        """
        Subscribed one task / port to another task/port

        Args:
            eventName : Name of the event as a string
            source: Source Group / Task name as a string
            destination: Target Group / Task subscribing to the event as a string
            callback: Callback method in the destination as a string
            breakpoint: boolean of whether or not this subscription is a breakpoint
        """
        subscription = Subscription(eventName, source, destination, callback, breakpoint)
        if subscription in self.subscriptions:
            self.subscriptions.remove(subscription)
        self.subscriptions.add(subscription)

    def unBindParameter(self, sourceKey, destinationKey, target):
        """"Remove a parameter binding

        Args:
            sourceKey: key for this group's parameter store
            destinationKey: key for the target's parameter store
            target: Target Task or Group for the parameter as a string
        """
        bindings = self.parameterBindings.pop(sourceKey)
        for binding in bindings:
            if binding.destinationKey == destinationKey and binding.target == target:
                bindings.remove(binding)
                break
        if len(bindings) > 0:
            self.parameterBindings[sourceKey] = bindings

    def unsubscribeEvent(self, eventName, source, destination, callback):
        """Unsubscribe an event

        Args:
            eventName: name of the event as a string
            source: Name of the source as a string
            destination: name of the destination as a string
            callback: name of the callback method as a string
        """
        subscription = Subscription(eventName, source, destination, callback, False)
        try:
            self.subscriptions.remove(subscription)
        except KeyError:
            raise GroupException('Trying to unsubscribe event:{}, but event does not exist'.format(subscription))

    def updateParameters(self, parameter, value):
        """Helper function to update parameter to its bound parameters

        Args:
            parameter: name of source parameter
            value: value of source parameter
        """
        bindings = self.parameterBindings.get(parameter, set())
        for binding in bindings:
            block = self.getChild(binding.target)
            block.setParameter({binding.destinationKey: value})
