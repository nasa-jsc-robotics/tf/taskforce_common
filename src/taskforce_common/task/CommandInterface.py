from taskforce_common import Command, ExceptionMessage, Message


class CommandInterface(object):
    """Interface for sending commands over an Client port

    The purpose of the CommandInterface is to provide a means to
    """

    def __init__(self, name, client):
        """

        Args:
            name: Name of this object as a string.  This will be used as the "source" of outgoing Commands
            client: A Client Port as a taskforce_common.Client
        """
        self.name = name
        self.client = client

    def sendCommand(self, name, destination, *args, **kwargs):
        """Send the command over the port and return the response.

        If and error occurs, a CommandException will be raised.

        Args:
            name: name of the method to call as a string
            destination: name of the object that contains the method in "name" as a string
            *args: Arguments to send to the method
            **kwargs: Key-work arguments to send to the method

        Returns:
            When the response is a Message, the "data" attribute of the response is returned.  Otherwise,
            the entire response object is returned.
        """
        command = Command(name=name, source=self.name, destination=destination,  args=args, kwargs=kwargs)
        response = self.client.request(command)

        if isinstance(response, ExceptionMessage):
            return response

        if isinstance(response, Message):
            return response.data
        else:
            return response
