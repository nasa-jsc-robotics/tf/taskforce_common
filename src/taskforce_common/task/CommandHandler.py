import logging
from threading import Thread, currentThread
import time
import traceback

from taskforce_common import Command, Server, ExceptionMessage, Response, ErrorResponse

logger = logging.getLogger(__name__)

COMMAND_LOOP_SLEEP_TIME = 0.05  # minimum loop-time for the command loop in seconds


class CommandException(Exception):
    pass


class CommandHandler(object):
    """Handle incoming Commands using a Request / Reply scheme

    The CommandHandler has a loop that checks it's Server port(s) for incoming Command objects from a Client.  The
    Commands are used to call a registered object's member method, and then the CommandHandler sends the return
    value of the method in a Response message.

    Once registered, an incoming command can target that object by sending a Command with the "destination" set to
    the object "name".  The specific method to call from the registered object is specified using the "name"
    field in the Command.

    Note that the CommandHandler will block when it is servicing and incoming Command.  This is to ensure
    synchronization between the Server and Client ports.
    """

    def __init__(self, name, servers=[]):
        """

        Args:
            name: Name of the CommandHandler as a string.  This value is used by the logger.
            servers: List of Server port objects.
        """
        self.name = name
        self._logger = logger.getChild('CommandHandler')

        self.servers = []
        self.commandThreads = []
        self.commandLoopRunning = False

        # Map of names : objects
        self.registeredDestinations = {}

        for server in servers:
            self.addPort(server)

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, logger):
        self._logger = logger

    def addPort(self, port):
        """
        Add Server port to the command handler.

        Each port will have it's own commandLoop.

        Args:
            port: Server port
        """
        if not isinstance(port, Server):
            raise CommandException(
                'Trying to add a server port with a non-server object.  server:"{}", type={}'.format(
                    port, type(port)))
        port.logger = self.logger
        self.servers.append(port)
        if self.commandLoopRunning:
            commandThread = Thread(target=self.commandLoop, args=(port,))
            commandThread.daemon = True
            commandThread.start()
            self.commandThreads.append(commandThread)

    def commandLoop(self, server):
        """
        Command loop for a single Server port

        Args:
            server: Server port
        """
        server.initialize()
        server.connect()
        while self.commandLoopRunning:
            command = server.waitForRequest()
            if command is not None:
                if not isinstance(command, Command):
                    response = ErrorResponse('Received a non-command object')
                else:
                    try:
                        response = Response(name=command.name, data=self.handleCommand(command))
                    except Exception:
                        trace = traceback.format_exc()
                        response = ExceptionMessage(traceback=trace, source=command.name)
                        self.logger.error(
                            '[{}] Caught exception handling command {}\n{}'.format(self.name, command.name, trace))
                self.logger.debug('[{}] reply with response "{}"'.format(self.name, response))
                try:
                    server.reply(response)
                except IOError as e:
                    self.logger.warn('[{}]:{}'.format(self.name, e))
            time.sleep(COMMAND_LOOP_SLEEP_TIME)
        self.logger.debug('[{}] command loop exiting'.format(self.name))
        if currentThread() in self.commandThreads:
            self.commandThreads.remove(currentThread())

    def commandLoopRunning(self):
        """Query if the command loop is currently running.

        Returns:
            True if running, otherwise False
        """
        alive = False
        for thread in self.commandThreads:
            if thread.is_alive():
                alive = True
        return alive

    def disconnect(self):
        """Disconnect all connected Server ports"""
        for server in self.getServerPorts():
            server.disconnect()

    def getObjects(self):
        """Return the dictionary of registered objects

        Returns:
            Dictionary of {command:callback}
        """
        return self.registeredDestinations

    def getServerPorts(self):
        """Get the Server ports for the CommandHandler.

        Returns:
            List of Server port objects
        """
        return self.servers

    def handleCommand(self, command):
        """Find a matching command, call it, and return the result

        If command is not a Command object, raise a CommandException.
        If command is not registered, raise a CommandException.
        """
        if not isinstance(command, Command):
            raise CommandException(
                'Trying to handle a non-Command object.  command="{}", type="{}"'.format(command, type(command)))

        try:
            object = self.registeredDestinations[command.destination]
        except KeyError:
            raise CommandException(
                'Trying to send command to unregistered object.  Command:"{}" source:"{}" destination:"{}"'.format(
                    command.name, command.source, command.destination))

        try:
            callback = getattr(object, command.name)
        except AttributeError:
            raise CommandException(
                'Trying to send command to invalid object method.  Command:"{}" source:"{}" destination:"{}"'.format(
                    command.name, command.source, command.destination))

        self.logger.debug('[{}] handle command: "{}", callback "{}"'.format(self.name, str(command), callback))
        return callback(*command.args, **command.kwargs)

    def registerDestination(self, name, object):
        """Register destination object with the handler.

        Args:
            name: Name of the object.  This should be the "destination" of a command.
            object: Object to be registered.

        Returns:

        """
        self.logger.debug('[{}] register object:"{}"'.format(self.name, name))
        self.registeredDestinations[name] = object

    def setLogLevel(self, level):
        """Set the log level

        Args:
            level: log level as defined in Python's logging module
        """
        self.logger.setLevel(level)

    def startCommandLoop(self):
        """Start the command loop"""
        if not self.commandLoopRunning:
            if len(self.servers) == 0:
                raise CommandException('Trying to run commandLoop with no Server ports')
            self.logger.debug('[{}] command loop starting'.format(self.name))
            self.commandLoopRunning = True
            for server in self.servers:
                commandThread = Thread(target=self.commandLoop, args=(server,))
                commandThread.daemon = True
                commandThread.start()
                self.commandThreads.append(commandThread)

    def stopCommandLoop(self):
        """Tell the command loop to stop."""
        if self.commandLoopRunning:
            self.commandLoopRunning = False

    def unregisterDestination(self, name):
        """Unregister an object by name

        Args:
            name: name of currently registered object.  If no object exists, a CommandException is raised.
        """
        try:
            self.registeredDestinations.pop(name)
        except KeyError:
            raise CommandException('Trying to unregister destination which is not registered.  name:"{}"'.format(name))
