class Subscription(object):
    """Encapsulates the concept of a Task to subscribing to another Task's events

    This class has it's own hash method based on the eventName, source, destination, and callback.  This allows objects
    of this class to be used as a key in a dictionary.

    #TODO: Add ability to enable or disable breakpoints https://bender.jsc.nasa.gov/jira/browse/TF-186
    """

    def __init__(self, eventName, source, destination, callback, breakpoint):
        """

        Args:
            eventName: Name of the Event as a string
            source: Name of the object that emits the Event as a string
            destination: Name of the object that is subscribing to the Event as a string
            callback: Name of the method in the destination object that gets called when the Event is received
            breakpoint: boolean of whether or not this subscription has a breakpoint
        """
        self.eventName = str(eventName)
        self.source = str(source)
        self.destination = str(destination)
        self.callback = str(callback)
        self.breakpoint = breakpoint

    def changeName(self, oldName, newName):
        """Change the names of the source and/or destination if it matches "oldName".  Otherwise, ignore

        Args:
            oldName: search name to replace
            newName: new name
        """
        if self.source == oldName:
            self.source = str(newName)
        if self.destination == oldName:
            self.destination = str(newName)

    def applyNameSpace(self, namespace, delimiter='/'):
        """Apply a namespace to the source and destination

        Args:
            namespace: name to prepend
            delimiter: symbol used to delimit the namespace and name
        """
        self.source = delimiter.join([namespace, self.source])
        self.destination = delimiter.join([namespace, self.destination])

    def serialize(self):
        """Output the data contents of this class as a dictionary"""
        definition = {"eventName": self.eventName,
                      "source": self.source,
                      "destination": self.destination,
                      "callback": self.callback,
                      "breakpoint": self.breakpoint}
        return definition

    def __hash__(self):
        return hash((self.eventName, self.source, self.destination, self.callback))

    def __eq__(self, other):
        if not isinstance(other, Subscription):
            return False
        return (self.eventName, self.source, self.destination, self.callback) == (
                other.eventName, other.source, other.destination, other.callback)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return "{}:{} {}:{} {}:{} {}:{} {}:{}".format('eventName', self.eventName, 'source', self.source, 'destination',
                                                      self.destination, 'callback', self.callback, 'breakpoint', self.breakpoint)

    def __repr__(self):
        return self.__str__()
