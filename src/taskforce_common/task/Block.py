import logging

from copy import deepcopy

from taskforce_common import Event
from taskforce_common.utils import getFunctionDefinitionString

logger = logging.getLogger(__name__)


class BlockException(Exception):
    pass


class Block(object):
    Delimiter = '/'

    InvalidNameCharacters = [Delimiter]

    commands = []
    events = []
    parameters = {}

    _commands = [
        'init',
        'getParameter',
        'setParameter',
        'shutdown',
        'startup',
        'status',
        'stop'
    ]

    def __init__(self, name):
        if name is None or name == "":
            raise BlockException("Trying to instantiate a Block object without a name. You must provide a name")

        for c in Block.InvalidNameCharacters:
            if c in name:
                raise BlockException(
                    "Trying to create a Block named '{}', but name contains invalid character:'{}'".format(name, c))

        self._name = name
        self._parent = None
        self.parameters = deepcopy(self.parameters)
        self.logger = None

        # Register the default and user-defined commands
        self.commandDefinitions = []
        self.registeredCommands = {}
        self.registeredEvents = {}

        self._commands = deepcopy(self._commands)
        allCommands = self._commands + self.commands
        for command in allCommands:
            self.registerCommand(command)

        self._updateCommandDefinitions()
        self._updateLoggerName()
        self._state = None

    @property
    def className(self):
        return self.__class__.__name__

    @property
    def module(self):
        return self.__module__

    @property
    def name(self):
        return self._name

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, value):
        if not isinstance(value, Block):
            raise Exception("Trying to set parent as non-block object")
        self._parent = value
        self._updateLoggerName()

    @property
    def path(self):
        if self.parent is not None:
            path = Block.Delimiter.join([self.parent.path, self.name])
        else:
            try:
                path = Block.Delimiter + self.name
            except Exception:
                logger.error("Received error when concatenating paths of sub blocks. Are you sure the name is not 'None'?")
        return path

    @property
    def state(self):
        return self._state

    def _setState(self, value):
        self._state = value

    def _updateCommandDefinitions(self):
        """Update the command definitions"""
        commandDefinitions = []
        for commandName in self.getCommandNames():
            commandDefinitions.append(getFunctionDefinitionString(self.registeredCommands[commandName]))
        self.commandDefinitions = commandDefinitions

    def _updateLoggerName(self):
        logger_name = self.path.replace(Block.Delimiter, ".")
        if logger_name.startswith("."):
            logger_name = logger_name[1:]
        self.logger = logging.getLogger(logger_name)

    def changeName(self, oldName, newName):
        """
        Change the name of all references which match oldName to newName

        Args:
            oldName: old task name
            newName: new task name
        """
        for c in Block.InvalidNameCharacters:
            if c in newName:
                raise BlockException("Changing name to '{}' but contains invalid character:'{}'".format(newName, c))
        if self.name == oldName:
            self._name = newName
            self._updateLoggerName()

    def getInfo(self, key=None):
        """Get info about this Block

        NOTE: While most of this info can change over the course of the life-cycle of the Block, it is meant to be
        the more "stable" data values.

        Args:
            key: Key value to get info from.  If None, return all values.

        Returns:
            Dictionary of key-value status pairs
        """
        info = dict()
        info['path'] = self.path
        info['name'] = self.name
        info['class'] = self.className
        info['module'] = self.module
        info['commandNames'] = self.getCommandNames()
        info['eventNames'] = self.getEventNames()
        info['logLevel'] = self.getLogLevel()
        info['commands'] = self.commandDefinitions

        if key is None:
            return info
        else:
            return info[key]

    def getCommandNames(self):
        """Get the list of registerd command names"""
        return self.registeredCommands.keys()

    def getEventNames(self):
        return sorted(self.registeredEvents.keys())

    def getLogLevel(self):
        return self.logger.level

    def getParameter(self, key=None):
        """Get one or all parameters

        Args:
            key: Name of the parameter to get.  If None, get all parameters

        Returns:
            Dictionary of parameters
        """
        if not key:
            return deepcopy(self.parameters)
        else:
            try:
                value = {key: deepcopy(self.parameters[key])}
            except KeyError:
                raise BlockException("Trying to get parameter:'{}', but that parameter does not exist".format(key))
            return value

    def getStateString(self):
        """Return the current state as a string.

        This should be implemented in the child class
        """
        return str(self.state)

    def registerCommand(self, name):
        """Register a method as a command.

        Args:
            name: name of the method to register as a command as a string.
        """
        if not hasattr(self, name):
            raise BlockException('Trying to create command:"{}" with no matching member method'.format(name))

        callback = getattr(self, name)
        self.registeredCommands[name] = callback

    def registerEvent(self, name):
        """Register a new named event.

        After creating a new named event, it can be emitted with the 'emit' method.

        Args:
            name: name of the event as a string.  This value will be in the Event's "name field, which is the primary
                  filter criteria for event matches.

        Returns:
            The new Event object
        """
        event = Event(name, source=self.path)
        self.registeredEvents[name] = event
        return event

    def setLogLevel(self, level):
        """Set log level.

        Args:
            level: Log level.  Must be one of 'info', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'

        Returns:
            New log level
        """
        self.logger.setLevel(level)
        return level

    def setParameter(self, keyValues):
        """Set the parameters using a dictionary

        Args:
            keyValues: Dictionary of all parameters to set
        """

        value_set = False
        for key, value in keyValues.items():
            if key in self.parameters.keys():
                self.parameters[key] = value
                value_set = True

        return value_set

    def status(self, keyword=None):
        """Get the status of the Block

        Args:
            keyword: keyword Status element as a string.  If none, all status will be returned

        Returns:
            Status as a dictionary
        """
        status = {}
        status['state'] = self.getStateString()
        status['parameters'] = self.parameters
        status['logLevel'] = self.getLogLevel()
        status['emitEventNames'] = self.getEventNames()
        status['class'] = self.className
        status['commands'] = self.commandDefinitions
        status['commandNames'] = self.getCommandNames()

        if keyword is None:
            return status
        else:
            return status[keyword]

    def deploy(self):
        self._updateLoggerName()

    def startup(self):
        """Start this Block.  Implement in child class"""
        return True

    def stop(self):
        """Stop this Block.  Implement in child class"""
        return True

    def shutdown(self):
        """Shutdown this Block.  Implement in child class"""
        return True

    def init(self):
        """initialize this Block.  Implement in child class"""
        return True
