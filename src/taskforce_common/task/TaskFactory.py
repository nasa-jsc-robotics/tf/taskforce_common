import logging

from taskforce_common.utils import getClassDefinition
from .factory import ValidationFactory

logger = logging.getLogger(__name__)


class TaskFactoryException(Exception):
    pass


class TaskFactory(ValidationFactory):
    """The TaskFactory will create Task objects based on the serialized definition."""

    def createTaskFromDefinition(self, definition, namespace=None, delimiter='/', validate=True):
        """Create a Task from the definition.

        The definition is assumed to conform to the taskforce_group.json schema definition of a "task",
        This method can handle parsing a task definition with or without the "task" header.

        Example:
        definition = {
            "task" : {
                "class"   : "XyzTask",
                "module" : "taskforce_plugin_xyz",
                "name"    : "MyTask",
                "parameters" : { "key1":value1, "key2":value2 }
            }
        }
        Args:
            definition: Python dictionary definition of the object
            validate: Flag to validate the definition as a bool

        Returns:
            A Task object
        """
        if validate:
            self.validate(definition)

        if len(definition) == 1:
            # Pull off the header
            task_header, task_def = definition.items()[0]
            if task_header != 'task':
                raise TaskFactoryException('Trying to create a task with a non-task definition')
        else:
            raise TaskFactoryException('Trying to create a task with a non-task definition')

        name = str(task_def['name'])
        if namespace is not None:
            name = delimiter.join([namespace, name])
        module = str(task_def['module'])
        className = str(task_def['class'])
        params = task_def.get('parameters', {})

        try:
            classDef = getClassDefinition(module, className)
            logger.debug('found Task:{} ({}.{})'.format(name, module, className))
        except ImportError as e:
            raise TaskFactoryException(
                'Import Error when creating Task.  definition={}, Error={}'.format(definition, e))

        try:
            # instantiate the task.  The full_name is always the first argument for any Task.
            task = classDef(name)
            task.setParameter(params)
        except TypeError as e:
            raise TaskFactoryException(
                'Error creating instance of Task.  definition={}. Error={}'.format(definition, e))

        return task
