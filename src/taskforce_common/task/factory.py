import inspect
import json
import jsonschema
import logging
import os

from taskforce_common.utils import getClassDefinition, openJsonFile

logger = logging.getLogger(__name__)


def getTaskforceSchema():
    """Get the TaskForce JSON schema"""
    current_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    schema_path = os.path.join(current_path, 'resources', 'json', 'schemas', 'taskforce_schema.json')

    with open(schema_path) as file_object:
        schema = json.load(file_object)
    file_object.close()
    return schema

# module variable to hold the schema
taskforce_schema = getTaskforceSchema()


def getGroupDefinitionFromFile(filename):
    """Get the contents of a Group file by filename.

    Args:
        filename: path to a file as a string

    Returns:
        Group file contents as a JSON dictionary
    """
    with open(filename) as file_object:
        definition = json.load(file_object)
    file_object.close()
    return definition


def getNamespacedName(name, namespaces=[], delimiter='/'):
    """Construct a name-spaced name

    Args:
        name: Suffix for the name
        namespaces: Sub-spaces as a list of strings
        delimiter: Delimiter between the name-space "levels"

    Returns:
        Name-spaced name
    """
    nameSpacedName = delimiter.join(namespaces + [name])
    return nameSpacedName.replace(delimiter+'<block>', '')


class ValidationFactory(object):
    """Base class for other factories which wish to validate a JSON definition against a schema"""

    def __init__(self, schemaFile=None):
        """

        Args:
            schemaFile: Filename of the schema for validation as a string
        """
        self.schema = taskforce_schema

        if schemaFile != None:
            with open(schemaFile) as file_object:
                self.schema = json.load(file_object)
            file_object.close()

    def validate(self, input):
        """Validate the input against the schema

        Args:
            input: Input to be validated

        Returns:
            True on successful validation or if the schema is not specified.  Unsuccessful validation throws a
            jsonschema.exceptions.ValidationError
        """
        if hasattr(self, 'schema'):
            jsonschema.validate(input, self.schema)
        return True
