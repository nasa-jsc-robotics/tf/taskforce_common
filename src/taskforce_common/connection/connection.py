class ConnectionException(Exception):
    pass


class Connection(object):
    pass


class PubSub(Connection):
    """
    A PubSub Connection is one where there is one publisher and one or more subscribers.  This is an abstract
    class that needs to be sub-classed and implemented with a particular transport.
    """

    def getPublisher(self, *args, **kwargs):
        raise NotImplementedError('must sub-class abstract PubSub connection')

    def getSubscriber(self, *args, **kwargs):
        raise NotImplementedError('must sub-class abstract PubSub connection')


class ReqRep(Connection):
    """
    A ReqRep Connection is a request-reply connection.  Here, one side acts as a server, and others act as a client.

    Messages are sent from a client to the server, and then the server sends a response.  This is an abstract class
    that needs to be sub-classed and implemented with a particular transport.
    """

    def getServer(self, *args, **kwargs):
        raise NotImplementedError('must sub-class abstract ReqRep connection')

    def getClient(self, *args, **kwargs):
        raise NotImplementedError('must sub-class abstract ReqRep connection')


class Pair(Connection):
    """
    A Pair connection is a one-to-one, bi-directional communication scheme.  Data flows in either direction between
    the parent and the child.

    This is an abstract class that needs to be sub-classed and implemented with a particular transport.
    """

    def getParent(self, *args, **kwargs):
        raise NotImplementedError('must sub-class abstract Pair connection')

    def getChild(self, *args, **kwargs):
        raise NotImplementedError('must sub-class abstract Pair connection')
