from Queue import Queue

from .connection import PubSub, ReqRep, Pair
from .queue_port import QueuePublisher, QueueSubscriber, QueueBidirectional, QueueServer, QueueClient
from .queue_port import QUEUE_POLL_TIMEOUT_SECONDS


class QueuePubSub(PubSub):
    """PubSub Connection based on Python Queue objects."""

    def __init__(self, poll_timeout=QUEUE_POLL_TIMEOUT_SECONDS):
        self.publisher = QueuePublisher()
        self.poll_timeout = poll_timeout

    def getPublisher(self):
        """Get the Publisher object."""
        return self.publisher

    def getSubscriber(self):
        """Create an return a new subscriber object"""
        queue = Queue()
        sub = QueueSubscriber(queue, self.poll_timeout)
        self.publisher.addConnection(queue)
        return sub


class QueuePair(Pair):
    """Pair Connection based on Python Queue objects."""

    def __init__(self, poll_timeout=QUEUE_POLL_TIMEOUT_SECONDS):
        parentToChild = Queue()
        childToParent = Queue()
        self.parent = QueueBidirectional((parentToChild, childToParent), poll_timeout)
        self.child = QueueBidirectional((childToParent, parentToChild), poll_timeout)

    def getParent(self):
        """Get the Parent of the Pair"""
        return self.parent

    def getChild(self):
        """Get the Child of the Pair"""
        return self.child


class QueueReqRep(ReqRep):
    """ReqRep Connection based on Python Queue objects"""

    def __init__(self):
        self.server = QueueServer()

    def getServer(self):
        """Get the server object"""
        return self.server

    def getClient(self):
        """Create and return a new client object"""
        serverToClient = Queue()
        clientToServer = Queue()
        client = QueueClient((clientToServer, serverToClient))
        self.server.addConnections((serverToClient, clientToServer))
        return client
