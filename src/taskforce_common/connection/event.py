class Event(object):
    """
    The Event class is used as an announcement to subscribers in order to trigger other actions.

    This object has a custom hashing method that allows it to act as a key in a dictionary.  The hash is based on the
    name and source values of the object.  Likewise, the "equals" operator has been overloaded to check if the "name"
    and "source" attributes match between two objects.
    """

    def __init__(self, name, source=None, destination=None, args=(), kwargs={}):
        """
        Args:
            name: The name of the event as a string.
            source: The name of the sender of this event, as a string
            destination: The name of the target for this event, as a string
            args: Args to be sent to the method as a list
            kwargs: Key-word arguments to send to the method as a dictionary
        """
        self.name = name
        self.source = source
        self.destination = destination
        self.args = args
        self.kwargs = kwargs

    def __hash__(self):
        return hash((self.name, self.source))

    def __eq__(self, other):
        if not isinstance(other, Event):
            return super(Event, self) == other
        return (self.name, self.source) == (other.name, other.source)

    def __ne__(self, other):
        if not isinstance(other, Event):
            return super(Event, self) != other
        return not self.__eq__(other)

    def __str__(self):
        return 'name:{} source:{} destination:{} args:{} kwargs:{}'.format(self.name, self.source, self.destination, self.args, self.kwargs)

    def __repr__(self):
        return self.__str__()
