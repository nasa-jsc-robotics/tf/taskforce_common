from multiprocessing import Pipe

from .connection import PubSub, ReqRep, Pair
from .pipe_port import PipePublisher, PipeSubscriber, PipeBidirectional, PipeServer, PipeClient
from .pipe_port import PIPE_POLL_TIMEOUT_SECONDS


class PipePubSub(PubSub):
    """A PubSub Connection that uses Python multiprocessing.Pipe objects."""
    def __init__(self, poll_timeout=PIPE_POLL_TIMEOUT_SECONDS):
        self.publisher = PipePublisher()
        self.poll_timeout = poll_timeout

    def getPublisher(self):
        """Get the publisher object """
        return self.publisher

    def getSubscriber(self):
        """Create and return a new PipeSubscriber object"""
        child_conn, parent_conn = Pipe()
        sub = PipeSubscriber(child_conn, self.poll_timeout)
        self.publisher.addConnection(parent_conn)
        return sub


class PipePair(Pair):
    """A Pair Connection that uses Python multiprocessing.Pipe objects"""
    def __init__(self, poll_timeout=PIPE_POLL_TIMEOUT_SECONDS):
        self.child_conn, self.parent_conn = Pipe()
        self.parent = PipeBidirectional(self.parent_conn, poll_timeout)
        self.child = PipeBidirectional(self.child_conn, poll_timeout)

    def getParent(self):
        """Get the Parent of the pair"""
        return self.parent

    def getChild(self):
        """Get the Child of the pair"""
        return self.child


class PipeReqRep(ReqRep):
    """A ReqRep Connection that uses Python multiprocessing.Pipe objects"""
    def __init__(self):
        self.server = PipeServer()

    def getServer(self):
        """Get the Server object"""
        return self.server

    def getClient(self):
        """Create and return a new Client object"""
        child_conn, parent_conn = Pipe()
        client = PipeClient(child_conn)
        self.server.addConnection(parent_conn)
        return client
