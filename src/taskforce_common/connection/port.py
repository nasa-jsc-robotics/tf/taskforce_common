import logging


class PortException(Exception):
    pass


class Port(object):
    """The base class used by connections to transfer data.

    The Port is a base class that is meant to be sub-classed using some transport implementation.
    Instead of sub-classing the "Port" class directly, sub-class one of: Publisher, Subscriber, Client, Server, or
    Bidirectional port types.

    The "logger" property of this class is used for logging messages.
    """
    portType = 'Port'

    def __init__(self):
        self.connected = False
        self.initialized = False
        self._logger = logging.getLogger(self.portType)

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, logger):
        self._logger = logger.getChild(self.portType)

    def initialize(self):
        """
        Initialize the port.

        Initialization of a port usually needs to happen in the Process
        in which it is used.

        Returns:
            True on success
        """
        self.initialized = True
        return True

    def connect(self):
        """
        Connect the port.

        Connection of a port usually needs to happen in the Process in
        which it is used.

        Returns:
            True on success
        """
        if not self.initialized:
            init_ok = self.initialize()
            if not init_ok:
                return False

        self.connected = True
        return True

    def disconnect(self):
        """Disconnect the port.

        Disconnecting a port usually needs to happen in the Process in
        which it is used.

        Returns:
            True on success
        """
        self.connected = False
        return True

    def registerCallback(self, callable, *args, **kwargs):
        """Register a callback to this port

        Some protocols allow for a callback.  Use this API to register your callback.
        NOTE: In order to use this method, it must be implemented in the sub-class.

        Args:
            callable: Python callable object
            *args: arguments to send to the callable
            **kwargs: key-word arguments to send to the callable
        """
        raise NotImplementedError('Must be implemented by subclass')


class Publisher(Port):
    portType = 'Publisher'

    def send(self, message):
        """Send a message over the connection.

        This method must be implemented by the subclass.

        Args:
            message: message to send as a taskforce_common.Message (or sub-class)

        Returns:
            True on success
        """
        raise NotImplementedError


class Subscriber(Port):
    portType = 'Subscriber'

    def receive(self):
        """Receive message over the connection.

        This method must be implemented by the subclass.

        returns:
            Incoming message as a taskforce_common.Message
        """
        raise NotImplementedError


class Client(Port):
    portType = 'Client'

    def request(self, message):
        """Send an out-going request to a Server and wait for a reply.

        This method must be implemented by the subclass. If the server times out, a PortException will be raised

        Args:
            message: Out-going message to the server as a taskforce_common.Message

        Returns:
            Reply from the server.
        """
        raise NotImplementedError


class Server(Port):
    portType = 'Server'

    def waitForRequest(self):
        """Wait for an incoming request from a Client.

        This method must be implemented by the subclass.

        A Server can be connected to any number of Clients.  When an incoming request is received,
        a Server must reply to the client using it's reply method.

        It is up to the sub-class to determine whether or not make this method blocking

        Returns:
            Incoming message from a Client as a taskforce_common.Message
        """
        raise NotImplementedError

    def reply(self, data):
        """Reply to incoming request.

        This method must be implemented by the subclass.

        After a Server has received a request from it's waitForRequest method, it replies to the Client using this
        method.

        Args:
            data: Message to send back to the Client as a taskforce_common.Message

        Returns:
            True on successful reply
        """
        raise NotImplementedError


class Bidirectional(Port):
    portType = 'Bidirectional'

    def send(self, message):
        """Send a message over the connection.

        This method must be implemented by the subclass.

        Args:
            message: Message to send as a taskforce_common.Message.

        Returns:
            True on successful send
        """
        raise NotImplementedError

    def receive(self):
        """Receive message over the connection.

        This method must be implemented by the subclass.

        Returns:
            Incoming message as a taskforce_common.Message
        """
        raise NotImplementedError
