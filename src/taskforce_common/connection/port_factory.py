class ClientFactory(object):
    """Class to create taskforce_common.port.Client objects"""

    def __init__(self, clientClass, **kwargs):
        """
        Args:
            clientClass: client class to create when requested
            kwargs: key-word arguments to send to the constructor for the client class
        """
        self.clientClass = clientClass
        self.kwargs = kwargs

    def createClient(self):
        """
        Create an instance of a client described in the constructor

        Returns:
            Instance of client

        """
        return self.clientClass(**self.kwargs)
