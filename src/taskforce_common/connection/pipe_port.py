import logging
import _multiprocessing
from .port import PortException, Publisher, Subscriber, Server, Client, Bidirectional

logger = logging.getLogger(__name__)

# Polling timeout for multiprocessing.Pipe objects
PIPE_POLL_TIMEOUT_SECONDS = 0.0


class PipePortException(PortException):
    pass


class PipePublisher(Publisher):
    """Publisher Port based on multiprocessing.Pipe objects"""
    def __init__(self, connection=None):
        """

        Args:
            connection: a _multiprocessing.Connection object.  One of the objects created when calling
                        multiprocessing.Pipe()
        """
        super(PipePublisher, self).__init__()
        self._logger = logging.getLogger('PipePublisher')
        self.connections = []
        if connection is not None:
            self.addConnection(connection)

    def send(self, data):
        """Send data through the Port

        Args:
            data: a Python object.
        """
        if not self.connected:
            self.connect()

        self.logger.debug('send:{}'.format(str(data)))
        for connection in self.connections:
            connection.send(data)

    def disconnect(self):
        """Disconnect the port.

        Returns:
            True on success.  False if it was not connected.
        """
        if self.connected:
            for connection in self.connections:
                connection.close()
            self.connected = False
            return True
        return False

    def addConnection(self, connection):
        """Add a connection to Publisher

        Args:
            connection: a _multiprocessing.Connection object.  This is in the return value of a call to
                        multiprocessing.Pipe()
        """
        if not isinstance(connection, _multiprocessing.Connection):
            raise PipePortException('Adding connection with a non-pipe object: connection={}, type={}'.format(connection,type(connection)))

        self.connections.append(connection)


class PipeSubscriber(Subscriber):

    def __init__(self, connection, poll_timeout=PIPE_POLL_TIMEOUT_SECONDS):
        """

        Args:
            connection: a _multiprocessing.Connection object
            poll_timeout: Poll timeout for the Connection.recv() method in seconds
        """
        super(PipeSubscriber, self).__init__()
        self._logger = logging.getLogger('PipeSubscriber')

        if not isinstance(connection, _multiprocessing.Connection):
            raise PipePortException('Initializing with a non-pipe object: connection={}, type={}'.format(connection,type(connection)))

        self.conn = connection
        self.poll_timeout = poll_timeout

    def receive(self):
        """Check the subscriber for new data

        Returns:
            New data, or None if the poll times out
        """
        if not self.connected:
            self.connect()
        if self.conn.poll(self.poll_timeout):
            try:
                data = self.conn.recv()
                self.logger.debug('receive:{}'.format(str(data)))
                return data
            except EOFError:
                pass
        return None

    def disconnect(self):
        """Disconnect the subscriber"""
        if self.connected:
            self.conn.close()
            self.connected = False


class PipeServer(Server):

    def __init__(self, connection=None, poll_timeout=PIPE_POLL_TIMEOUT_SECONDS):
        """

        Args:
            connection: a _multiprocessing.Connection object
            poll_timeout: Poll timeout for the Connection.recv() method in seconds
        """
        super(PipeServer, self).__init__()
        self._logger = logging.getLogger('PipeServer')
        self.poll_timeout = poll_timeout
        self.connections = []
        self.reply_conn = None
        if connection is not None:
            self.addConnection(connection)

    def waitForRequest(self):
        """Wait for incoming server request

        Returns:
            Server request data or None on timeout.
        """
        if not self.connected:
            self.connect()
        if self.reply_conn is not None:
            raise PipePortException('Waiting for another request with a pending reply')

        for conn in self.connections:
            if conn.poll(self.poll_timeout):
                try:
                    data = conn.recv()
                    self.logger.debug('request:{}'.format(str(data)))
                    self.reply_conn = conn
                    return data
                except EOFError as e:
                    pass
        return None

    def reply(self, data):
        """Reply to an incoming request

        Args:
            data: data to return to the client

        Returns:
            Python object.  Typically a sub-class of a taskforce.Message object
        """
        if self.reply_conn is None:
            raise PipePortException('Sending reply with no pending requests')
        self.logger.debug('reply:{}'.format(str(data)))
        self.reply_conn.send(data)
        self.reply_conn = None

    def disconnect(self):
        """Disconnect the server

        Returns:
            True on success, False if not connected
        """
        if self.connected:
            for connection in self.connections:
                connection.close()
            self.connected = False
            return True
        return False

    def addConnection(self, connection):
        """Add a connection to the server

        Args:
            connection: a _multiprocessing.Connection object
        """
        if not isinstance(connection, _multiprocessing.Connection):
            raise PipePortException(
                'Adding connection with a non-pipe object: connection={}, type={}'.format(connection, type(connection)))

        self.connections.append(connection)


class PipeClient(Client):

    def __init__(self, connection, reply_timeout=5):
        """

        Args:
            connection: a _multiprocessing.Connection object
            reply_timeout: Maximum wait time to receive a reply
        """
        super(PipeClient, self).__init__()
        self._logger = logging.getLogger('PipeClient')
        if not isinstance(connection, _multiprocessing.Connection):
            raise PipePortException(
                'Initializing with a non-pipe object: connection={}, type={}'.format(connection, type(connection)))

        self.conn = connection
        self.reply_timeout = reply_timeout

    def request(self, data):
        """Send a request to a Server

        Args:
            data: Data sent to the server.  Typically a taskforce.Message or subclass

        Returns:
            Reply from the server or None on timeout.
        """
        self.conn.send(data)
        if self.conn.poll(self.reply_timeout):
            reply = self.conn.recv()
            self.logger.debug('reply:{}'.format(str(reply)))
            return reply
        return None


class PipeBidirectional(Bidirectional):

    def __init__(self, connection, poll_timeout=PIPE_POLL_TIMEOUT_SECONDS):
        """

        Args:
            connection: a _multiprocessing.Connection object
            poll_timeout: Poll timeout for the Connection.recv() method in seconds
        """
        super(PipeBidirectional, self).__init__()
        self._logger = logging.getLogger('PipeBidirectional')
        if not isinstance(connection, _multiprocessing.Connection):
            raise PipePortException(
                'Initializing with a non-pipe object: connection={}, type={}'.format(connection, type(connection)))

        self.conn = connection
        self.poll_timeout = poll_timeout

    def send(self, data):
        """Send data

        Args:
            data: data object, typically a taskforce.Message or subclass
        """
        if not self.connected:
            self.connect()

        self.logger.debug('send:{}'.format(str(data)))
        self.conn.send(data)

    def receive(self):
        """Check for incoming data

        Returns:
            Incoming data.  Typically a taskforce.Message or subclass.  None on timeout
        """
        if not self.connected:
            self.connect()
        if self.conn.poll(self.poll_timeout):
            try:
                data = self.conn.recv()
                self.logger.debug('receive:{}'.format(str(data)))
            except EOFError:
                self.logger.warn('pipe received EOFERROR')
                data = None
            return data
        return None

    def disconnect(self):
        """Disconnect the port"""
        if self.connected:
            self.conn.close()
            self.connected = False
