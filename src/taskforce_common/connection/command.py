from .message import Message


class Command(Message):
    """
    The Command class is used as a proxy to a remote procedure call.

    Objects of this type encapsulates a remote procedure call.  This object has a custom hashing method that allows it
    to act as a key in a dictionary.  The hash is based on the name, source, and destination values of the object.
    Likewise, the "equals" operator has been overloaded to check if the "name", "source", and "destination" parameters
    match between two objects.
    """

    def __init__(self, name, source=None, destination=None, args=(), kwargs={}):
        """
        Args:
            name: The name of the command.  Usually the remote method name
            source: The name of the sender of this object, as a string
            destination: The name of the target for this object, as a string
            args: Args to be sent to the method declared in "name" as a list
            kwargs: Key-word arguments to send to the method declared in "name" as a dictionary
        """
        super(Command, self).__init__(name)
        self.source = source
        self.destination = destination
        self.args = args
        self.kwargs = kwargs

    def __hash__(self):
        return hash((self.name, self.source, self.destination))

    def __eq__(self, other):
        if not isinstance(other, Command):
            return super(Command, self) == other
        return (self.name, self.source, self.destination) == (other.name, other.source, other.destination)

    def __ne__(self, other):
        if not isinstance(other, Command):
            return super(Command, self) != other
        return not self.__eq__(other)

    def __str__(self):
        return Message.__str__(self) + ' args:{} kwargs:{}'.format(self.args, self.kwargs)


class Response(Message):
    """
    The Response class is used when returning the value of remote function called using a Command.
    """
    pass


class ErrorResponse(Response):
    """
    The ErrorResponse is used when an error occurs during a remote function call.
    """
    name = 'error'

    def __init__(self, description):
        super(ErrorResponse, self).__init__(name='error', data=description)
