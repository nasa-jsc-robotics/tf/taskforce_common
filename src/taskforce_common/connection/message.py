class MessageException(Exception):
    pass


class Message(object):
    """
    The Message class is used to send data between as source and a destination.
    """

    def __init__(self, name, source=None, destination=None, data=None):
        """

        Args:
            name: Name of the message, as a string
            source: The sender of this message, as a string
            destination: The intended target of this message, as a string
            data: "Payload" of this message, as a Python object
        """
        self.name = name
        self.source = source
        self.destination = destination
        self.data = data

    def match(self, message):
        """
        Determine if this message matches another message.

        Any field in this messages that is None is considered a "don't care".
        """
        if not isinstance(message, Message):
            return False

        def check(a, b):
            return (a is None) or (a == b)

        return check(self.name, message.name) and check(self.source, message.source) and check(self.destination, message.destination) and check(self.data, message.data)

    def __str__(self):
        return "{}:{} {}:{} {}:{} {}:{}".format('name', self.name, 'source', self.source, 'destination', self.destination, 'data', self.data)


class ExceptionMessage(Message):
    """
    A special message containing a stack trace after an exception.

    To generate the 'traceback', simply call:
        import traceback
        traceback.format_exc()
    """
    def __init__(self, traceback, source=None, destination=None):
        Message.__init__(self, name='exception', source=source, destination=destination)
        self.traceback = traceback

    def __str__(self):
        return Message.__str__(self) + 'traceback: ' + self.traceback
