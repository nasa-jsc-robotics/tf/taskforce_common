import logging
import Queue
from .port import PortException, Publisher, Subscriber, Server, Client, Bidirectional

logger = logging.getLogger(__name__)

QUEUE_POLL_TIMEOUT_SECONDS = 0.1
QUEUE_TYPE = Queue.Queue


class QueuePortException(PortException):
    pass


class QueuePublisher(Publisher):
    """Publisher Port based on Python Queues"""

    def __init__(self):
        super(QueuePublisher, self).__init__()
        self._logger = logging.getLogger('QueuePublisher')
        self.connections = []

    def send(self, data):
        """Send a message over the connection.

        Args:
            message: message to send as a taskforce_common.Message (or sub-class)

        Returns:
            True on success
        """
        if not self.connected:
            self.connect()

        self.logger.debug('send:{}'.format(str(data)))
        for connection in self.connections:
            connection.put(data)
        return True

    def addConnection(self, connection):
        """Add another Queue to the Publisher.

        If an invalid type is passed in, a QueuePortException is thrown.

        Args:
            connection: Queue.Queue object.
        """
        if not isinstance(connection, QUEUE_TYPE):
            raise QueuePortException(
                'Adding connection with a non-queue object: connection={}, type={}'.format(connection,
                                                                                           type(connection)))
        self.connections.append(connection)


class QueueSubscriber(Subscriber):
    """Subscriber Port based on Python Queues"""

    def __init__(self, connection, poll_timeout=QUEUE_POLL_TIMEOUT_SECONDS):
        """

        Args:
            connection: A Python Queue.Queue object
            poll_timeout: Poll timeout used in the 'receive' method.
        """
        super(QueueSubscriber, self).__init__()
        self._logger = logging.getLogger('QueueSubscriber')

        if not isinstance(connection, QUEUE_TYPE):
            raise QueuePortException(
                'Initializing with a non-queue object: connection={}, type={}'.format(connection, type(connection)))

        self.conn = connection
        self.poll_timeout = poll_timeout

    def receive(self):
        """ Receive a new message

        Returns:
            Incoming message as a taskforce_common.Message object.  If message not received due to timeout, return None.
        """
        if not self.connected:
            self.connect()

        try:
            data = self.conn.get(timeout=self.poll_timeout)
            self.logger.debug('receive:{}'.format(str(data)))
            return data
        except Queue.Empty:
            pass

        return None


class QueueServer(Server):
    """Server Port based on Python Queues"""

    def __init__(self, connections=None, poll_timeout=QUEUE_POLL_TIMEOUT_SECONDS):
        """

        Args:
            connections:  tuple or list of two Queue objects: (send_queue, receive_queue)
            poll_timeout: Timeout used when polling the receive queue
        """
        super(QueueServer, self).__init__()
        self._logger = logging.getLogger('QueueServer')
        self.poll_timeout = poll_timeout

        # dictionary of receive port to send port
        self.connections = {}

        self.reply_conn = None
        if connections is not None:
            self.addConnections(connections)

    def waitForRequest(self):
        """Wait for an incoming request message.

        Returns:
            Incoming message.  If receiver times out, returns None.
        """
        if not self.connected:
            self.connect()
        if self.reply_conn != None:
            raise QueuePortException('Waiting for another request with a pending reply')

        for receiver, sender in self.connections.items():
            try:
                data = receiver.get(timeout=self.poll_timeout)
                self.logger.debug('request:{}'.format(str(data)))
                self.reply_conn = sender
                return data
            except Queue.Empty:
                pass
        return None

    def reply(self, data):
        """Reply to an incoming request.

        If the server does not have a pending request, a QueuePortException will be thrown.

        Args:
            data: reply message as a taskforce_common.Message object
        """
        if self.reply_conn is None:
            raise QueuePortException('Sending reply with no pending requests')
        self.logger.debug('reply:{}'.format(str(data)))
        self.reply_conn.put(data)
        self.reply_conn = None

    def addConnections(self, connections):
        """Add a pair of sender / receiver Queue objects to the Server.

        If the objects are not of the right type, a QueuePortException is thrown.

        Args:
            connections: tuple of (send_queue, receive_queue)
        """
        if not isinstance(connections, (tuple, list)):
            raise QueuePortException('Connections must be a tuple or a list')

        if len(connections) != 2:
            raise QueuePortException('Connections arg must be of size 2')

        if not isinstance(connections[0], QUEUE_TYPE):
            raise QueuePortException(
                'Initializing with a non-queue send_queue: connection={}, type={}'.format(connections[0],
                                                                                          type(connections[0])))
        if not isinstance(connections[1], QUEUE_TYPE):
            raise QueuePortException(
                'Initializing with a non-queue receive_queue: connection={}, type={}'.format(connections[1],
                                                                                             type(connections[1])))

        self.connections[connections[1]] = connections[0]


class QueueClient(Client):
    """Client Port based on Python Queues"""

    def __init__(self, connections, reply_timeout=5):
        """
        Args:
            connections:  tuple or list of two Queue objects: (send_queue, receive_queue)
            reply_timeout: Timeout for waiting for a reply from the server in seconds
        """
        super(QueueClient, self).__init__()
        self._logger = logging.getLogger('QueueClient')

        if not isinstance(connections, (tuple, list)):
            raise QueuePortException('Connections must be a tuple or a list')

        if len(connections) != 2:
            raise QueuePortException('Connections arg must be of size 2')

        if not isinstance(connections[0], QUEUE_TYPE):
            raise QueuePortException(
                'Initializing with a non-queue send_queue: connection={}, type={}'.format(connections[0],
                                                                                          type(connections[0])))

        if not isinstance(connections[1], QUEUE_TYPE):
            raise QueuePortException(
                'Initializing with a non-queue receive_queue: connection={}, type={}'.format(connections[1],
                                                                                             type(connections[1])))

        self.sender, self.receiver = connections
        self.reply_timeout = reply_timeout

    def request(self, data):
        """Send a request to the server

        Args:
            data: request message as a taskforce_common.Message

        Returns:
            Reply message from the server as a taskforce_common.Message.  If request times out, None is returned
        """
        self.sender.put(data)
        try:
            reply = self.receiver.get(timeout=self.reply_timeout)
            self.logger.debug('reply:{}'.format(str(reply)))
            return reply
        except Queue.Empty:
            pass
        return None


class QueueBidirectional(Bidirectional):
    """Bidirectional Port based on Python Queues"""

    def __init__(self, connections, poll_timeout=QUEUE_POLL_TIMEOUT_SECONDS):
        """
        Args:
            connections:  tuple or list of two Queue objects: (send_queue, receive_queue)
            poll_timeout: Polling timeout for receiver in seconds
        """
        super(QueueBidirectional, self).__init__()
        self._logger = logging.getLogger('QueueBidirectional')

        if not isinstance(connections, (tuple, list)):
            raise QueuePortException('Connections must be a tuple or a list')

        if len(connections) != 2:
            raise QueuePortException('Connections arg must be of size 2')

        if not isinstance(connections[0], QUEUE_TYPE):
            raise QueuePortException(
                'Initializing with a non-queue send_queue: connection={}, type={}'.format(connections[0],
                                                                                          type(connections[0])))

        if not isinstance(connections[1], QUEUE_TYPE):
            raise QueuePortException(
                'Initializing with a non-queue receive_queue: connection={}, type={}'.format(connections[1],
                                                                                             type(connections[1])))

        self.send_queue, self.receive_queue = connections
        self.poll_timeout = poll_timeout

    def send(self, data):
        """Send a message over the connection.

        Args:
            data: message to send as a taskforce_common.Message (or sub-class)

        Returns:
            True on success
        """
        if not self.connected:
            self.connect()

        self.logger.debug('send:{}'.format(str(data)))
        self.send_queue.put(data)

    def receive(self):
        """Receive a new message

        Returns:
            Incoming message as a taskforce_common.Message object.  If message not received due to timeout, return None.
        """
        if not self.connected:
            self.connect()
        try:
            data = self.receive_queue.get(timeout=self.poll_timeout)
            self.logger.debug('receive:{}'.format(str(data)))
            return data
        except Queue.Empty:
            pass
        return None

    def disconnect(self):
        """Disconnect the port"""
        if self.connected:
            self.send_queue.close()
            self.receive_queue.close()
            self.connected = False
