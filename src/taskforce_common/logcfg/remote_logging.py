######################################################################
# taken from: https://docs.python.org/2/howto/logging-cookbook.html
######################################################################
import pickle
import logging
import logging.handlers
import SocketServer
import struct

from threading import Thread
import signal


class LogRecordStreamHandler(SocketServer.StreamRequestHandler):
    """Handler for a streaming logging request.

    This basically logs the record using whatever logging policy is
    configured locally.
    """

    def handle(self):
        """Handle multiple requests

        Each expected to be a 4-byte length, followed by the LogRecord in pickle format. Logs the record
        according to whatever policy is configured locally.
        """
        while True:
            chunk = self.connection.recv(4)
            if len(chunk) < 4:
                break
            slen = struct.unpack('>L', chunk)[0]
            chunk = self.connection.recv(slen)
            while len(chunk) < slen:
                chunk = chunk + self.connection.recv(slen - len(chunk))
            obj = self.unPickle(chunk)
            record = logging.makeLogRecord(obj)
            self.handleLogRecord(record)

    def unPickle(self, data):
        return pickle.loads(data)

    def handleLogRecord(self, record):
        # if a name is specified, we use the named logger rather than the one
        # implied by the record.
        self.server.logname = 'server'

        if self.server.logname is not None:
            name = self.server.logname
        else:
            name = record.name
        logger = logging.getLogger(name)
        # N.B. EVERY record gets logged. This is because Logger.handle
        # is normally called AFTER logger-level filtering. If you want
        # to do filtering, do it at the client end to save wasting
        # cycles and network bandwidth!
        logger.handle(record)


class LogRecordSocketReceiver(SocketServer.ThreadingTCPServer):
    """Simple TCP socket-based logging receiver suitable for testing."""

    allow_reuse_address = True

    def __init__(self, host='localhost',
                 port=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                 handler=LogRecordStreamHandler):
        SocketServer.ThreadingTCPServer.__init__(self, (host, port), handler)
        self.abort = False
        self.timeout = 1
        self.logname = None

    def serve_until_stopped(self):
        import select
        abort = False
        self.serve_forever()
        while not abort:
            rd, wr, ex = select.select([self.socket.fileno()],
                                       [], [],
                                       self.timeout)
            if rd:
                self.handle_request()
            abort = self.abort


class LogServer(object):
    def __init__(self,port=logging.handlers.DEFAULT_TCP_LOGGING_PORT):
        self.tcpserver = LogRecordSocketReceiver(port=port)
        signal.signal(signal.SIGINT, signal.SIG_DFL)

    def start(self):
        logging.info('About to start TCP server...')
        self.serverThread = Thread(target=self.tcpserver.serve_until_stopped)
        self.serverThread.start()

    def stop(self):
        logging.info('Stopping TCP log server...')
        self.tcpserver.abort = True
        self.tcpserver.shutdown()
        self.serverThread.join()
