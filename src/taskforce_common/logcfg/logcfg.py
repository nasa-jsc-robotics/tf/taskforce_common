import logging
import logging.config
import os
import socket

log_level = {
    'CRITICAL': logging.CRITICAL,
    'critical': logging.CRITICAL,
    'DEBUG': logging.DEBUG,
    'debug': logging.DEBUG,
    'ERROR': logging.ERROR,
    'error': logging.ERROR,
    'FATAL': logging.FATAL,
    'fatal': logging.FATAL,
    'INFO': logging.INFO,
    'info': logging.INFO,
    'WARN': logging.WARNING,
    'warn': logging.WARNING,
    'WARNING': logging.WARNING,
    'warning': logging.WARNING
}

# To set the location of the TaskForce log file, set the environment variable: "TASKFORCE_LOG_FILE_NAME"
if 'TASKFORCE_LOG_FILE_NAME' not in os.environ.keys():
    os.environ['TASKFORCE_LOG_FILE_NAME'] = os.path.join(os.environ['HOME'], '.log', 'taskforce.log')


def get_log_config_file_path():
    """Get the path of the log config file provided in taskforce_common

    Returns:
        Path of the 'python_logging.conf' file as a string
    """
    directory = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(directory, 'python_logging.conf')
    return path


def configure_common_logging(config_file=None):
    """Configure logging for TaskForce

    Args:
        config_file: Optional path to a Python logging.conf file
    """
    if config_file is None:
        config_file = get_log_config_file_path()
        logfile = os.environ['TASKFORCE_LOG_FILE_NAME']
        if not os.path.exists(logfile):
            dir_name = os.path.dirname(logfile)
            if not os.path.exists(dir_name):
                os.makedirs(dir_name)
            f = open(logfile, 'w')
            f.close()

    logging.config.fileConfig(config_file, disable_existing_loggers = False)


def get_log_file_path():
    """Return the path of the log file"""
    return os.environ['TASKFORCE_LOG_FILE_NAME']


def setLevel(level, logger=None):
    """Set the log level

    Args:
        level: Log level as a string, or as the level defined in Python's logging module
        logger: Target logger.  If None, configure the root logger.
    """
    if logger is None:
        logger = logging.getLogger('')
    if isinstance(level, str):
        logger.setLevel(log_level[level])
    else:
        logger.setLevel(level)


class PreciseFormatter(logging.Formatter):
    """PreciseFormatter for the logger """

    def __init__(self, fmt=None, date_fmt=None):
        """
        Args:
            fmt: custom log message format
            date_fmt: custom time format
        """

        if fmt is None:
            hostname = socket.gethostname()
            fmt = "%(asctime)s {} taskforce: [%(levelname)-8s] [%(name)-15s] %(message)s".format(hostname)

        if date_fmt is None:
            date_fmt = "%Y-%m-%d %H:%M:%S"

        super(PreciseFormatter, self).__init__(fmt, date_fmt)
